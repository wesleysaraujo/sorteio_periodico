<?php
return array(
	'_root_'  => 'promotions',  // The default route
	'_404_'   => 'welcome/404',    // The main 404 route
	
	'hello(/:name)?' => array('welcome/hello', 'name' => 'hello'),
	//My Routes
	'login-de-acesso' => 'user/login',
	'cadastro-de-usuario' => 'user/cadastro',
	'confirmar-cadastro/(:any)' => 'user/confirmation/$1/',
	'perfil/(:any)' => 'user/perfil/$1',
	'alterar-senha/(:any)' => 'user/change_password/$1',
	'sair' => 'user/logout',
	//Promoções/Site
	'promocoes-ativas' => 'promotions',
	'participar-promocao/(:any)' => 'promotions/view/$1',
	'promocoes-que-participei' => 'promotions/minhas_participacoes',
	'detalhe-da-participacao/(:any)' => 'promotions/detalhe_participacao/$1',
	'ver-resultado-promocao/(:any)' => 'promotions/resultado/$1',
	'conta-click-parceiro' => 'promotions/conta_click_parceiro/',
	//Routes admin
	'admin/perguntas/(:any)' => 'admin/perguntas/$1'
);