<?php
return array(
	'version' => 
	array(
		'app' => 
		array(
			'default' => 
			array(
				0 => '001_create_users',
				1 => '002_add_resgisterhash_to_users',
				2 => '003_add_userstatus_to_users',
				3 => '004_add_profilefields_to_users',
				4 => '005_create_pesquisas',
				5 => '006_add_status_to_pesquisas',
				6 => '007_create_perguntas',
				7 => '008_create_banners',
				8 => '009_rename_field_pergunta_id_to_pesquisa_id_in_perguntas',
				9 => '010_create_resposta',
				10 => '011_create_opcaos',
				11 => '012_create_promotions',
				12 => '013_create_parceiros',
				13 => '014_add_status_to_promotions',
				14 => '015_delete_dates_from_pesquisas',
				15 => '016_create_brindes',
				16 => '017_add_title_to_promotions',
				17 => '018_create_users_promotions',
				18 => '019_create_users_opcoes',
				19 => '020_create_winners',
				20 => '021_create_raffles',
				21 => '022_create_click_parceiros',
			),
		),
		'module' => 
		array(
		),
		'package' => 
		array(
			'auth' => 
			array(
				0 => '001_auth_create_usertables',
				1 => '002_auth_create_grouptables',
				2 => '003_auth_create_roletables',
				3 => '004_auth_create_permissiontables',
				4 => '005_auth_create_authdefaults',
				5 => '006_auth_add_authactions',
				6 => '007_auth_add_permissionsfilter',
				7 => '008_auth_create_providers',
				8 => '009_auth_create_oauth2tables',
				9 => '010_auth_fix_jointables',
			),
		),
	),
	'folder' => 'migrations/',
	'table' => 'migration',
);
