<?php
class Model_Brinde extends \Orm\Model
{
	protected static $_belongs_to = array('promotion','parceiro');

	protected static $_properties = array(
		'id',
		'name',
		'amount',
		'description',
		'parceiro_id',
		'promotion_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Name', 'required|max_length[255]');
		$val->add_field('amount', 'Amount', 'required|valid_string[numeric]');
		$val->add_field('description', 'Description', 'required');
		$val->add_field('parceiro_id', 'Parceiro Id', 'required|valid_string[numeric]');
		$val->add_field('promotion_id', 'Promotion Id', 'required|valid_string[numeric]');

		return $val;
	}

}
