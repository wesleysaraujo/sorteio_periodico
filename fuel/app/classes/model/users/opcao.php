<?php

class Model_Users_Opcao extends \Orm\Model
{
	protected static $_belongs_to = array('user', 'opcao');
	protected static $_has_many = array('perguntas'); 

	protected static $_properties = array(
		'id',
		'opcao_id',
		'pergunta_id',
		'pesquisa_id',
		'user_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'users_opcoes';

}
