<?php
class Model_Pergunta extends \Orm\Model
{
	protected static $_has_many = array('opcao' => array('key_to' => 'pergunta_id'), 'respostas' => array('key_to' => 'pergunta_id'), 'users_opcao' => array('key_to' => 'pergunta_id'));
	
	protected static $_belongs_to = array(
		'user' => array(
			'key_from' => 'user_id'
		),
		'pesquisa' => array(
			'key_from' => 'pesquisa_id'
		),
		'banner' => array(
			'key_from' => 'banner_id'
		),
	);

	protected static $_properties = array(
		'id',
		'description',
		'banner_id',
		'type',
		'user_id',
		'pesquisa_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('description', 'Description', 'required|max_length[255]');
		$val->add_field('banner_id', 'Banner Id', 'required|valid_string[numeric]');
		$val->add_field('type', 'Type', 'required|max_length[50]');
		return $val;
	}

	protected static function validate_option($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('title', 'Opção', 'required|max_length[255]');
		$val->add_field('pergunta_id', 'Pergunta', 'required');
		return $val;
	}

}
