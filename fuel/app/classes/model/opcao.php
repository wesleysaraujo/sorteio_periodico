<?php

class Model_Opcao extends \Orm\Model
{
	protected static $_belongs_to = array('user' => array('key_from' => 'user_id'));

	protected static $_has_many = array('users_opcao' => array('key_to' => 'opcao_id'));

	protected static $_properties = array(
		'id',
		'title',
		'pergunta_id',
		'user_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('title', 'Opção', 'required|max_length[255]');
		return $val;
	}

	protected static $_table_name = 'opcoes';

}
