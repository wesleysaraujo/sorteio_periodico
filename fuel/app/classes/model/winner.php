<?php

class Model_Winner extends \Orm\Model
{
	public static $_belongs_to = array('user', 'promotion', 'brinde');

	protected static $_properties = array(
		'id',
		'user_id',
		'promotion_id',
		'brinde_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'winners';

}
