<?php
class Model_Banner extends \Orm\Model
{
	protected static $_has_many = array();
	protected static $_belongs_to = array('user');

	protected static $_properties = array(
		'id',
		'title',
		'description',
		'user_id',
		'type',
		'url',
		'media_url',
		'views',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate_parceiro($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('title', 'Title', 'required|max_length[128]');
		$val->add_field('description', 'Description', 'required');
		$val->add_field('type', 'Type', 'required|max_length[50]');
		//$val->add_field('url', 'Url', 'required');

		return $val;
	}

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('title', 'Title', 'required|max_length[128]');
		$val->add_field('description', 'Description', 'required');
		$val->add_field('type', 'Type', 'required|max_length[50]');
		$val->add_field('media_url', 'Media Url', 'required');

		return $val;
	}

}
