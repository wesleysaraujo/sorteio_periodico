<?php

class Model_Resposta extends \Orm\Model
{
	//public static $_belongs_to = array('pesquisa', 'user', 'pergunta');

	protected static $_properties = array(
		'id',
		'description',
		'pergunta_id',
		'pesquisa_id',
		'user_id',
		'type',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'respostas';

}
