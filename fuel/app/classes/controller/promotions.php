<?php

class Controller_Promotions extends Controller_Base
{
	private $status;

	public function before()
	{
		parent::before();

		if (Request::active()->controller !== 'Controller_Pesquisa' or ! in_array(Request::active()->action, array('login', 'logout')))
		{
			if (Auth::check())
			{
				if(Auth::get_profile_fields('user_status') === 'incompleto' OR Auth::get_profile_fields('user_status') === 'aguardando')
				{
					Response::redirect('perfil/'.$this->current_user->id);
				}
			}
			else
			{
				Response::redirect('login-de-acesso');
			}
		}
	}

	public function action_index()
	{
		$this->status = 'ativo';

		$data['promotions'] = Model_Promotion::find('all', array(
			'where' => array(
				array('status' => $this->status)
			),
		));


		$this->template->title = 'Promoções Atuais';
		$this->template->content = View::forge('promotions/index', $data);
	}

	public function action_view($id = null)
	{
		$promotion = Model_Promotion::find($id);
		
		if(!$promotion)
		{
			Session::set_flash('error', e('A promoção que você tentou acessar não está ativa ou não existe!'));
			Response::redirect('promocoes-ativas/');
		}
		
		$participation_check = Model_Users_Promotion::find('all', array(
			'where' => array(
				array('user_id', $this->current_user->id),
				array('promotion_id', $promotion->id)
			)
		));

		if($participation_check)
		{
			Session::set_flash('error', e('Você já participou dessa promoção'));
			Response::redirect('promocoes-ativas');
		}

		$data['promotion'] = $promotion;

		$this->template->title = 'Participando da promoção '.$promotion->title;
		$this->template->content = View::forge('promotions/view', $data, false);
	}

	public function action_resposta()
	{
		$response = Response::forge();
		if(Input::method() !== 'POST' or !Input::is_ajax())
		{
			$response->set_status(400);

			return $response;
		}
		if(Input::post('type') == 'aberta')
		{
			$resposta = Model_Resposta::forge(array(
				'description' => Input::post('resposta'),
				'pergunta_id' => Input::post('pergunta_id'),
				'pesquisa_id' => Input::post('pesquisa_id'),
				'type'		  => Input::post('type'),
				'user_id'     => $this->current_user->id
			));	
		}
		else
		{
			$resposta = Model_Users_Opcao::forge(array(
				'opcao_id' 	  => Input::post('opcao_id'),
				'pergunta_id' => Input::post('pergunta_id'),
				'pesquisa_id' => Input::post('pesquisa_id'),
				'type'		  => Input::post('type'),
				'user_id'     => $this->current_user->id
			));
		}
		

		if($resposta and $resposta->save())
		{
			$response->body(json_encode(array(
				'status' => true,
				'message' => 'Resposta armazenada com sucesso'
			)));
		}
		else
		{
			$response->set_status(500);
		}

		return $response;
	}

	public function action_participacao()
	{

		$response = Response::forge();
		if(Input::method() !== 'POST' or !Input::is_ajax())
		{
			$response->set_status(400);

			return $response;
		}
		
		$participation = Model_Users_Promotion::forge(array(
			'user_id' => $this->current_user->id,
			'promotion_id' => Input::post('promotion_id')
		));

		if($participation and $participation->save())
		{
			$response->body(json_encode(array(
				'status' => true,
				'message' => 'Resposta armazenada com sucesso'
			)));
		}
		else
		{
			$response->set_status(500);
		}

		return $response;
	}

	public function action_minhas_participacoes()
	{
		$participacoes = Model_Users_Promotion::find('all', array('where' => array(array('user_id', $this->current_user->id))));

		$data['participacoes'] = $participacoes;

		$this->template->title = 'Promoções que já participei';
		$this->template->content = View::forge('promotions/my_participations',$data);

	}

	public function action_detalhe_participacao($id = null)
	{
		$participacao = Model_Users_Promotion::find($id, array('where' => array(array('user_id', $this->current_user->id))));
		
		$winner = Model_Winner::find('all', array('where' => array(array('user_id', $this->current_user->id), array('promotion_id', $participacao->promotion_id))));
		
		$data['participacao'] = $participacao;
		$data['winner'] = $winner;
		$this->template->title = 'Detalhe da participação na promoção';
		$this->template->content = View::forge('promotions/details', $data);
	}

	public function action_resultado($id = null)
	{
		$promotion = Model_Promotion::find($id);
		$winners = Model_Winner::find('all', array('where' => array(array('promotion_id', $id))));

		$data['promotion'] = $promotion;
		$data['winners'] = $winners;

		$this->template->title = 'Resultado da promoção: '.$promotion->title;
		$this->template->content = View::forge('promotions/result', $data); 
	}

	public function action_conta_click_parceiro()
	{
		$response = Response::forge();
		if(Input::method() !== 'POST' or !Input::is_ajax())
		{
			$response->set_status(400);

			return $response;
		}

		$click_parceiro = Model_Click_Parceiro::forge(array(
			'parceiro_id' 	  => Input::post('parceiro_id'),
			'promotion_id' => Input::post('promotion_id'),
		));
		

		if($click_parceiro and $click_parceiro->save())
		{
			$response->body(json_encode(array(
				'status' => 200,
				'message' => 'Click registrado com sucesso'
			)));
		}
		else
		{
			$response->set_status(500);
		}

		return $response;
	}

}
