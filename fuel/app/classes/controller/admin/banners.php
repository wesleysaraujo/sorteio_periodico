<?php
class Controller_Admin_Banners extends Controller_Admin{

	public function action_index()
	{
		$data['banners'] = Model_Banner::find('all');
		$this->template->title = "Banners";
		$this->template->content = View::forge('admin/banners/index', $data);

	}

	public function action_view($id = null)
	{
		$data['banner'] = Model_Banner::find($id);

		$this->template->title = "Banner";
		$this->template->content = View::forge('admin/banners/view', $data, false);

	}

	public function action_create()
	{
		$data = array();

		if (Input::method() == 'POST')
		{
			if(Input::post('type') == 'parceria'){
				$val = Model_Banner::validate_parceiro('create');
			}
			else
			{
				$val = Model_Banner::validate('create');
			}

			if ($val->run())
			{
				
				if(Input::post('type') === 'adsense')
				{
					$banner = Model_Banner::forge(array(
						'title' => Input::post('title'),
						'description' => Input::post('description'),
						'user_id' => $this->current_user->id,
						'type' => Input::post('type'),
						'media_url' => Input::post('media_url'),
					));
				}
				else
				{
					//Configurações para realizar o upload do arquivo
					$config = array(
					    'path' => DOCROOT.'files/banners/',
					    'randomize' => true,
					    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
					);
					//Processa a configuração e o upload
					Upload::process($config);
					if (Upload::is_valid())
					{
					    // save them according to the config
					    Upload::save();
					    // call a model method to update the database
					    foreach(Upload::get_files() as $file){
						    $banner = Model_Banner::forge(array(
								'title' => Input::post('title'),
								'description' => Input::post('description'),
								'user_id' => $this->current_user->id,
								'type' => Input::post('type'),
								'url' => Input::post('url'),
								'media_url' => "files/banners/".$file['saved_as'],
							));	
						}
					}
					else
					{
						$data['val'] = Upload::get_error('erros'); 
					}
				}

				if ($banner and $banner->save())
				{
					Session::set_flash('success', e('Banner cadastrado com sucesso (ID: #'.$banner->id.').'));

					Response::redirect('admin/banners');
				}

				else
				{
					Session::set_flash('error', e('Não foi possível cadastrar o banner. Verifique o erro e tente novamente.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Banners";
		$this->template->content = View::forge('admin/banners/create', $data);

	}

	public function action_edit($id = null)
	{
		$banner = Model_Banner::find($id);
		$val = Model_Banner::validate('edit');

		if ($val->run())
		{
			$banner->title = Input::post('title');
			$banner->description = Input::post('description');
			$banner->user_id = Input::post('user_id');
			$banner->type = Input::post('type');
			$banner->url = Input::post('url');
			$banner->media_url = Input::post('media_url');
			$banner->views = Input::post('views');

			if ($banner->save())
			{
				Session::set_flash('success', e('Updated banner #' . $id));

				Response::redirect('admin/banners');
			}

			else
			{
				Session::set_flash('error', e('Could not update banner #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$banner->title = $val->validated('title');
				$banner->description = $val->validated('description');
				$banner->user_id = $val->validated('user_id');
				$banner->type = $val->validated('type');
				$banner->url = $val->validated('url');
				$banner->media_url = $val->validated('media_url');
				$banner->views = $val->validated('views');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('banner', $banner, false);
		}

		$this->template->title = "Banners";
		$this->template->content = View::forge('admin/banners/edit');

	}

	public function action_delete($id = null)
	{
		if ($banner = Model_Banner::find($id))
		{
			$banner->delete();

			Session::set_flash('success', e('Deleted banner #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete banner #'.$id));
		}

		Response::redirect('admin/banners');

	}


}