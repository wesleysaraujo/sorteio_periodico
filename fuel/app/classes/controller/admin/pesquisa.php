<?php
class Controller_Admin_Pesquisa extends Controller_Admin{

	public function action_index()
	{
		$config = array(
		    'pagination_url' => Uri::base().'admin/pesquisa/',
		    'total_items'    => Model_Pesquisa::count(),
		    'per_page'       => 20,
		    'uri_segment'    => 'page',
		    // or if you prefer pagination by query string
		    //'uri_segment'    => 'page',
		);

		$pagination = Pagination::forge('mypagination', $config);

		if (Auth::member(100) or Auth::member(1)) {
			$data['pesquisas'] = Model_Pesquisa::find('all', array('related' => array('user'), 'offset' => $pagination->offset, 'limit' => $pagination->per_page));
		}
		else
		{
			$data['pesquisas'] = Model_Pesquisa::find('all', array('where' => array(array('user_id', $this->current_user->id)),'related' => array('user'), 'offset' => $pagination->offset, 'limit' => $pagination->per_page));

		}
		
		
		$this->template->title = "Pesquisas";
		$this->template->content = View::forge('admin/pesquisa/index', $data);

	}

	public function action_view($id = null)
	{
		$data['pesquisa'] = Model_Pesquisa::find($id);
		$data['profile_fields'] = unserialize(html_entity_decode($data['pesquisa']->user->profile_fields, ENT_QUOTES));

		$this->template->title = "Pesquisa";
		$this->template->content = View::forge('admin/pesquisa/view', $data);

	}

	public function action_create()
	{
		if(Auth::member(1))
		{
			Session::set_flash('error', e('Você não é um pesquisador'));
			Response::redirect('admin/pesquisa');
		}

		if (Input::method() == 'POST')
		{
			$val = Model_Pesquisa::validate('create');

			if ($val->run())
			{
				$pesquisa = Model_Pesquisa::forge(array(
					'title' => Input::post('title'),
					'description' => Input::post('description'),
					//'date_start' => strtotime(Input::post('date_start')." ".Input::post('hour_start')),
					//'date_end' => strtotime(Input::post('date_end')." ".Input::post('hour_end')),
					'user_id'  => $this->current_user->id,
					'status'   => 'inativo'
				));

				if ($pesquisa and $pesquisa->save())
				{
					Session::set_flash('success', e('Pesquisa adicionada com sucesso #'.$pesquisa->id.'.'));

					Response::redirect('admin/pesquisa');
				}

				else
				{
					Session::set_flash('error', e('Could not save pesquisa.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Pesquisas";
		$this->template->content = View::forge('admin/pesquisa/create');

	}

	public function action_edit($id = null)
	{
		if(Auth::member(100))
		{
			$pesquisa = Model_Pesquisa::find($id);
		}
		else
		{
			$pesquisa = Model_Pesquisa::find($id, array('where' => array(array('user_id', $this->current_user->id))));
			if(count($pesquisa) == 0)
			{
				Session::set_flash('error', e('A pequisa que você está tentando editar não lhe pertence'));
				Response::redirect('admin/pesquisa');
			}
		}
		
		$val = Model_Pesquisa::validate('edit');

		//$hora_inicial = explode(' ',date('Y-m-d H:i:s', $pesquisa->date_start));
		//$hora_final = explode(' ',date('Y-m-d H:i:s', $pesquisa->date_end));

		//$pesquisa->hour_start = $hora_inicial[1];
		//$pesquisa->hour_end = $hora_final[1];
		//$pesquisa->date_start = $hora_inicial[0]; 
		//$pesquisa->date_end = $hora_final[0]; 

		if ($val->run())
		{
			$pesquisa->title = Input::post('title');
			$pesquisa->description = Input::post('description');
			//$pesquisa->date_start = strtotime(Input::post('date_start')." ".Input::post('hour_start'));
			//$pesquisa->date_end = strtotime(Input::post('date_end')." ".Input::post('hour_end'));
			$pesquisa->status = Input::post('status');

			if ($pesquisa->save())
			{
				Session::set_flash('success', e('Updated pesquisa #' . $id));

				Response::redirect('admin/pesquisa');
			}

			else
			{
				Session::set_flash('error', e('Could not update pesquisa #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$pesquisa->title = $val->validated('title');
				$pesquisa->description = $val->validated('description');
				//$pesquisa->date_start = $val->validated('date_start');
				//$pesquisa->date_end = $val->validated('date_end');
				//$pesquisa->result = $val->validated('hour_start');
				//$pesquisa->views = $val->validated('hour_end');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('pesquisa', $pesquisa, false);
		}

		$this->template->title = "Pesquisas";
		$this->template->content = View::forge('admin/pesquisa/edit');

	}

	public function action_delete($id = null)
	{
		if(Auth::member(100))
		{
			$pesquisa = Model_Pesquisa::find($id);
		}
		else
		{
			$pesquisa = Model_Pesquisa::find($id, array('where' => array(array('user_id', $this->current_user->id))));
			if(count($pesquisa) == 0)
			{
				Session::set_flash('error', e('A pequisa que você está tentando excluir não lhe pertence'));
				Response::redirect('admin/pesquisa');
			}
		}

		if ($pesquisa)
		{
			//Seleciona as perguntas dessa pesquisa
			$perguntas = Model_Pergunta::find('all', array('where' => array(array('pesquisa_id', $pesquisa->id))));
			
			//Se existirem perguntas
			if(count($perguntas) > 0)
			{
				
				foreach($perguntas as $pergunta)
				{
					//Ele verifica se elas tem opções
					$opcoes = Model_Opcao::find('all', array('where' => array(array('pergunta_id', $pergunta->id))));
					if(count($opcoes) > 0)
					{
						foreach($opcoes as $opcao)
						{
							//Se existirem opções para a pergunta ele exclui
							$opcao->delete();
						}
					}
					//Terminado a exclusão das opões e exclui a pergunta
					$pergunta->delete();
				}
				//Exclui a pesquisa
				$pesquisa->delete();
				Session::set_flash('success', e('Deleted pesquisa #'.$id));
			}
			else
			{
				$pesquisa->delete();
				Session::set_flash('success', e('Deleted pesquisa #'.$id));
			}
		}

		else
		{
			Session::set_flash('error', e('Could not delete pesquisa #'.$id));
		}

		Response::redirect('admin/pesquisa');

	}


}