<?php
class Controller_Admin_Promotion extends Controller_Admin{


	public function action_index()
	{
		$config = array(
		    'pagination_url' => Uri::base().'admin/promotion/',
		    'total_items'    => Model_Promotion::count(),
		    'per_page'       => 20,
		    'uri_segment'    => 'page',
		    // or if you prefer pagination by query string
		    //'uri_segment'    => 'page',
		);

		$pagination = Pagination::forge('mypagination', $config);

		if($this->current_user->group == 100)
		{
			$data['promotions'] = Model_Promotion::find('all',array('related' => array('pesquisa', 'user'),'offset' => $pagination->offset, 'limit' => $pagination->per_page));
		}
		else
		{
			$data['promotions'] = Model_Promotion::find('all',array('where' => array(array('user_id', $this->current_user->id)) ,'related' => array('pesquisa', 'user'),'offset' => $pagination->offset, 'limit' => $pagination->per_page));
		}
		// we pass the object, it will be rendered when echo'd in the view
		$data['pagination'] = $pagination;

		$this->template->title = "Promoções";
		$this->template->content = View::forge('admin/promotion/index', $data, false);

	}

	public function action_view($id = null)
	{
		if ($this->current_user->group == 100)
		{
			$data['promotion'] = Model_Promotion::find($id);
		}
		else
		{
			$data['promotion'] = Model_Promotion::find($id, array('where' => array(array('user_id', $this->current_user->id))));
			if(count($data['promotion']) == 0)
			{
				Response::redirect('admin/promotion');
			}
		}

		$this->template->title = "Visualizando promoção";
		$this->template->content = View::forge('admin/promotion/view', $data);

	}

	public function action_participations($id = null)
	{
		if($this->current_user->group != 100)
		{
			Response::redirect('/admin');
		}

		$config = array(
		    'pagination_url' => Uri::base().'admin/promotion/participations/',
		    'total_items'    => Model_Users_Promotion::count(),
		    'per_page'       => 20,
		    'uri_segment'    => 'page',
		    // or if you prefer pagination by query string
		    //'uri_segment'    => 'page',
		);

		$pagination = Pagination::forge('mypagination', $config);

		$promotion = Model_Promotion::find($id);

		$participations = Model_Users_Promotion::find('all', array('related' => array('user', 'promotion'),'where' => array(array('promotion_id' => $id)), 'offset' => $pagination->offset, 'limit' => $pagination->per_page));

		$raffle = Model_Raffle::find('last', array('where' => array(array('promotion_id', $id))));

		$new_users = Model_User::find('all', array('where' => array(array('created_at', '>=', $promotion->date_start))));

		$winners = Model_Winner::find('all', array('where' => array(array('promotion_id', $id)), 'related' => array('user', 'brinde', 'promotion')));

		$data['participations'] = $participations;
		$data['promotion'] = $promotion;
		$data['raffle'] = $raffle;
		$data['winners'] = $winners;
		$data['new_users'] = $new_users;
		$this->template->title = 'Participações da promoção: '.$promotion->title;
		$this->template->content = View::forge('admin/promotion/participations', $data);
	}

	public function action_raffle($promotion_id = false)
	{
		if($this->current_user->group != 100)
		{
			Response::redirect('/admin');
		}

		$promotion = Model_Promotion::find($promotion_id, array('related' => array('brindes', 'user')));
		$raffle_exist = Model_Raffle::find('all', array('where' => array(array('promotion_id', $promotion_id))));
		$this->template->title = "Sorteio da promoção";
		
		
		if(count($raffle_exist) == 0)
		{

			$chosen = DB::select('user_id')->from('users_promotions')->where('promotion_id', $promotion_id)->limit(count($promotion->brindes))->order_by(DB::expr('RAND()'))->as_object('Model_Users_Promotion')->execute();
			
			//Array que irá armazenar os IDs dos Brindes
			$winner_brinde = array();
			foreach ($promotion->brindes as $brinde)
			{
				array_push($winner_brinde, $brinde->id);	
			}
			//Array que irá armazernar os IDs dos ganhandores
			$winner_raffle = array();
			foreach($chosen as $winner)
			{
				array_push($winner_raffle, $winner->user->id);
			}
			//Associa cada ganhador a um brinde
			$winners = array_combine($winner_brinde, $winner_raffle);

			//print_r($winner_brinde);
			//print_r($winner_raffle);
			for ($i=0; $i < count($winners); $i++) { 
				
				$brinde = key($winners);
				$user   = $winners[$brinde];

				//Salva ganhador na tabela
				$save_winner = Model_Winner::forge(array(
					'user_id' => $user,
					'promotion_id' => $promotion_id,
					'brinde_id' => $brinde,
				))->save();

				next($winners);  
			}
			//Salva o sorteio
			$raffle = Model_Raffle::forge(array(
				'promotion_id' => $promotion_id,
			));
			if($raffle->save())
			{
				Session::set_flash('success', e('Sorteio da promoção "'.$promotion->description.'" foi realizado com sucesso consulte os ganhadores.'));
			}
			else
			{
				Session::set_flash('error', e('Ocorreu um erro ao tentar realizar o sorteio para a promoção: '.$promotion->description));
			}			

		}
		else
		{
			Session::set_flash('error', e('Essa promoção já foi sorteada'));
			Response::redirect('admin/promotion/participations/'.$promotion->id);
		}

		Response::redirect('admin/promotion/participations/'.$promotion->id);
		
	}

	public function action_send_email()
	{
		$id = Input::post('promotion');
		$resposne = Response::forge();
		if($id == null)
		{
			
		}
	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Promotion::validate('create');

			if ($val->run())
			{
				
				$promotion = Model_Promotion::forge(array(
					'title' => Input::post('title'),
					'description' => Input::post('description'),
					'pesquisa_id' => Input::post('pesquisa_id'),
					'date_start' => strtotime(Input::post('date_start')." ".Input::post('hour_start')),
					'date_end' => strtotime(Input::post('date_end')." ".Input::post('hour_end')),
					'user_id' => $this->current_user->id,
					'status' => Input::post('status'),
				));

				if ($promotion and $promotion->save())
				{
					Session::set_flash('success', e('Promoção criada com sucesso! ID #'.$promotion->id.'.'));

					Response::redirect('admin/promotion');
				}

				else
				{
					Session::set_flash('error', e('Could not save promotion.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$view = View::forge('admin/promotion/create');
		$view->set_global('pesquisas', Arr::assoc_to_keyval(Model_Pesquisa::find('all', array('where' => array(array('status', 'ativo')))),'id', 'title'));
		$this->template->title = "Criar promoção";
		$this->template->content = $view;

	}

	public function action_edit($id = null)
	{
		if($this->current_user->group == 100)
		{
			$promotion = Model_Promotion::find($id);	
		}
		else
		{
			$promotion = Model_Promotion::find($id, array('where' => array(array('user_id', $this->current_user->id))));
			if(count($promotion) == 0)
			{
				Session::set_flash('error', e('A promoção que você está tentando editar não pertence a você ou não existe'));
				Response::redirect('admin/promotion');
			}
		}
		$val = Model_Promotion::validate('edit');

		$hora_inicial = explode(' ',date('Y-m-d H:i:s', $promotion->date_start));
		$hora_final = explode(' ',date('Y-m-d H:i:s', $promotion->date_end));

		$promotion->hour_start = $hora_inicial[1];
		$promotion->hour_end = $hora_final[1];
		$promotion->date_start = $hora_inicial[0]; 
		$promotion->date_end = $hora_final[0]; 


		if ($val->run())
		{
			$promotion->title = Input::post('title');
			$promotion->description = Input::post('description');
			$promotion->pesquisa_id = Input::post('pesquisa_id');
			$promotion->date_start = strtotime(Input::post('date_start')." ".Input::post('hour_start'));
			$promotion->date_end = strtotime(Input::post('date_end')." ".Input::post('hour_end'));
			$promotion->user_id = $this->current_user->id;
			$promotion->status = Input::post('status');

			if ($promotion->save())
			{
				Session::set_flash('success', e('Promoção #' . $id .' atualizada com sucesso'));

				Response::redirect('admin/promotion');
			}

			else
			{
				Session::set_flash('error', e('Could not update promotion #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$promotion->title = $val->validated('title');
				$promotion->description = $val->validated('description');
				$promotion->pesquisa_id = $val->validated('pesquisa_id');
				$promotion->date_start = $val->validated('date_start');
				$promotion->date_end = $val->validated('date_end');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('promotion', $promotion, false);
		}
		
		$view = View::forge('admin/promotion/edit');
		$view->set_global('pesquisas', Arr::assoc_to_keyval(Model_Pesquisa::find('all', array('whare' => array(array('status', 'ativo')))),'id', 'title'));
		$this->template->title = "Editando promoção";
		$this->template->content = $view;

	}

	public function action_delete($id = null)
	{
		if ($promotion = Model_Promotion::find($id))
		{
			$promotion->delete();

			Session::set_flash('success', e('Deleted promotion #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete promotion #'.$id));
		}

		Response::redirect('admin/promotion');

	}


}