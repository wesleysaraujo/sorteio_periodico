<?php 
class Controller_Admin_Users extends Controller_Admin
{
	public function action_index()
	{
		$config = array(
		    'pagination_url' => Uri::base().'admin/users/',
		    'total_items'    => Model_User::count(),
		    'per_page'       => 20,
		    'uri_segment'    => 'page',
		    // or if you prefer pagination by query string
		    //'uri_segment'    => 'page',
		);

		$pagination = Pagination::forge('mypagination', $config);

		$users = Model_User::find('all', array('offset' => $pagination->offset, 'limit' => $pagination->per_page));
		
		$data['pagination'] = $pagination;
		$data['users'] = $users;
		#$data['profile_fields'] = unserialize(html_entity_decode($users->profile_fields, ENT_QUOTES));

		$this->template->title = "Usuários do Site";
		$this->template->content = View::forge('admin/users/index', $data,false);
	}

	public function action_view($id = null)
	{
		$user = Model_User::find($id);
		$data['user'] = $user;
		$data['profile_fields'] = unserialize(html_entity_decode($user->profile_fields, ENT_QUOTES));
		$this->template->title = "Informações do usuário";
		$this->template->content = View::forge('admin/users/view', $data);
	}

	public function action_create()
	{
		$val = Validation::forge('user_cadastro');

		if (Input::method() == 'POST')
		{
			$val->add_callable('MyRules');
			$val->add_field('full_name', 'Nome completo', 'required|min_length[3]');
			$val->add_field('username', 'Username (Nome de usuário)', 'required|min_length[3]')->add_rule('unique', 'users.username');
			$val->add_field('email', 'E-mail', 'required|valid_email')->add_rule('unique', 'users.email');
			$val->add_field('password', 'Senha', 'required|min_length[6]');
			$val->add_field('password-confirm', 'Confirmar Senha', 'required')->add_rule('match_field', 'password');

			if($val->run())
			{
				$hash 		 = Auth::create_login_hash();
				$username 	 = Input::post('full_name'); 
				$create_user = Auth::create_user(
					Input::post('username'),
					Input::post('password'),
					Input::post('email'),
					Input::post('group'),
					array(
						'full_name' => Input::post('full_name'),
						'register_hash' => $hash,
						'user_status' => 'ativado',
					)
				);

				if($create_user)
				{
					$mensagem_email = "<h3>Olá ".Input::post('full_name')."</h3>";
					$mensagem_email .= "Você acabou de criar um usuário no nosso sistema de pesquisa.<br/>";
					$mensagem_email .= "Para ativar sua conta clique no link abaixo e faça login no sistema.";
					$mensagem_email .= "<p> Nome de usuário: ".Input::post('username')."</p>";
					$mensagem_email .= "<p> E-mail de cadastro: ".Input::post('email')."</p>";
					$mensagem_email .= "<p> Senha: ".Input::post('password')."</p>";
					$mensagem_email .= "<a href=\"".Uri::base()."confirmar-cadastro/$hash/?email=".Input::post('email')."\">Clique aqui para confirmar seu cadastro</a>";

					$email = Email::forge();
					$email->from('admin@server.comeceuma.com', 'O Debate On')->to(Input::post('email'), Input::post('full_name'));
					$email->subject('Confirmação de cadastro de Usuário no O Debate On Pesquisas');
					$email->html_body($mensagem_email);

					try
					{
						$email->send();
					}
					catch(\EmailValidationFailedException $e)
					{
						Session::set_flash('error', e('Erro na validação do envio de e-mail.'));
					}
					catch(\EmailSendingFailedException $e)
					{
						Session::set_flash('error', e('Erro no envio do e-mail da confirmação do cadastro'));
					}

					Session::set_flash('success', e('Cadastro realizado com sucesso, O usuário recebeu um e-mail de confirmação para ativar seu cadastro.'));
					Response::redirect('admin/users/');
				}
			}else{
				Session::set_flash('error', e('Erro de cadastro,tente novamente preenchendo todas as informações corretamente.'));
			}
		
		}
		
		$this->template->title = 'Cadastro de usuário';
		$this->template->content = View::forge('admin/users/create', array('val' => $val));
	}

	public function action_delete($id = null)
	{
		if ($user = Model_User::find($id))
		{
			$user->delete();

			Session::set_flash('success', e('Usuario #'.$id.' excluido com sucesso'));
		}

		else
		{
			Session::set_flash('error', e('Não foi possível excluir usuário #'.$id));
		}

		Response::redirect('admin/users');

	}
}