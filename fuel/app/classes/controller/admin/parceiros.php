<?php
class Controller_Admin_Parceiros extends Controller_Admin{
	
	public function action_index()
	{
		$config = array(
		    'pagination_url' => Uri::base().'admin/parceiros/',
		    'total_items'    => Model_Parceiro::count(),
		    'per_page'       => 10,
		    'uri_segment'    => 'page',
		    // or if you prefer pagination by query string
		    //'uri_segment'    => 'page',
		);

		$pagination = Pagination::forge('mypagination', $config);

		$data['parceiros'] = Model_Parceiro::find('all', array('offset' => $pagination->offset, 'limit' => $pagination->per_page));
		
		$data['pagination'] = $pagination;

		$this->template->title = "Parceiros";
		$this->template->content = View::forge('admin/parceiros/index', $data);

	}

	public function action_view($id = null)
	{
		$parceiro = Model_Parceiro::find($id);
		$data['parceiro'] = $parceiro;
		$data['clicks']   = Model_Click_Parceiro::find('all', array('where' => array(array('parceiro_id', $parceiro->id))));

		$this->template->title = "Visualização de Parceiro";
		$this->template->content = View::forge('admin/parceiros/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Parceiro::validate('create');

			if ($val->run())
			{
				//Configurações para realizar o upload do arquivo
				$config = array(
				    'path' => DOCROOT.'files/parceiros/',
				    'randomize' => true,
				    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
				);
				//Processa a configuração e o upload
				Upload::process($config);
				if (Upload::is_valid())
				{
				    // save them according to the config
				    Upload::save();
				    // call a model method to update the database
				    foreach(Upload::get_files() as $file){
				    	$parceiro = Model_Parceiro::forge(array(
							'name' => Input::post('name'),
							'site_url' => Input::post('site_url'),
							'image_url' => "files/parceiros/".$file['saved_as'],
							'email' => Input::post('email'),
							'description' => Input::post('description'),
							'status' => Input::post('status'),
						));

						Image::load(DOCROOT.'files/parceiros/'.$file['saved_as'])->resize(200, 200);
					}
				}	
				else
				{
					$data['val'] = Upload::get_errors();

					$parceiro = Model_Parceiro::forge(array(
							'name' => Input::post('name'),
							'site_url' => Input::post('site_url'),
							'image_url' => '',
							'email' => Input::post('email'),
							'description' => Input::post('description'),
							'status' => Input::post('status'),
						)); 
				}	

				if ($parceiro and $parceiro->save())
				{
					Session::set_flash('success', e('Parceiro cadastrado com sucesso #'.$parceiro->id.'.'));

					Response::redirect('admin/parceiros');
				}

				else
				{
					Session::set_flash('error', e('Não foi possivel cadastrar o parceiro.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Cadastro de parceiro";
		$this->template->content = View::forge('admin/parceiros/create', '',false);

	}

	public function action_edit($id = null)
	{
		$parceiro = Model_Parceiro::find($id);
		$val = Model_Parceiro::validate('edit');

		if ($val->run())
		{
			//Configurações para realizar o upload do arquivo
			/*
			$config = array(
			    'path' => DOCROOT.'files/parceiros/',
			    'randomize' => true,
			    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
			);
			//Processa a configuração e o upload
			Upload::process($config);
			if (Upload::is_valid())
			{
			    // save them according to the config
			    Upload::save();
			    $image_url = null;
			    foreach(Upload::get_files() as $file)
			    {
			    	$image_url = 'files/parceiros/'.$file['saved_as'];
			    	Image::load(DOCROOT.'files/parceiros/'.$file['saved_as'])
			    			->resize(200, 200, true, true);
			    } 
			}
			else
			{
				$image_url = $parceiro->image_url;
			}
			*/

			$image_url = $parceiro->image_url;
			$parceiro->name = Input::post('name');
			$parceiro->site_url = Input::post('site_url');
			$parceiro->image_url = $image_url;
			$parceiro->email = Input::post('email');
			$parceiro->description = Input::post('description');
			$parceiro->status = Input::post('status');

			if ($parceiro->save())
			{
				Session::set_flash('success', e('Informações do parceiro #' . $id . ' foram atualizadas com sucesso'));

				Response::redirect('admin/parceiros');
			}

			else
			{
				Session::set_flash('error', e('Could not update parceiro #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$parceiro->name = $val->validated('name');
				$parceiro->site_url = $val->validated('site_url');
				$parceiro->email = $val->validated('email');
				$parceiro->description = $val->validated('description');
				$parceiro->status = $val->validated('status');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('parceiro', $parceiro, false);
		}

		$this->template->title = "Editando Parceiro";
		$this->template->content = View::forge('admin/parceiros/edit');

	}

	public function action_delete($id = null)
	{
		if ($parceiro = Model_Parceiro::find($id))
		{
			if($parceiro->image_url !== null)
			{
				$file = File::delete(DOCROOT."$parceiro->image_url");
			}

			$parceiro->delete();

			Session::set_flash('success', e('Parceiro excluido com sucesso: #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete parceiro #'.$id));
		}

		Response::redirect('admin/parceiros');

	}


}