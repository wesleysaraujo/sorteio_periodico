<?php
class Controller_Admin_Brindes extends Controller_Admin{
	public function action_index()
	{
		$data['brindes'] = Model_Brinde::find('all');
		$this->template->title = "Brindes";
		$this->template->content = View::forge('admin/brindes/index', $data);

	}

	public function action_view($id = null)
	{
		$data['brinde'] = Model_Brinde::find($id);

		$this->template->title = "Brinde";
		$this->template->content = View::forge('admin/brindes/view', $data);

	}

	public function action_get_brindes()
	{
		$id_promotion = Input::get('id');
		$response = Response::forge();
		
		if($id_promotion === null)
		{
			$response->set_status(400);

			return $response;
		}

		$brindes = Model_Brinde::find('all', array(
			'where' => array(
				array('promotion_id', $id_promotion)
			),
		));

		if($brindes)
		{
			$response->body(json_encode(array(
				'status' => 200,
				'brindes' => Arr::assoc_to_keyval(Model_Brinde::find('all', array('where' => array(array('promotion_id', $id_promotion)))), 'id', 'name')
			)));
		}
		else
		{
			$response->set_status(500);
		}

		return $response;
	}

	public function action_create($id_promotion = null)
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Brinde::validate('create');

			if ($val->run())
			{
				$brinde = Model_Brinde::forge(array(
					'name' => Input::post('name'),
					'amount' => Input::post('amount'),
					'description' => Input::post('description'),
					'parceiro_id' => Input::post('parceiro_id'),
					'promotion_id' => Input::post('promotion_id'),
				));

				if ($brinde and $brinde->save())
				{
					Session::set_flash('success', e('Brinde cadastrado com sucesso #'.$brinde->id.'.'));

					Response::redirect('admin/promotion/view/'.$id_promotion);
				}

				else
				{
					Session::set_flash('error', e('Não foi possível salvar o brinde.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}
		$view = View::forge('admin/brindes/create');
		$view->set_global('parceiros', Arr::assoc_to_keyval(Model_Parceiro::find('all'), 'id', 'name'));
		$view->set_global('promotion', $id_promotion);
		/*
		if(isset($id_promotion) and $id_promotion != null)
		{
			$view->set_global('promotions', Arr::assoc_to_keyval(Model_Promotion::find($id_promotion), 'id', 'title'));
		}
		else
		{
			$view->set_global('promotions', Arr::assoc_to_keyval(Model_Promotion::find('all'), 'id', 'title'));
		}
		*/
		$this->template->title = "Cadastrar novo brinde";
		$this->template->content = $view;

	}

	public function action_edit($id = null)
	{
		$brinde = Model_Brinde::find($id);
		$val = Model_Brinde::validate('edit');

		if ($val->run())
		{
			$brinde->name = Input::post('name');
			$brinde->amount = Input::post('amount');
			$brinde->description = Input::post('description');
			$brinde->parceiro_id = Input::post('parceiro_id');
			$brinde->promotion_id = Input::post('promotion_id');

			if ($brinde->save())
			{
				Session::set_flash('success', e('Updated brinde #' . $id));

				Response::redirect('admin/promotion/view/'.$brinde->promotion_id);
			}

			else
			{
				Session::set_flash('error', e('Could not update brinde #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$brinde->name = $val->validated('name');
				$brinde->amount = $val->validated('amount');
				$brinde->description = $val->validated('description');
				$brinde->parceiro_id = $val->validated('parceiro_id');
				$brinde->promotion_id = $val->validated('promotion_id');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('brinde', $brinde, false);
		}
		$view = View::forge('admin/brindes/edit');
		$view->set_global('parceiros', Arr::assoc_to_keyval(Model_Parceiro::find('all'), 'id', 'name'));

		$this->template->title = "Edição de brinde";
		$this->template->content = $view;

	}

	public function action_delete($id = null)
	{
		if ($brinde = Model_Brinde::find($id))
		{
			$id_promotion = $brinde->promotion_id;

			$brinde->delete();

			Session::set_flash('success', e('Brinde Excluido #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Brinde não pode ser Excluir #'.$id));
		}

		Response::redirect('admin/promotion/view/'.$id_promotion);

	}


}