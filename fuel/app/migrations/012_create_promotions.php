<?php

namespace Fuel\Migrations;

class Create_promotions
{
	public function up()
	{
		\DBUtil::create_table('promotions', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'description' => array('type' => 'text'),
			'pesquisa_id' => array('constraint' => 11, 'type' => 'int'),
			'date_start' => array('constraint' => 11, 'type' => 'int'),
			'date_end' => array('constraint' => 11, 'type' => 'int'),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('promotions');
	}
}