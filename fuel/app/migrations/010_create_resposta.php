<?php

namespace Fuel\Migrations;

class Create_resposta
{
	public function up()
	{
		\DBUtil::create_table('respostas', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'description' => array('type' => 'text'),
			'pergunta_id' => array('constraint' => 11, 'type' => 'int'),
			'pesquisa_id' => array('constraint' => 11, 'type' => 'int'),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'type' => array('constraint' => 32, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('respostas');
	}
}