<?php

namespace Fuel\Migrations;

class Create_brindes
{
	public function up()
	{
		\DBUtil::create_table('brindes', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'amount' => array('constraint' => 11, 'type' => 'int'),
			'description' => array('type' => 'text'),
			'parceiro_id' => array('constraint' => 11, 'type' => 'int'),
			'promotion_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('brindes');
	}
}