<?php

namespace Fuel\Migrations;

class Add_status_to_pesquisas
{
	public function up()
	{
		\DBUtil::add_fields('pesquisas', array(
			'status' => array('constraint' => 32, 'type' => 'varchar'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('pesquisas', array(
			'status'

		));
	}
}