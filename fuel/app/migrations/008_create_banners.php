<?php

namespace Fuel\Migrations;

class Create_banners
{
	public function up()
	{
		\DBUtil::create_table('banners', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'title' => array('constraint' => 128, 'type' => 'varchar'),
			'description' => array('type' => 'text'),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'type' => array('constraint' => 50, 'type' => 'varchar'),
			'url' => array('constraint' => 255, 'type' => 'varchar'),
			'media_url' => array('type' => 'text'),
			'views' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('banners');
	}
}