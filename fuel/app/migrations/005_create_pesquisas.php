<?php

namespace Fuel\Migrations;

class Create_pesquisas
{
	public function up()
	{
		\DBUtil::create_table('pesquisas', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'title' => array('constraint' => 255, 'type' => 'varchar'),
			'description' => array('type' => 'text'),
			'result' => array('type' => 'text', 'null' => true),
			'views' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('pesquisas');
	}
}