<?php

namespace Fuel\Migrations;

class Create_users_opcoes
{
	public function up()
	{
		\DBUtil::create_table('users_opcoes', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'opcao_id' => array('constraint' => 11, 'type' => 'int'),
			'pergunta_id' => array('constraint' => 11, 'type' => 'int'),
			'pesquisa_id' => array('constraint' => 11, 'type' => 'int'),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('users_opcoes');
	}
}