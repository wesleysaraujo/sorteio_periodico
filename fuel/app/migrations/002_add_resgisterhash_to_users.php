<?php

namespace Fuel\Migrations;

class Add_resgisterhash_to_users
{
	public function up()
	{
		\DBUtil::add_fields('users', array(
			'resgister_hash' => array('constraint' => 32, 'type' => 'varchar', 'null' => true),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('users', array(
			'resgister_hash'

		));
	}
}