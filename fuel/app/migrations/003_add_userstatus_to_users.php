<?php

namespace Fuel\Migrations;

class Add_userstatus_to_users
{
	public function up()
	{
		\DBUtil::add_fields('users', array(
			'user_status' => array('constraint' => 32, 'type' => 'varchar', 'null' => ''),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('users', array(
			'user_status'

		));
	}
}