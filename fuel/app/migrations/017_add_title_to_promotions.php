<?php

namespace Fuel\Migrations;

class Add_title_to_promotions
{
	public function up()
	{
		\DBUtil::add_fields('promotions', array(
			'title' => array('constraint' => 128, 'type' => 'varchar'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('promotions', array(
			'title'

		));
	}
}