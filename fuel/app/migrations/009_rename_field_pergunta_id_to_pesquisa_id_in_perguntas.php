<?php

namespace Fuel\Migrations;

class Rename_field_pergunta_id_to_pesquisa_id_in_perguntas
{
	public function up()
	{
		\DBUtil::modify_fields('perguntas', array(
			'pergunta_id' => array('name' => 'pesquisa_id', 'type' => 'int', 'constraint' => 11)
		));
	}

	public function down()
	{
	\DBUtil::modify_fields('perguntas', array(
			'pesquisa_id' => array('name' => 'pergunta_id', 'type' => 'int', 'constraint' => 11)
		));
	}
}