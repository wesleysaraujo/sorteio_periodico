<?php

namespace Fuel\Migrations;

class Create_click_parceiros
{
	public function up()
	{
		\DBUtil::create_table('click_parceiros', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'parceiro_id' => array('constraint' => 11, 'type' => 'int'),
			'promotion_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('click_parceiros');
	}
}