<?php

namespace Fuel\Migrations;

class Create_perguntas
{
	public function up()
	{
		\DBUtil::create_table('perguntas', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'description' => array('constraint' => 255, 'type' => 'varchar'),
			'banner_id' => array('constraint' => 11, 'type' => 'int'),
			'type' => array('constraint' => 50, 'type' => 'varchar'),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'pergunta_id' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('perguntas');
	}
}