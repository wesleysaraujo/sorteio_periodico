<?php

namespace Fuel\Migrations;

class Add_status_to_promotions
{
	public function up()
	{
		\DBUtil::add_fields('promotions', array(
			'status' => array('constraint' => 16, 'type' => 'varchar'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('promotions', array(
			'status'
		));
	}
}