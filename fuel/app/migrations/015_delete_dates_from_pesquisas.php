<?php

namespace Fuel\Migrations;

class Delete_dates_from_pesquisas
{
	public function up()
	{
		\DBUtil::drop_fields('pesquisas', array(
			'date_start'
,			'date_end'

		));
	}

	public function down()
	{
		\DBUtil::add_fields('pesquisas', array(
			'date_start' => array('constraint' => 11, 'type' => 'int'),
			'date_end' => array('constraint' => 11, 'type' => 'int'),

		));
	}
}