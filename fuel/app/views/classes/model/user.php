<?php

class Model_User extends \Orm\Model
{

	protected static $_has_many = array(
		'pesquisas' => array(
				'key_to' => 'user_id'
		),
		'perguntas' => array(
				'key_to' => 'user_id'
		),
		'banners' => array(
			'key_to' => 'user_id'
		),
		'opcoes' => array(
			'key_to' => 'user_id'
		),
		'promotions' => array(
			'key_to' => 'user_id'
		),

	);

	protected static $_belongs_to = array('user');

	protected static $_properties = array(
		'id',
		'username',
		'password',
		'email',
		'group'
,		'last_login',
		'login_hash',
		'created_at',
		'updated_at',
		'profile_fields'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'users';

}
