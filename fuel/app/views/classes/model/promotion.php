<?php
class Model_Promotion extends \Orm\Model
{
	protected static $_belongs_to = array('user', 'pesquisa' => array('key_from' => 'pesquisa_id')); 

	protected static $_properties = array(
		'id',
		'description',
		'pesquisa_id',
		'date_start',
		'date_end',
		'user_id',
		'created_at',
		'updated_at',
		'status',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('description', 'Descrição', 'required');
		$val->add_field('pesquisa_id', 'Pesquisa', 'required|valid_string[numeric]');
		$val->add_field('date_start', 'Data de início', 'required');
		$val->add_field('date_end', 'Data Final', 'required');

		return $val;
	}

}
