<?php
class Model_Pesquisa extends \Orm\Model
{
	protected static $_belongs_to = array(
		'user' => array(
			'key_from' => 'user_id'
		),
	);

	protected static $_has_many = array(
		'perguntas' => array(
			'key_to' => 'pesquisa_id' 
		),
		//'promotions' => array(
		//	'key_to' => 'pesquisa_id'
		//),
	);

	protected static $_properties = array(
		'id',
		'title',
		'description',
		'date_start',
		'date_end',
		'result',
		'views',
		'user_id',
		'status',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('title', 'Título', 'required|max_length[255]');
		$val->add_field('description', 'Descrição', 'required');
		$val->add_field('date_start', 'Data de início', 'required');
		$val->add_field('date_end', 'Data final', 'required');
		$val->add_field('hour_start', 'Horário inicial', 'required');
		$val->add_field('hour_end', 'Horário final', 'required');
		return $val;
	}

}
