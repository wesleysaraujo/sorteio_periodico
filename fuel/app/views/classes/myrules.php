<?php 
class MyRules
{
    // note this is a static method
    public static function _validation_unique($val, $options)
    {
        list($table, $field) = explode('.', $options);

        $result = DB::select("LOWER (\"$field\")")
        ->where($field, '=', Str::lower($val))
        ->from($table)->execute();

        Validation::active()->set_message('unique', 'Escolha outro :label, pois :value já está sendo usado por outro usuário');

        return ! ($result->count() > 0);
    }

    public static function _validation_majority($val)
    {
        $age     = Date::time_ago(strtotime($val), time(), 'year');

        $age_val = explode(' ', $age); 

        return ! ($age_val[0] < 18);
    }

    // note this is a non-static method
    public function _validation_is_upper($val)
    {
        return $val === strtoupper($val);
    }

}