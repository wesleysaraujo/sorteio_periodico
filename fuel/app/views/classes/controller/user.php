<?php

class Controller_User extends Controller_Base
{
	public function before()
	{
		parent::before();

		if (Request::active()->controller !== 'Controller_User' or ! in_array(Request::active()->action, array('login', 'logout', 'cadastro')))
		{
			if (!Auth::check())
			{
				Response::redirect('user/login');
			}
		}
	}
	
	public function action_index()
	{
		Response::redirect('perfil/'.$this->current_user->id);
	}

	public function action_change_password()
	{
		$data['user'] = Model_User::find($this->current_user->id);

		$val = Validation::forge();

		if(Input::method() == 'POST')
		{	
			$val->add_field('old_password','Informe sua senha atual', 'required');
			$val->add_field('password', 'Nova senha', 'required|min_length[6]');
			$val->add_field('repeat_password', 'Repita a nova senha', 'required|min_length[6]')->add_rule('match_field', 'password');	
			
			if($val->run())
			{
				$change_password = Auth::change_password(Input::post('old_password'), Input::post('password'));
				
				if($change_password)
				{
					Session::set_flash('success', 'Sua senha foi atualizada com sucesso');
					Response::redirect('user');
				}
				else
				{
					Session::set_flash('error', 'Não foir possível atualizar sua senha tente novamente');
				}
			}
			else
			{
				$data['messages'] = $val->error();
			}
		}
		$data['val'] = $val;
		$this->template->title = 'Alterar minha senha - @'.$this->current_user->username;
		$this->template->content = View::forge('user/change_password', $data);
	}

	public function action_perfil($id)
	{
		$data['user'] = Model_User::find($this->current_user->id);

		$val = Validation::forge('user_perfil');

		if (Input::method() == 'POST')
		{
			$val->add_callable('MyRules');
			$val->add_field('full_name', 'Nome completo', 'required|min_length[3]');
			$val->add_field('date_birth', 'Data de nascimento', 'required');
			$val->add_field('number_rg', 'Número do RG', 'required');
			$val->add_field('cpf', 'CPF', 'required|min_length[11]');
			$val->add_field('address', 'Endereço ', 'required');
			$val->add_field('district', 'Bairro ', 'required');
			$val->add_field('number_home', 'Nº ', 'required');
			$val->add_field('complement', 'Complemento', 'required');
			$val->add_field('cep', 'CEP', 'required');
			$val->add_field('city', 'Cidade', 'required');
			$val->add_field('uf', 'Estado', 'required');

			if($val->run())
			{
				$update_user = Auth::update_user(
					array(
						'full_name' => Input::post('full_name'),
						'date_birth' => Input::post('date_birth'),
						'number_rg'	=> Input::post('number_rg'),
						'cpf'		=> Input::post('cpf'),
						'address' 	=> Input::post('address'),
						'district' 	=> Input::post('district'),
						'number_home' 	=> Input::post('number_home'),
						'complement' 	=> Input::post('complement'),
						'cep' 	=> Input::post('cep'),
						'city' 	=> Input::post('city'),
						'uf' 	=> Input::post('uf'),
						'user_status' => 'ativado'
					)
				);
				if($update_user)
				{
					Session::set_flash('success', 'Seu perfil foi atualizado com sucesso');
					Response::redirect('pesquisa');
				}
			}
			else
			{
				$data['messages'] = $val->error();
			}
		}

		if(Auth::get_profile_fields('user_status') == "incompleto")
		{
			Session::set_flash('error', e('Seu perfil está incompleto, por favor termine de preencher seu perfil, para que possa particar das pesquisas e promoções'));
		}
		elseif(Auth::get_profile_fields('user_status') == "aguardando")
		{
			Session::set_flash('error', e('Seu perfil precisa ser ativado para que você continue preenchendo seu perfil, acesse o e-mail que você recebeu de confirmação caso contrário clique aqui e envie um outro e-mail de confirmação!'));
		}
		$data['status'] = Auth::get_profile_fields('user_status');
		$data['val']    = $val;
		$this->template->title = 'Meu perfil: @'.$this->current_user->username;
		$this->template->content = View::forge('user/perfil', $data);
	}

	public function action_cadastro()
	{
		if(Auth::check())
		{
			Output::redirect('/');
		}

		$val = Validation::forge('user_cadastro');

		if (Input::method() == 'POST')
		{
			$val->add_callable('MyRules');
			$val->add_field('full_name', 'Nome completo', 'required|min_length[3]');
			$val->add_field('username', 'Username (Nome de usuário)', 'required|min_length[3]')->add_rule('unique', 'users.username');
			$val->add_field('email', 'E-mail', 'required|valid_email')->add_rule('unique', 'users.email');
			$val->add_field('password', 'Senha', 'required|min_length[6]');
			$val->add_field('password-confirm', 'Confirmar Senha', 'required')->add_rule('match_field', 'password');

			if($val->run())
			{
				$hash 		 = Auth::create_login_hash();

				$username 	 = Input::post('full_name'); 
				$create_user = Auth::create_user(
					Input::post('username'),
					Input::post('password'),
					Input::post('email'),
					0,
					array(
						'full_name' => Input::post('full_name'),
						'register_hash' => $hash,
						'user_status' => 'aguardando',
					)
				);

				if($create_user)
				{
					$mensagem_email = "<h3>Olá ".Input::post('full_name')."</h3>, você acabou de criar um usuário no nosso sistema de pesquisa.<br/>";
					$mensagem_email .= "Para ativar sua conta clique no link abaixo e faça login no sistema.";
					$mensagem_email .= "<p> Nome de usuário: ".Input::post('username')."</p>";
					$mensagem_email .= "<p> E-mail de cadastro: ".Input::post('email')."</p>";
					$mensagem_email .= "<p> Senha: ".Input::post('password')."</p>";
					$mensagem_email .= "<a href=\"http://odebateon-pesquisa.local/confirmar-cadastro/$hash/".Input::post('email')."\">Clique aqui para confirmar seu cadastro</a>";

					$email = Email::forge();
					$email->from('wsadesigner@gmail.com', 'Wesley')->to(Input::post('email'), Input::post('full_name'));
					$email->subject('Confirmação de cadastro de Usuário no ODebateOn Pesquisas');
					$email->body($mensagem_email);

					try
					{
						$email->send();
					}
					catch(\EmailValidationFailedException $e)
					{
						Session::set_flash('error', e('Erro na validação do envio de e-mail.'));
					}
					catch(\EmailSendingFailedException $e)
					{
						Session::set_flash('error', e('Erro no envio do e-mail da confirmação do cadastro'));
					}

					Session::set_flash('success', e('Cadastro realizado com sucesso, você recebeu um e-mail de confirmação para ativar seu cadastro. Não esqueça de verificar sua caixa de spam.'));
					Response::redirect('login-de-acesso');
				}
			}else{
				Session::set_flash('error', e('Erro de cadastro,tente novamente preenchendo todas as informações corretamente.'));
			}
		
		}
		
		$this->template->title = 'Cadastro de usuário';
		$this->template->content = View::forge('user/cadastro', array('val' => $val));
	}

	public function action_login()
	{
		// Already logged in
		Auth::check() and Response::redirect('pesquisa');

		$val = Validation::forge();

		if (Input::method() == 'POST')
		{
			$val->add('email', 'E-mail ou username')
			    ->add_rule('required');
			$val->add('password', 'Senha')
			    ->add_rule('required');

			if ($val->run())
			{
				$auth = Auth::instance();

				// check the credentials. This assumes that you have the previous table created
				if (Auth::check() or $auth->login(Input::post('email'), Input::post('password')))
				{
					// credentials ok, go right in
					if (Config::get('auth.driver', 'Simpleauth') == 'Ormauth')
					{
						$current_user = Model\Auth_User::find_by_username(Auth::get_screen_name());
					}
					else
					{
						$current_user = Model_User::find_by_username(Auth::get_screen_name());
					}
	 
					$url_redirect 	= ( Input::post('destination') != '' ) ?   Input::post('destination') : 'user/perfil/'.$current_user->id;

					Session::set_flash('success', e('Bem vindo, '.$current_user->username));
					Response::redirect($url_redirect);
				}
				else
				{
					$this->template->set_global('login_error', 'Falha de login');
				}
			}
		}

		$this->template->title = 'Login de acessso';
		$this->template->content = View::forge('user/login', array('val' => $val), false);
	}

	public function action_logout()
	{
		Auth::logout();
		Response::redirect('login-de-acesso');
	}

}
