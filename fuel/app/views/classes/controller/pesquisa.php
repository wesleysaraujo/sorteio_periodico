<?php

class Controller_Pesquisa extends Controller_Base
{

	public function before()
	{
		parent::before();

		if (Request::active()->controller !== 'Controller_Pesquisa' or ! in_array(Request::active()->action, array('login', 'logout')))
		{
			if (Auth::check())
			{
				if(Auth::get_profile_fields('user_status') === 'incompleto' OR Auth::get_profile_fields('user_status') === 'aguardando')
				{
					Response::redirect('perfil/'.$this->current_user->id);
				}
			}
			else
			{
				Response::redirect('login-de-acesso');
			}
		}
	}

	public function action_index()
	{
		$this->template->title = 'Pesquisas';
		$this->template->content = View::forge('pesquisa/index');
	}

}
