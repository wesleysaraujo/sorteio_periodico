<?php
class Controller_Admin_Pesquisa extends Controller_Admin{

	public function action_index()
	{
		$data['pesquisas'] = Model_Pesquisa::find('all');
		$this->template->title = "Pesquisas";
		$this->template->content = View::forge('admin/pesquisa/index', $data);

	}

	public function action_view($id = null)
	{
		$data['pesquisa'] = Model_Pesquisa::find($id);
		$data['profile_fields'] = unserialize(html_entity_decode($data['pesquisa']->user->profile_fields, ENT_QUOTES));

		$this->template->title = "Pesquisa";
		$this->template->content = View::forge('admin/pesquisa/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Pesquisa::validate('create');

			if ($val->run())
			{
				$pesquisa = Model_Pesquisa::forge(array(
					'title' => Input::post('title'),
					'description' => Input::post('description'),
					'date_start' => strtotime(Input::post('date_start')." ".Input::post('hour_start')),
					'date_end' => strtotime(Input::post('date_end')." ".Input::post('hour_end')),
					'user_id'  => $this->current_user->id,
					'status'   => 'inativo'
				));

				if ($pesquisa and $pesquisa->save())
				{
					Session::set_flash('success', e('Pesquisa adicionada com sucesso #'.$pesquisa->id.'.'));

					Response::redirect('admin/pesquisa');
				}

				else
				{
					Session::set_flash('error', e('Could not save pesquisa.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Pesquisas";
		$this->template->content = View::forge('admin/pesquisa/create');

	}

	public function action_edit($id = null)
	{
		$pesquisa = Model_Pesquisa::find($id);
		$val = Model_Pesquisa::validate('edit');

		$hora_inicial = explode(' ',date('Y-m-d H:i:s', $pesquisa->date_start));
		$hora_final = explode(' ',date('Y-m-d H:i:s', $pesquisa->date_end));

		$pesquisa->hour_start = $hora_inicial[1];
		$pesquisa->hour_end = $hora_final[1];
		$pesquisa->date_start = $hora_inicial[0]; 
		$pesquisa->date_end = $hora_final[0]; 

		if ($val->run())
		{
			$pesquisa->title = Input::post('title');
			$pesquisa->description = Input::post('description');
			$pesquisa->date_start = strtotime(Input::post('date_start')." ".Input::post('hour_start'));
			$pesquisa->date_end = strtotime(Input::post('date_end')." ".Input::post('hour_end'));
			$pesquisa->status = Input::post('status');

			if ($pesquisa->save())
			{
				Session::set_flash('success', e('Updated pesquisa #' . $id));

				Response::redirect('admin/pesquisa');
			}

			else
			{
				Session::set_flash('error', e('Could not update pesquisa #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$pesquisa->title = $val->validated('title');
				$pesquisa->description = $val->validated('description');
				$pesquisa->date_start = $val->validated('date_start');
				$pesquisa->date_end = $val->validated('date_end');
				$pesquisa->result = $val->validated('hour_start');
				$pesquisa->views = $val->validated('hour_end');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('pesquisa', $pesquisa, false);
		}

		$this->template->title = "Pesquisas";
		$this->template->content = View::forge('admin/pesquisa/edit');

	}

	public function action_delete($id = null)
	{
		if ($pesquisa = Model_Pesquisa::find($id))
		{
			$pesquisa->delete();

			Session::set_flash('success', e('Deleted pesquisa #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete pesquisa #'.$id));
		}

		Response::redirect('admin/pesquisa');

	}


}