<?php
class Controller_Admin_Promotion extends Controller_Admin{

	public function action_index()
	{
		$config = array(
		    'pagination_url' => Uri::base().'admin/promotion/',
		    'total_items'    => Model_Promotion::count(),
		    'per_page'       => 10,
		    'uri_segment'    => 'page',
		    // or if you prefer pagination by query string
		    //'uri_segment'    => 'page',
		);

		$pagination = Pagination::forge('mypagination', $config);

		$data['promotions'] = Model_Promotion::find('all',array('related' => array('pesquisa', 'user'),'offset' => $pagination->offset, 'limit' => $pagination->per_page));

		// we pass the object, it will be rendered when echo'd in the view
		$data['pagination'] = $pagination;

		$this->template->title = "Promoções";
		$this->template->content = View::forge('admin/promotion/index', $data, false);

	}

	public function action_view($id = null)
	{
		$data['promotion'] = Model_Promotion::find($id);

		$this->template->title = "Visualizando promoção";
		$this->template->content = View::forge('admin/promotion/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Promotion::validate('create');

			if ($val->run())
			{
				
				$promotion = Model_Promotion::forge(array(
					'description' => Input::post('description'),
					'pesquisa_id' => Input::post('pesquisa_id'),
					'date_start' => strtotime(Input::post('date_start')." ".Input::post('hour_start')),
					'date_end' => strtotime(Input::post('date_end')." ".Input::post('hour_end')),
					'user_id' => $this->current_user->id,
					'status' => Input::post('status'),
				));

				if ($promotion and $promotion->save())
				{
					Session::set_flash('success', e('Promoção criada com sucesso! ID #'.$promotion->id.'.'));

					Response::redirect('admin/promotion');
				}

				else
				{
					Session::set_flash('error', e('Could not save promotion.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$view = View::forge('admin/promotion/create');
		$view->set_global('pesquisas', Arr::assoc_to_keyval(Model_Pesquisa::find('all', array('whare' => array(array('status', 'ativo')))),'id', 'title'));
		$this->template->title = "Criar promoção";
		$this->template->content = $view;

	}

	public function action_edit($id = null)
	{
		$promotion = Model_Promotion::find($id);
		$val = Model_Promotion::validate('edit');

		$hora_inicial = explode(' ',date('Y-m-d H:i:s', $promotion->date_start));
		$hora_final = explode(' ',date('Y-m-d H:i:s', $promotion->date_end));

		$promotion->hour_start = $hora_inicial[1];
		$promotion->hour_end = $hora_final[1];
		$promotion->date_start = $hora_inicial[0]; 
		$promotion->date_end = $hora_final[0]; 


		if ($val->run())
		{
			$promotion->description = Input::post('description');
			$promotion->pesquisa_id = Input::post('pesquisa_id');
			$promotion->date_start = strtotime(Input::post('date_start')." ".Input::post('hour_start'));
			$promotion->date_end = strtotime(Input::post('date_end')." ".Input::post('hour_end'));
			$promotion->user_id = $this->current_user->id;
			$promotion->status = Input::post('status');

			if ($promotion->save())
			{
				Session::set_flash('success', e('Promoção #' . $id .' atualizada com sucesso'));

				Response::redirect('admin/promotion');
			}

			else
			{
				Session::set_flash('error', e('Could not update promotion #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$promotion->description = $val->validated('description');
				$promotion->pesquisa_id = $val->validated('pesquisa_id');
				$promotion->date_start = $val->validated('date_start');
				$promotion->date_end = $val->validated('date_end');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('promotion', $promotion, false);
		}
		
		$view = View::forge('admin/promotion/edit');
		$view->set_global('pesquisas', Arr::assoc_to_keyval(Model_Pesquisa::find('all', array('whare' => array(array('status', 'ativo')))),'id', 'title'));
		$this->template->title = "Editando promoção";
		$this->template->content = $view;

	}

	public function action_delete($id = null)
	{
		if ($promotion = Model_Promotion::find($id))
		{
			$promotion->delete();

			Session::set_flash('success', e('Deleted promotion #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete promotion #'.$id));
		}

		Response::redirect('admin/promotion');

	}


}