<?php 
class Controller_Admin_Opcoes extends Controller_Admin
{

	public function action_index()
	{
		Response::redirect('admin/pesquisa/');
	}

	public function action_create()
	{
		if(!Input::get('pergunta') or Input::get('pergunta') === null)
		{
			Response::redirect('admin/pesquisa/');
		}

		$id = Crypt::decode(Input::get('pergunta'));
		$data['pergunta'] = Model_Pergunta::find($id);
		$data['opcoes']	= Model_Opcao::find('all', array(
			'where' => array(
				array('pergunta_id', $id),
				),
			'order_by' => array('title' => 'asc'),
			)
		);

		$this->template->title = 'Adicionar Opção para Pergunta';
		$this->template->content = View::forge('admin/opcoes/create', $data);
	}

	public function action_edit($id = null)
	{
		//if(!Input::get('pergunta') or Input::get('pergunta') === null)
		//{
		//	Response::redirect('admin/pesquisa/');
		//}

		$id_pergunta = Crypt::decode(Input::get('pergunta'));
		$data['pergunta'] = Model_Pergunta::find($id_pergunta);

		$opcao = Model_Opcao::find($id);

		$val = Model_Opcao::validate('edit');
		
		if($val->run())
		{
			$opcao->title = Input::post('title');

			if ($opcao->save())
			{
				Session::set_flash('success', e('Opção #' .$id.' alterada com sucesso'));

				Response::redirect('admin/perguntas/view/'.$opcao->pergunta_id);

			}

			else
			{
				Session::set_flash('error', e('Erro ao editar opção #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$opcao->title = $val->validated('title');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('opcao', $opcao, false);
		}

		$this->template->title = 'Adicionar Opção para Pergunta';
		$this->template->content = View::forge('admin/opcoes/edit', $data);
	}

	public function action_create_option()
	{
		$response = Response::forge();

		if(Input::method() !== 'POST' or !Input::is_ajax())
		{
			$response->set_status(400);

			return $response;
		}

		//$val = Model_Opcao::validate('create');
		
		//if($val->run())
		//{
			$opcao = Model_Opcao::forge(array(
				'title' => Input::post('title'),
				'pergunta_id' => Crypt::decode(Input::post('pergunta_id')),
				'user_id'	=> $this->current_user->id
			));

			if($opcao and $opcao->save())
			{
				$response->body(json_encode(array(
					'status' => true,
				)));
			}
			else
			{
				$response->set_status(500);
			}
		//}
		//else
		//{
		//	$response = Response::body(json_encode(array(
		//		'status' => false,
		//		'validation' => array(
		//			'title' => $val->error('title') ? $val->error('title')->get_message('Título da opção não pode ser em branco.') : null,
		//			'pergunta_id' => $val->error('pergunta_id') ? $val->error('pergunta_id')->get_message('Houve algum problema ao relacionar a pergunta com a opção') : null,
		//		),
		//	)));
		//}

		return $response;
	}

	public function action_delete($id = null)
	{
		if ($opcao = Model_Opcao::find($id))
		{
			$pergunta = $opcao->pergunta_id;
			$opcao->delete();

			Session::set_flash('success', e('Opção de resposta excluida com sucesso #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Não foi possível excluir a opção #'.$id));
		}

		Response::redirect('admin/perguntas/view/'.$pergunta);

	}
}
