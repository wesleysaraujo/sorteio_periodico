<?php
class Controller_Admin_Perguntas extends Controller_Admin{

	public function action_index()
	{
		$id = Crypt::decode(Input::get('pesquisa'));
		$data['perguntas'] = Model_Pergunta::find('all', array(
			'where' => array(array('pesquisa_id', $id)),
			'related' => array('banner', 'user', 'pesquisa'),
		));
		$data['pesquisa'] = Model_Pesquisa::find($id);
		$this->template->title = "Perguntas";
		$this->template->content = View::forge('admin/perguntas/index', $data);

	}

	public function action_view($id = null)
	{
		$data['pergunta'] = Model_Pergunta::find($id, array('related' => array('user', 'opcaos', 'pesquisa')));
		
		$this->template->title = "Pergunta";
		$this->template->content = View::forge('admin/perguntas/view', $data, false);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Pergunta::validate('create');

			if ($val->run())
			{
				$pergunta = Model_Pergunta::forge(array(
					'description' => Input::post('description'),
					'banner_id' => Input::post('banner_id'),
					'type' => Input::post('type'),
					'user_id' => $this->current_user->id,
					'pesquisa_id' => Crypt::decode(Input::post('pesquisa_id')),
				));

				if ($pergunta and $pergunta->save())
				{
					Session::set_flash('success', e('Pesquisa #'.$pergunta->id.' adicionada com sucesso'));

					Response::redirect('admin/perguntas/?pesquisa='.Input::post('pesquisa_id').'&slug='.Input::post('slug'));
				}

				else
				{
					Session::set_flash('error', e('Could not save pergunta.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}
		$view = View::forge('admin/perguntas/create');
		$view->set_global('banners', Arr::assoc_to_keyval(Model_Banner::find('all'), 'id', 'description'));
		$this->template->title = "Cadastro de Pergunta";
		$this->template->content = $view;

	}

	public function action_edit($id = null)
	{
		$pergunta = Model_Pergunta::find($id);
		$val = Model_Pergunta::validate('edit');

		if ($val->run())
		{
			$pergunta->description = Input::post('description');
			$pergunta->banner_id = Input::post('banner_id');
			$pergunta->type = Input::post('type');
			$pergunta->user_id = Input::post('user_id');
			$pergunta->pesquisa_id = Input::post('pesquisa_id');

			if ($pergunta->save())
			{
				Session::set_flash('success', e('Pergunta #' .$id.' alterada com sucesso'));

				Response::redirect('admin/perguntas/?pesquisa='.Input::post('pesquisa_id').'&slug='.Input::post('slug'));

			}

			else
			{
				Session::set_flash('error', e('Could not update pergunta #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$pergunta->description = $val->validated('description');
				$pergunta->banner_id = $val->validated('banner_id');
				$pergunta->type = $val->validated('type');
				$pergunta->user_id = $val->validated('user_id');
				$pergunta->pesquisa_id = $val->validated('pesquisa_id');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('pergunta', $pergunta, false);
		}
		$view = View::forge('admin/perguntas/edit');
		$view->set_global('banners', Arr::assoc_to_keyval(Model_Banner::find('all'), 'id', 'title'));
		$this->template->title = "Perguntas";
		$this->template->content = $view;

	}

	public function action_delete($id = null)
	{
		if ($pergunta = Model_Pergunta::find($id))
		{
			$pesquisa = $pergunta->pesquisa->id;
			$pergunta->delete();

			Session::set_flash('success', e('Deleted pergunta #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete pergunta #'.$id));
		}

		Response::redirect('admin/perguntas/?pesquisa='.Crypt::encode($pesquisa));

	}


}