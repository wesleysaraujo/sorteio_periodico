<?php
class Controller_Admin_Parceiros extends Controller_Admin{

	public function action_index()
	{
		$config = array(
		    'pagination_url' => Uri::base().'admin/parceiros/',
		    'total_items'    => Model_Parceiro::count(),
		    'per_page'       => 10,
		    'uri_segment'    => 'page',
		    // or if you prefer pagination by query string
		    //'uri_segment'    => 'page',
		);

		$pagination = Pagination::forge('mypagination', $config);

		$data['parceiros'] = Model_Parceiro::find('all', array('offset' => $pagination->offset, 'limit' => $pagination->per_page));
		
		$data['pagination'] = $pagination;

		$this->template->title = "Parceiros";
		$this->template->content = View::forge('admin/parceiros/index', $data);

	}

	public function action_view($id = null)
	{
		$data['parceiro'] = Model_Parceiro::find($id);

		$this->template->title = "Visualização de Parceiro";
		$this->template->content = View::forge('admin/parceiros/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Parceiro::validate('create');

			if ($val->run())
			{
				$parceiro = Model_Parceiro::forge(array(
					'name' => Input::post('name'),
					'site_url' => Input::post('site_url'),
					'email' => Input::post('email'),
					'description' => Input::post('description'),
					'status' => Input::post('status'),
				));

				if ($parceiro and $parceiro->save())
				{
					Session::set_flash('success', e('Parceiro cadastrado com sucesso #'.$parceiro->id.'.'));

					Response::redirect('admin/parceiros');
				}

				else
				{
					Session::set_flash('error', e('Não foi possivel cadastrar o parceiro.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Cadastro de parceiro";
		$this->template->content = View::forge('admin/parceiros/create', '',false);

	}

	public function action_edit($id = null)
	{
		$parceiro = Model_Parceiro::find($id);
		$val = Model_Parceiro::validate('edit');

		if ($val->run())
		{
			$parceiro->name = Input::post('name');
			$parceiro->site_url = Input::post('site_url');
			$parceiro->email = Input::post('email');
			$parceiro->description = Input::post('description');
			$parceiro->status = Input::post('status');

			if ($parceiro->save())
			{
				Session::set_flash('success', e('Informações do parceiro #' . $id . ' foram atualizadas com sucesso'));

				Response::redirect('admin/parceiros');
			}

			else
			{
				Session::set_flash('error', e('Could not update parceiro #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$parceiro->name = $val->validated('name');
				$parceiro->site_url = $val->validated('site_url');
				$parceiro->email = $val->validated('email');
				$parceiro->description = $val->validated('description');
				$parceiro->status = $val->validated('status');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('parceiro', $parceiro, false);
		}

		$this->template->title = "Editando Parceiro";
		$this->template->content = View::forge('admin/parceiros/edit');

	}

	public function action_delete($id = null)
	{
		if ($parceiro = Model_Parceiro::find($id))
		{
			$parceiro->delete();

			Session::set_flash('success', e('Parceiro excluido com sucesso: #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete parceiro #'.$id));
		}

		Response::redirect('admin/parceiros');

	}


}