<h2>Visualizando pergunta #<?php echo $pergunta->id." - ".$pergunta->description; ?></h2>

<p>
	<strong>Descrição:</strong>
	<?php echo $pergunta->description; ?></p>
<p>
	<strong>Banner:</strong><br>
	<?php if ($pergunta->banner->type != 'adsense'): ?>
		<img src="<?php echo $pergunta->banner->media_url ?>" alt="<?php echo $pergunta->banner->description ?>">
	
	<?php else: ?>
		<?php echo $pergunta->banner->media_url ?>
	<?php endif ?>
</p>
<p>
	<strong>Tipo:</strong>
	<?php echo $tipo = ($pergunta->type === 'fechada') ? "Multipla Escolha": "Respostas abertas"; ?></p>
<p>
	<strong>Criado por:</strong>
	<?php echo $pergunta->user->username; ?></p>
<p>
	<strong>Pesquisa:</strong>
	<?php echo $pergunta->pesquisa->title; ?>
</p>
<?php if ($pergunta->type == 'fechada'): ?>
	<hr>
	<div class="pull-right">
		<?php echo Html::anchor('admin/opcoes/create/?pergunta='.Crypt::encode($pergunta->id), '<i class="glyphicon glyphicon-plus"></i> Adicionar Opções de resposta', array('class' => 'btn btn-success btn-sm', 'id' => 'btn-add-options')) ?>
	</div>
	<h3>Opões para essa pergunta</h3>
	<div class="row">
		<?php foreach ($pergunta->opcao as $opcao): ?>
			<div class="btn-group">
			  <button type="button" class="btn btn-info"><?php echo $opcao->title; ?></button>
			  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
			    <span class="caret"></span>
			    <span class="sr-only">Toggle Dropdown</span>
			  </button>
			  <ul class="dropdown-menu" role="menu">
			    <li><?php echo Html::anchor('admin/opcoes/edit/'.$opcao->id.'?pergunta='.Crypt::encode($opcao->pergunta_id), '<i class="glyphicon glyphicon-pencil"></i> Editar') ?></li>
			    <li><?php echo Html::anchor('admin/opcoes/delete/'.$opcao->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('onclick' => "return confirm('Tem certeza que deseja excluir?')")) ?></li>
			  </ul>
			</div>
		<?php endforeach ?>
	</div>
	<div class="btn-group hidden">	
		<?php echo Html::anchor('admin/opcoes/view', '<i class="glyphicon glyphicon-list"></i> Listar Opções de resposta', array('class' => 'btn btn-info btn-sm', 'id' => 'btn-list-option')) ?>
		
		<input type="hidden" class="pergunta" id="pergunta" name="pergunta" value="<?php echo crypt::encode($pergunta->id) ?>">
	</div>
	<br>
	<hr>
<?php endif ?>
<div class="btn-group">
	<?php echo Html::anchor('admin/perguntas/edit/'.$pergunta->id.'?pesquisa='.Crypt::encode($pergunta->pesquisa_id)."&slug=".$pergunta->pesquisa->title, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning btn-sm')); ?> 
	<?php echo Html::anchor('admin/perguntas/?pesquisa='.Crypt::encode($pergunta->pesquisa_id), 'Voltar', array('class' => 'btn btn-danger btn-sm')); ?>
</div>