<div class="pull-right">
	<?php echo Html::anchor('admin/perguntas/create/?pesquisa='.Crypt::encode($pesquisa->id).'&slug='.$pesquisa->title, '<i class="glyphicon glyphicon-plus"></i> Criar nova Pergunta', array('class' => 'btn btn-success')); ?>
</div>
<h2>
	Perguntas para a pesquisa: <?php echo Html::anchor('admin/pesquisa/view/'.$pesquisa->id, $pesquisa->title) ?>
</h2>
<br>
<?php if ($perguntas): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Pergunta</th>
			<th>Banner Relacionado</th>
			<th>Tipo de pergunta</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($perguntas as $item): ?>		<tr>

			<td><?php echo $item->description; ?></td>
			<td><?php echo $item->banner->title; ?></td>
			<td><?php echo $item->type; ?></td>
			<td>
				<div class="btn-group">
					<?php echo Html::anchor('admin/perguntas/view/'.$item->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-info btn-sm')); ?>
					<?php echo Html::anchor('admin/perguntas/edit/'.$item->id.'?pesquisa='.Crypt::encode($pesquisa->id).'&slug='.$pesquisa->title, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning btn-sm')); ?>
					<?php echo Html::anchor('admin/perguntas/delete/'.$item->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('class' => 'btn btn-danger btn-sm','onclick' => "return confirm('Tem certeza que deseja excluir?')")); ?>
				</div>
			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Perguntas.</p>

<?php endif; ?>
