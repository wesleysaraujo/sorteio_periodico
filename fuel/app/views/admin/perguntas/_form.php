<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Pergunta:', 'description', array('class'=>'control-label')); ?>
			<?php echo Form::input('description', Input::post('description', isset($pergunta) ? $pergunta->description : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Qual sua pergunta?')); ?>

		</div>
		<div class="form-group hidden">
			<?php echo Form::label('Banner da pergunta', 'banner_id', array('class'=>'control-label')); ?>
			
			<br />
			
			<div class="btn-group">	
				<a href="javascript:void();" id="add-banner-pergunta-parceiro" class="btn btn-primary"><i class="glyphicon glyphicon-picture"></i> Banner de Parceiro </a>
				<a href="javascript:void();" id="add-banner-pergunta-google" class="btn btn-danger"><i class="glyphicon glyphicon-usd"></i> Banner de Google Adsense </a>
			</div>
		</div>

		<div class="form-group">
			<?php echo Form::label('Escolha o banner da pergunta', 'banner_id', array('class' => 'control-label')); ?>
			<?php echo Form::select('banner_id', Input::post('banner_id', isset($pergunta) ? $pergunta->banner_id : ''), $banners, array('class' => 'form-control'));?>
		</div>

		<div class="form-group">
			<?php echo Form::label('Tipo de pergunta', 'type', array('class'=>'control-label')); ?>
			<?php echo Form::select('type', Input::post('type', isset($pergunta) ? $pergunta->type : ''), array('' => 'Escolha um tipo','fechada' => 'Multipla escolha', 'aberta' => 'Aberta'), array('class' => 'col-md-4 form-control', 'placeholder'=>'Type')); ?>
		</div>
		<div class="form-group">
			<?php echo Form::hidden('user_id', Input::post('user_id', isset($pergunta) ? $pergunta->user_id : $current_user->id), array('class' => 'col-md-4 form-control', 'placeholder'=>'User id')); ?>
		</div>
		<div class="form-group">
			<?php echo Form::hidden('pesquisa_id', Input::post('pesquisa_id', isset($pergunta) ? $pergunta->pesquisa_id : Input::get('pesquisa')), array('class' => 'col-md-4 form-control', 'placeholder'=>'Pergunta id')); ?>
			<?php echo Form::hidden('slug', Input::get('slug')); ?>
		</div>
		<div class="form-group">

			<label class='control-label'>&nbsp;</label>
			<div class="btn-group">
				<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-success')); ?>
				<?php echo Html::anchor('admin/pesquisa/view/'.Crypt::decode(Input::get('pesquisa')), 'Cancelar', array('class' => 'btn btn-danger')); ?>	
			</div>
			
		</div>
	</fieldset>
<?php echo Form::close(); ?>