<h2>Editing Pergunta</h2>
<br>

<?php echo render('admin/perguntas/_form'); ?>

<div class="btn-group">
	<?php echo Html::anchor('admin/perguntas/view/'.$pergunta->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-info btn-sm')); ?>
	<?php echo Html::anchor('admin/perguntas/?pesquisa='.Crypt::encode($pergunta->pesquisa_id), 'Voltar', array('class' => 'btn btn-danger btn-sm')); ?>	
</div>