<div class="col-md-12">
			<h3>Adicionar novas opções para pergunta: <small><?php echo isset($pergunta) ? $pergunta->description : ''; ?>		<input type="hidden" class="pergunta" id="pergunta" name="pergunta" value="<?php echo crypt::encode($pergunta->id) ?>">
</h3>
			<form action="<?php echo Uri::base(); ?>admin/opcoes/create_option" id="form-opcoes" class="form-horizontal">
				<div class="form-group">
					<label for="title" class="control-label">Título da opção</label>
					<div class="input-group">
						<input type="text" class="form-control" id="title-option" placeholder="Escreva aqui a opção para a resposta">
						<span class="input-group-btn">
					    	<button class="btn btn-primary" id="btn-add-option" type="button"><i class="glyphicon glyphicon-plus"></i> Adicionar!</button>
					    	<input type="hidden" class="pergunta" id="pergunta" name="pergunta" value="<?php echo crypt::encode($pergunta->id) ?>">
					    </span>
					</div>
					<span class="text-danger" id="val-title"></span>
					<span class="text-success" id="success-title"></span>
				</div>
			</form>
			<div class="row">
				<div id="adicionados">
					<h5>Opções para essa pergunta: </h5>
					<?php if(isset($opcoes)):
						foreach($opcoes as $opcao):
					?>
					<span class="label label-success"><?php echo $opcao->title; ?></span>
					<?php endforeach; endif; ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="progress progress-striped active hidden" id="progress">
					  <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
					    <span class="sr-only"></span>
					  </div>
					</div>
				</div>
			</div>
			<hr>
			<?php echo Html::anchor('admin/perguntas/view/'.$pergunta->id, 'Voltar para a pergunta', array('class' => 'btn btn-lg btn-danger')) ?>
		</div>