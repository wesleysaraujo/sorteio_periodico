<div class="col-md-12">
			<h3>Adicionar novas opções para pergunta: <small><?php echo isset($pergunta) ? $pergunta->description : ''; ?>		<input type="hidden" class="pergunta" id="pergunta" name="pergunta" value="<?php echo crypt::encode($pergunta->id) ?>">
</h3>
			<?php echo Form::open(array('class' => 'form-horizontal'));?>
				<div class="form-group">
					<label for="title" class="control-label">Título da opção</label>
					<div class="input-group">
						<?php echo Form::input('title', isset($opcao) ? $opcao->title: '', array('class' => 'form-control', '')) ?>
						<span class="input-group-btn">
							<?php echo Form::submit('submit', 'Editar', array('class'=>'btn btn-success')) ?>
					    	<input type="hidden" class="pergunta" id="pergunta" name="pergunta" value="<?php echo crypt::encode($opcao->pergunta_id) ?>">
					    </span>
					</div>
					<span class="text-danger" id="val-title"><?php //if ($val->error('title')){echo $val->error('title');} ?></span>
					<span class="text-success" id="success-title"></span>
				</div>
			<?php echo Form::close() ?>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="progress progress-striped active hidden" id="progress">
					  <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
					    <span class="sr-only"></span>
					  </div>
					</div>
				</div>
			</div>
			<hr>
			<?php echo Html::anchor('admin/perguntas/view/'.$pergunta->id, 'Voltar para a pergunta', array('class' => 'btn btn-lg btn-danger')) ?>
		</div>