<div class="row">
	<div class="col-md-8">
		<div class="panel panel-info">
			<div class="panel-heading">
				<?php echo $parceiro->name; ?>
			</div>
			<div class="panel-body">
				<?php if ($parceiro->image_url != null): ?>
					<div class="pull-right">
						<?php echo Html::img(Uri::base().$parceiro->image_url, array('alt' => $parceiro->name, 'class' => 'thumbnail')); ?>
					</div>
				<?php endif ?>
				<p>
					<strong>Site url:</strong> 
					<?php echo Html::anchor($parceiro->site_url, $parceiro->site_url, array('target' => '_blank')); ?></p>
				<p>
					<strong>Email:</strong> 
					<?php echo $parceiro->email; ?></p>
				<p>
					<strong>Descrição:</strong> 
					<?php echo $parceiro->description; ?></p>
				<p>
			</div>
		</div>	
	</div>
	<div class="col-md-4">
		<ul class="list-group">
			<li class="list-group-item"><strong>Status:</strong> <?php echo $parceiro->status = ($parceiro->status == 'ativo') ? '<span class="label label-success">'.Inflector::humanize($parceiro->status).'</span>' : '<span class="label label-danger">'.Inflector::humanize($parceiro->status).'</span>'; ?></li>
			<li class="list-group-item"><strong>Clicks: </strong> <?php echo count($clicks) ?></li>
			<li class="list-group-item"><strong>Cadastrado em: </strong> <?php echo date('d/m/Y H:i:s', $parceiro->created_at); ?></li>
			<li class="list-group-item"><strong>Última alteração: </strong><?php echo date('d/m/Y H:i:s', $parceiro->updated_at) ?></li>
		</ul>
	</div>
</div>
<div class="btn-group">
	<?php echo Html::anchor('admin/parceiros/edit/'.$parceiro->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning')); ?>
	<?php echo Html::anchor('admin/parceiros', 'Voltar', array('class' => 'btn btn-danger')); ?>
</div>