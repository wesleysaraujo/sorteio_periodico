<div class="pull-right">
	<?php echo Html::anchor('admin/parceiros/create', '<i class="glyphicon glyphicon-plus"></i> Cadastrar novo Parceiro', array('class' => 'btn btn-success')); ?>
</div>
<h2>Parceiros</h2>
<br>
<?php if ($parceiros): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Nome</th>
			<th>Site</th>
			<th>E-mail</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($parceiros as $item): ?>		<tr>

			<td><?php echo $item->name; ?></td>
			<td><?php echo $item->site_url; ?></td>
			<td><?php echo $item->email; ?></td>
			<td><?php echo $item->status; ?></td>
			<td>
				<div class="btn-group">
					<?php echo Html::anchor('admin/parceiros/view/'.$item->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-sm btn-info')); ?>
					<?php echo Html::anchor('admin/parceiros/edit/'.$item->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-sm btn-warning')); ?>
					<?php echo Html::anchor('admin/parceiros/delete/'.$item->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('class' => 'btn btn-sm btn-danger','onclick' => "return confirm('Tem certeza que deseja excluir?')")); ?>
				</div>
			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>
<?php echo isset($pagination) ? $pagination : ''; ?>
<?php else: ?>
<p>Nenhum Parceiro cadastrado.</p>

<?php endif; ?>