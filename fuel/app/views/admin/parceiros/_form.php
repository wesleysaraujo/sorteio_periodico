<?php echo Form::open(array("class"=>"form-horizontal", "enctype" => "multipart/form-data")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Nome', 'name', array('class'=>'control-label')); ?>

				<?php echo Form::input('name', Input::post('name', isset($parceiro) ? $parceiro->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Nome do parceiro')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Url do site', 'site_url', array('class'=>'control-label')); ?>

				<?php echo Form::input('site_url', Input::post('site_url', isset($parceiro) ? $parceiro->site_url : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Informe a URL do site com http://')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('E-mail', 'email', array('class'=>'control-label')); ?>

				<?php echo Form::input('email', Input::post('email', isset($parceiro) ? $parceiro->email : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'E-mail')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Descrição', 'description', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('description', Input::post('description', isset($parceiro) ? $parceiro->description : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Você pode fornecer uma descrição para esse parceiro')); ?>
		</div>
		<?php if (Uri::segment(3) === 'create' or isset($parceiro->image_url) and $parceiro->image_url == null): ?>
			<div class="form-group">
				<?php echo Form::label('Foto/Logo', 'image_url', array('class' => 'control-label')) ?>
				<?php echo Form::file('image') ?>
			</div>
		<?php endif ?>
		<div class="form-group">
			<?php echo Form::label('Status', 'status', array('class'=>'control-label')); ?>
			<div class="row">
				<div class="col-md-4">
					<?php echo Form::select('status', Input::post('status', isset($parceiro) ? $parceiro->status : ''), array('ativo' => 'Ativo', 'inativo' => 'Inativo') ,array('class' => 'col-md-4 form-control')); ?>
				</div>
			</div>	
		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<div class="btn-group">
				<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-primary')); ?>		
				<?php echo Html::anchor('admin/parceiros', 'Cancelar', array('class' => 'btn btn-danger')); ?>
				<?php if (Uri::segment(3) == 'edit'): ?>
					<?php echo Html::anchor('admin/parceiros/view/'.$parceiro->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-info')); ?>
				<?php endif ?>
			</div>
		</div>
	</fieldset>
<?php echo Form::close(); ?>