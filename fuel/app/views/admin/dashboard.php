<div class="jumbotron">
	<h1>Bem vindo, <?php echo Auth::get_profile_fields('full_name'); ?>!</h1>
	<p>Esta é sua área administrativa, aqui você poderá controlar e gerenciar todos os recursos do sistema. </p>
	<p><a class="btn btn-primary btn-lg" href="http://docs.fuelphp.com">Em caso de dúvidas entre em contato</a></p>
</div>
<div class="row">
	<div class="col-md-4">
		<ul class="list-group">
			<li class="list-group-item active"><h4>Seus dados</h4></li>
			<li class="list-group-item"><strong>Username: </strong> <?php echo $current_user->username; ?></li>
			<li class="list-group-item"><strong>Útimo acesso: </strong> <?php echo date('d/m/Y H:i:s', $current_user->last_login) ?></li>
			<li class="list-group-item"><strong>Nome: </strong> <?php echo Auth::get_profile_fields('full_name'); ?></li>
			<li class="list-group-item"><strong>E-mail: </strong> <?php echo $current_user->email; ?></li>
		</ul>
	</div>
	<div class="col-md-4">
		<ul class="list-group">
			<li class="list-group-item active"><h4>Estatísticas</h4></li>
			<li class="list-group-item"><strong>Promoções cadastradas: </strong> <?php echo $qtd_promocoes; ?></li>
			<li class="list-group-item"><strong>Promoções ativas: </strong> <?php echo count($promocoes_ativas); ?></li>
			<li class="list-group-item"><strong>Usuários cadastrados: </strong> <?php echo $users_cadastrados; ?></li>
			<li class="list-group-item"><strong>Usuários Ativados: </strong> <?php echo count($users_ativos); ?></li>
		</ul>
	</div>
	<div class="col-md-4">
		
	</div>
</div>