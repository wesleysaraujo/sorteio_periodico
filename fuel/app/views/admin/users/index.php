<?php if ($current_user->group != 100): ?>
	<div class="alert">
		<p>Você não pode acessar essa área.</p>
	</div>	
<?php else: ?>

<div class="pull-right">
	<?php echo Html::anchor('admin/users/create', '<i class="glyphicon glyphicon-plus"></i> Adicionar Usuário Novo', array('class' => 'btn btn-success')) ?>
</div>
<h3>Usuários cadastrados <small>(<?php echo count($users); ?>)</small></h3>
<br>
<?php if(count($users) > 0): ?>
<table class="table table-striped table-bordered">
	<thead>
		<th>Username</th>
		<th>E-mail</th>
		<th>Grupo</th>
		<th>Status</th>
		<th>Último Login</th>
		<th>Gerenciar</th>
	</thead>
	<tbody>
	<?php 
		foreach ($users as $user):
		$profile_fields = unserialize(html_entity_decode($user->profile_fields, ENT_QUOTES));
		switch (@$profile_fields['user_status']) {
			case 'incompleto':
				$status = '<span class="label label-warning">'.Inflector::humanize(@$profile_fields['user_status']).'</span>';
				break;
			case 'aguardando':
				$status = '<span class="label label-danger">'.Inflector::humanize(@$profile_fields['user_status']).'</span>';
			break;
			default:
				$status = '<span class="label label-success">'.Inflector::humanize(@$profile_fields['user_status']).'</span>';
			break;
		}
	?>
		<tr>
			<td><?php echo $user->username ?></td>
			<td><?php echo $user->email ?></td>
			<td><?php echo Auth::group('Simplegroup')->get_name($user->group) ?></td>
			<td><?php echo $status ?></td>
			<td><?php echo date('d/m/Y H:i:s', $user->last_login) ?></td>
			<td>
				<div class="btn-group">
					<?php echo Html::anchor('admin/users/view/'.$user->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-info btn-sm')); ?>
					<?php echo Html::anchor('admin/users/edit/'.$user->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning btn-sm')); ?>
					<?php echo Html::anchor('admin/users/delete/'.$user->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Tem certeza que deseja excluir?');")); ?>
				</div>
			</td>
		</tr>
	<?php endforeach ?>
	</tbody>
</table>

<?php echo $pagination; ?>

<?php else: ?>
	<span class="alert alert-info">Nenhum usuário cadastrado</span>	
<?php endif ?>
<?php endif ?>