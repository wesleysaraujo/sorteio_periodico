<?php echo Form::open() ?>
		<?php if (Uri::segment(3) == 'create'): ?>
		<div class="row">
			<div class="col-md-4 form-group">
				<?php echo Form::label('Nome Completo: ','full_name', array('class' => 'control-label')) ?>
				<?php echo Form::input('full_name', Input::post('full_name'), array('class' => 'form-control')) ?>
			</div>
			
			<div class="col-md-4 form-group">
				<?php echo Form::label('E-mail: ', 'email', array('class' => 'control-label')) ?>
				<?php echo Form::input('email', Input::post('email'), array('class' => 'form-control')) ?>
			</div>

			<div class="col-md-4 form-group">
				<?php echo Form::label('Username ', 'username', array('class' => 'control-label')) ?>
				<?php echo Form::input('username', Input::post('username'), array('class' => 'form-control')) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 form-group">
				<?php echo Form::label('Senha: ', 'password', array('class' => 'control-label')) ?>
				<?php echo Form::password('password', Input::post('password'), array('class' => 'form-control')) ?>
			</div>
			<div class="col-md-4 form-group">
				<?php echo Form::label('Repetir Senha: ', 'password-confirm', array('class' => 'control-label')) ?>
				<?php echo Form::password('password-confirm', Input::post('password-confirm'), array('class' => 'form-control')) ?>
			</div>
		</div>			
		<?php else: ?>
		<h3>Informações Pessoais</h3>
		<div class="row">
			<div class="col-md-4 form-group">
				<?php echo Form::label('Nome Completo: ', 'full_name', array('class' => 'control-label')) ?>
				<?php echo Form::input('full_name', 'full_name', array('class' => 'form-control')) ?>
			</div>
			
			<div class="col-md-3 form-group">
				<?php echo Form::label('Data de nascimento: ', 'date_birth', array('class' => 'control-label')) ?>
				<?php echo Form::input('date_birth', 'date_birth', array('class' => 'form-control date-birth')) ?>
			</div>

			<div class="col-md-3 form-group">
				<?php echo Form::label('Nº de Identidade: ', 'number_rg', array('class' => 'control-label')) ?>
				<?php echo Form::input('number_rg', 'number_rg', array('class' => 'form-control number_rg')) ?>
			</div>
			
			<div class="col-md-2 form-group">
				<?php echo Form::label('CPF: ', 'cpf', array('class' => 'control-label')) ?>
				<?php echo Form::input('cpf', 'cpf', array('class' => 'form-control cpf')) ?>
			</div>
		</div>
		<hr>
		<h3>Informações de Endereço</h3>
		<div class="row">
			<div class="col-md-5 form-group">
				<?php echo Form::label('Endreço: ', 'address', array('class' => 'control-label')) ?>
				<?php echo Form::input('address', 'address', array('class' => 'form-control')) ?>
			</div>

			<div class="col-md-3 form-group">
				<?php echo Form::label('Bairro: ','district', array('class' => 'control-label')); ?>
				<?php echo Form::input('district', 'district', array('class' => 'form-control')) ?>
			</div>

			<div class="col-md-2 form-group">
				<?php echo Form::label('Número: ','number_home', array('class' => 'control-label')); ?>
				<?php echo Form::input('number_home', 'number_home', array('class' => 'form-control')) ?>
			</div>

			<div class="col-md-2 form-group">
				<?php echo Form::label('Complemento: ', 'complement', array('class' => 'control-label')); ?>
				<?php echo Form::input('complement', 'complement', array('class' => 'form-control')) ?>
			</div>

			<div class="col-md-3 form-group">
				<?php echo Form::label('Cidade: ', 'city', array('class' => 'control-label')); ?>
				<?php echo Form::input('city', '', array('class' => 'form-control')) ?>
			</div>
			<div class="col-md-3 form-group">
				<?php echo Form::label('CEP: ', 'cep', array('class' => 'control-label')); ?>
				<?php echo Form::input('cep', '', array('class' => 'form-control cep')) ?>
			</div>

			<div class="col-md-3 form-group">
				<?php echo Form::label('Estado: ', 'uf', array('class' => 'control-label')); ?>
				<?php echo Form::select('uf', '', array(
						""	=>"Escolha o estado",
						"AC"=>"Acre",
						"AL"=>"Alagoas",
						"AM"=>"Amazonas",
						"AP"=>"Amapá",
						"BA"=>"Bahia",
						"CE"=>"Ceará",
						"DF"=>"Distrito Federal",
						"ES"=>"Espírito Santo",
						"GO"=>"Goiás",
						"MA"=>"Maranhão",
						"MT"=>"Mato Grosso",
						"MS"=>"Mato Grosso do Sul",
						"MG"=>"Minas Gerais",
						"PA"=>"Pará",
						"PB"=>"Paraíba",
						"PR"=>"Paraná",
						"PE"=>"Pernambuco",
						"PI"=>"Piauí",
						"RJ"=>"Rio de Janeiro",
						"RN"=>"Rio Grande do Norte",
						"RO"=>"Rondônia",
						"RS"=>"Rio Grande do Sul",
						"RR"=>"Roraima",
						"SC"=>"Santa Catarina",
						"SE"=>"Sergipe",
						"SP"=>"São Paulo",
						"TO"=>"Tocantins"
					),
					array('class' => 'form-control')) ?>
			</div>
		</div>
		<?php endif; ?>
		<hr>
		<div class="row">
			<div class="col-md-3 form-group">
				<?php echo Form::label('Grupo de Usuário', 'group', array('class' => 'control-label')) ?>
				<?php echo Form::select('group', Input::post('group', isset($user) ? $user->id:''), array('0' => 'Usuário', '1' => 'Promotor', '50' => 'Pesquisador', '100' => 'Administradr'), array('class' => 'form-control')) ?>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="btn-group">
				<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-lg btn-primary')) ?>
				<?php echo Html::anchor('admin/users/', 'Cancelar', array('class' => 'btn btn-lg btn-danger')) ?>
			</div>
		</div>
		<?php echo Form::close() ?>