<div class="row">
	<div class="col-md-7">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3><?php echo $user->username." (# $user->id)" ?></h3>
			</div>
			<div class="panel-body">
				<p>
					<strong>Nome Completo: </strong>
					<?php echo $profile_fields['full_name'] ?>
				</p>
				<p>
					<strong>Data de nascimento: </strong>
					<?php echo isset($profile_fields['date_birth']) ? $profile_fields['date_birth'] : '' ?>
				</p>
				<p>
					<strong>Nº RG: </strong>
					<?php echo isset($profile_fields['number_rg']) ? $profile_fields['number_rg'] : '' ?>
				</p>
				<p>
					<strong>CPF: </strong>
					<?php echo isset($profile_fields['cpf']) ? $profile_fields['cpf'] : '' ?>
				</p>
				<hr>
				<h4>Informações de Endereço</h4>
				<p>
					<strong>Endereço: </strong>
					<?php echo isset($profile_fields['address']) ? $profile_fields['address'] : '' ?>
					<strong>Nº: </strong>
					<?php echo isset($profile_fields['number_home']) ? $profile_fields['number_home'] : '' ?>
				</p>
				<p>
					<strong>Complemento: </strong>
					<?php echo isset($profile_fields['complement']) ? $profile_fields['complement'] : '' ?>
				</p>
				<p>
					<strong>Bairro: </strong>
					<?php echo isset($profile_fields['district']) ? $profile_fields['district'] : '' ?>
				</p>
				<p>
					<strong>CEP: </strong>
					<?php echo isset($profile_fields['cep']) ? $profile_fields['cep'] : '' ?>
				</p>
				<p>
					<strong>Cidade: </strong>
					<?php echo isset($profile_fields['city']) ? $profile_fields['city'] : '' ?>
				</p>
				<p>
					<strong>Estado: </strong>
					<?php echo isset($profile_fields['uf']) ? $profile_fields['uf'] : '' ?>
				</p>
				

			</div>
		</div>
	</div>
	<div class="col-md-5">
		<?php 
			switch (@$profile_fields['user_status']) {
				case 'incompleto':
					$status = '<span class="label label-warning">'.Inflector::humanize(@$profile_fields['user_status']).'</span>';
				break;
				case 'aguardando':
					$status = '<span class="label label-danger">'.Inflector::humanize(@$profile_fields['user_status']).'</span>';
				break;
				default:
					$status = '<span class="label label-success">'.Inflector::humanize(@$profile_fields['user_status']).'</span>';
					break;
			}
		?>
		<ul class="list-group">
			<li class="list-group-item"><strong>Cadastrado em: </strong><?php echo date('d/m/Y H:i:s', $user->created_at) ?></li>
			<li class="list-group-item"><strong>Última alteração: </strong><?php echo date('d/m/Y H:i:s', $user->updated_at) ?></li>
			<li class="list-group-item"><strong>Última login: </strong><?php echo date('d/m/Y H:i:s', $user->last_login) ?></li>
			<li class="list-group-item"><strong>Grupo: </strong><?php echo Auth::group('Simplegroup')->get_name($user->group) ?></li>
			<li class="list-group-item"><strong>Status: </strong><?php echo $status ?></li>
		</ul>
	</div>
</div>
<hr>
<div class="btn-group">
	<?php echo Html::anchor('admin/users/edit/'.$user->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning')) ?>
	<?php echo Html::anchor('admin/users/', 'Voltar', array('class' => 'btn btn-danger')) ?>		
</div>