<div class="row">
	<div class="col-md-7">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $promotion->title ?> # <?php echo $promotion->id ?></h3>
			</div>
			<div class="panel-body">
				<p>
					<strong>Description:</strong><br>
					<?php echo $promotion->description; ?>
				</p>
				<p>
					<strong>Início: </strong><?php echo date('d/m/Y H:i:s', $promotion->date_start); ?>
				</p>
				<p>
					<strong>Fim: </strong><?php echo date('d/m/Y H:i:s', $promotion->date_end); ?>
				</p>
				<p>
					<strong>Situação atual: </strong><?php echo $situacao = (date('Y-m-d H:i:s', $promotion->date_end) > date('Y-m-d H:i:s')) ? '<span class="label label-success">Aberta</span>' : '<span class="label label-danger">Encerrada</span>' ?>
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<ul class="list-group">
			<li class="list-group-item"><strong>Pesquisa:</strong> <?php echo Html::anchor('admin/pesquisa/view/'.$promotion->pesquisa->id, $promotion->pesquisa->title, array('class' => 'btn btn-link')); ?></li>
			<li class="list-group-item"><strong>Criado em:</strong> <?php echo date('d/m/Y H:i:s', $promotion->created_at); ?></li>
			<li class="list-group-item"><strong>Útima alteração:</strong> <?php echo date('d/m/Y H:i:s', $promotion->updated_at); ?></li>
			<li class="list-group-item"><strong>Criado por:</strong> <?php echo $promotion->user->username; ?></li>
		</ul>
	</div>
</div>
<div class="row">
	<div class="col-md-7">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h2 class="panel-title">Brindes dessa promoção</h2>
			</div>
			<div class="panel-body">
				<div class="pull-right">
					<?php echo Form::hidden('id_promotion', $promotion->id) ?>
					<?php echo Form::hidden('base_url', Uri::base(), array('class' => 'base-url')) ?>
					<?php echo Html::anchor('admin/brindes/create/'.$promotion->id, 'Adicionar Brinde a Essa Promoção', array('class' => 'btn btn-sm btn-primary', 'id' => 'btn-add-brinde-promo')) ?>
				</div>
				<h4>Adicionados</h4>
				<div id="content-brindes">
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<div class="progress progress-striped active hidden" id="progress">
							  <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							    <span class="sr-only"></span>
							  </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="btn-group">
	<?php echo Html::anchor('admin/promotion/edit/'.$promotion->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning')); ?>
	<?php echo Html::anchor('admin/promotion', 'Voltar', array('class' => 'btn btn-danger')); ?>
</div>