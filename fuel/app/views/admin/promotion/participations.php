<div class="row">
	<div class="well">
		<?php if (time() >= $promotion->date_end and count($participations) >= count($promotion->brindes)): ?>
			<?php if (count($raffle) < 1): ?>
				<div class="pull-right">
					<?php echo Html::anchor('admin/promotion/raffle/'.$promotion->id, 'Realizar Sorteio', array('class' => 'btn btn-success')) ?>
				</div>
			<?php else: ?>
				<div class="pull-right">
					<?php echo Html::anchor('#ganhadores', 'Resultado do Sorteio', array('class' => 'btn btn-info')) ?>
				</div>
			<?php endif ?>
			
		<?php endif ?>
		<h1>Relatório Sintético</h1>
		<h3><i><?php echo $promotion->description ?></i></h3>
		<p><strong>Pesquisa: </strong> <?php echo $promotion->pesquisa->title ?></p>
		<p><strong>Quantidade de perguntas: </strong> <?php echo count($promotion->pesquisa->perguntas) ?></p>
		<h3>Prêmios (<?php echo count($promotion->brindes) ?>):</h3>
		<ul class="list-group">
		<?php foreach ($promotion->brindes as $brinde): ?>
			<li class="list-group-item"><?php echo $brinde->name ?> (<?php echo $brinde->amount ?>)</li>
		<?php endforeach ?>
		</ul>
		<hr>
		<h3>Período:</h3>
		<p class="text-primary"><strong>Data de início: </strong> <?php echo date('d/m/Y H:i:s', $promotion->date_start) ?></p>
		<p class="text-danger"><strong>Data de término: </strong> <?php echo date('d/m/Y H:i:s', $promotion->date_end) ?></p>
		<?php 
			if (time() > $promotion->date_end) {
				echo "<p class='text-danger'>Terminou em ".date('d/m/Y H:i:s',$promotion->date_end)."</p>";
			}
			elseif (time() >= $promotion->date_start) {
				echo '<p class="text-success">Aberto (Em andamento)</p>';
			}
			else
			{
				echo '<p class="text-info">Promoção ficará disponível para participação em '.date('d/m/Y H:i:s',$promotion->date_start).'</p>';
			}
		?>
		<hr>
		<h3>Estatísticas de participantes: </h3>
		<p>Total de participantes: <span class="text-success"><?php echo count($participations) ?></span></p>
		<p>Novos participantes no período: <span class="text-info"><?php echo count($new_users) ?></span></p>
		<p>
			Percentual de novos participantes:
			<?php 
				$total = count($participations);
				$novos = count($new_users) * 100;
				$percent = $novos/$total; 
			?> 
			<span class="text-primary"><?php echo $percent ?>%</span>
		</p>
	</div>
</div>
<div class="row well">
		<div class="<?php echo $col = (count($raffle) > 0) ? "col-md-6" : "col-md-12" ?>">
			<h3>Participações (<?php echo count($participations) ?>):</h3>
			<?php if (count($participations) > 0): ?>
				<table class="table table-bordered table-striped">
					<thead>
						<th>Usuário</th>
						<th>Data da partipação</th>
					</thead>
					<tbody>
						<?php foreach ($participations as $participation): ?>
							<tr>
								<td><?php echo $participation->user->username." (".$participation->user->email.")" ?></td>
								<td><?php echo date('d/m/Y H:i:s',$participation->created_at) ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			<?php endif ?>
		</div>
		<?php if (count($raffle) > 0): ?>
			<div class="col-md-6" id="ganhadores">
				<h3>Ganhadores (<?php echo count($winners) ?>) <small>Sorteio realizado em <?php echo date('d/m/Y H:i:s', $raffle->created_at) ?></small></h3>
				<?php if (count($winners) > 0): ?>
					<table class="table table-bordered table-striped">
						<thead>
							<th>Ganhador</th>
							<th>Brinde</th>
						</thead>
						<tbody>
							<?php foreach ($winners as $winner): ?>
								<tr>
									<td><?php echo Html::anchor('admin/users/view/'.$winner->user->id, $winner->user->username." (".$winner->user->email.")") ?></td>
									<td><?php echo $winner->brinde->name." (".$winner->brinde->amount.")" ?></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				<?php endif ?>
			<div class="pull-right">
					<a class="btn btn-danger btn-sm" data-toggle="modal" href="#myModal"><i class="glyphicon glyphicon-envelope"></i> Enviar e-mail para os ganhadores</a>
				</div>
			</div>
		<?php endif ?>
</div>
<?php echo Html::anchor('admin/promotion/', 'Voltar', array('class' => 'btn btn-danger')) ?>

<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">E-mail de aviso para os ganhadores</h4>
      </div>
      <div class="modal-body">
        <form role="form" id="form-send-email">
        	<fieldset>
        		<div class="form-group">
        			<input type="hidden" name="promotion" value="<?php echo $promotion->id ?>">
        			<label for="ganhadores" class="control-label">Vencedores</label>
        			<div>
        				<?php foreach ($winners as $winner): ?>
		        			<span class="label label-info"><?php echo $winner->user->email ?></span>
		        		<?php endforeach ?>
        			</div>
        		</div>
        		<div class="form-group">
        			<label class="control-label" for="input01">Assunto</label>
        			<input type="text" class="input-xlarge form-control" id="assunto" name="assunto">
        		</div>
        		<div class="form-group">
        			<label class="control-label" for="textarea">Mensagem</label>
        			<textarea class="input-xlarge form-control" id="mensagem" name="mensagem" rows="10"></textarea>
        		</div>
        	</fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary btn-send-email-winners" >Enviar e-mail</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->