<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Título da Promoção', 'title', array('class'=>'control-label')); ?>
			<?php echo Form::input('title', Input::post('title', isset($promotion) ? $promotion->title : ''), array('class' => 'col-md-4 form-control', 'required', 'placeholder'=>'Informe um Título para essa promoção')); ?>						
		</div>
		<div class="form-group">
			<?php echo Form::label('Descrição', 'description', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('description', Input::post('description', isset($promotion) ? $promotion->description : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Descreva a promoção aqui')); ?>
		</div>
		<div class="form-group">
			<?php echo Form::label('Pesquisa', 'pesquisa_id', array('class'=>'control-label')); ?>

				<?php echo Form::select('pesquisa_id', Input::post('pesquisa_id', isset($promotion) ? $promotion->pesquisa_id : ''), $pesquisas, array('class' => 'form-control')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Data de Início da pesquisa', 'date_start', array('class'=>'control-label')); ?>
			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				<?php echo Form::input('date_start', Input::post('date_start', isset($promotion) ? $promotion->date_start : ''), array('class' => 'col-md-4 form-control', 'required', 'placeholder'=>'Informe a data de início', 'id' => 'data-inicio', 'data-date-format'=> 'yyyy-mm-dd')); ?>						
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i> Horário de Início</span>
				<?php echo Form::input('hour_start', Input::post('hour_start', isset($promotion) ? $promotion->hour_start : ''), array('class' =>'form-control horario-inicio', 'required', 'placeholder' => 'Informe um horário de início da pesquisa')) ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo Form::label('Data de termino da pesquisa', 'date_end', array('class'=>'control-label')); ?>
			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				<?php echo Form::input('date_end', Input::post('date_end', isset($promotion) ? $promotion->date_end : ''), array('class' => 'col-md-4 form-control', 'required', 'placeholder'=>'Informe a data Final','id' => 'data-final', 'data-date-format'=> 'yyyy-mm-dd')); ?>
			</div>
		</div>

		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i> Horário de Término</span>
				<?php echo Form::input('hour_end', Input::post('hour_end', isset($promotion) ? $promotion->hour_end : ''), array('class' =>'form-control horario-fim', 'required', 'placeholder' => 'Informe um horário de término da pesquisa')) ?>
			</div>
		</div>

		<div class="form-group">
			<?php echo Form::label('Status','status', array('class' => 'control-label')) ?>
			<?php echo Form::select('status', Input::post('status', isset($promotion) ? $promotion->status : 'inativo'), array('ativo' => 'Ativo', 'inativo' => 'Inativo'), array('class' => 'form-control')) ?>
		</div>

		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<div class="btn-group">
				<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-primary')); ?>
				<?php echo Html::anchor('admin/promotion', 'Cancelar', array('class' => 'btn btn-danger')); ?>
				<?php if (Uri::segment(3) === 'edit'): ?>
					<?php echo Html::anchor('admin/promotion/view/'.$promotion->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-info')); ?>			
				<?php endif ?>		
			</div>
		</div>
		


	</fieldset>
<?php echo Form::close(); ?>