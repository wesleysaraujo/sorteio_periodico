<div class="pull-right">
	<?php echo Html::anchor('admin/promotion/create', '<i class="glyphicon glyphicon-plus"></i> Criar nova promoção', array('class' => 'btn btn-success')); ?>
</div>
<h2>Lista de Promoções</h2>
<br>
<?php if ($promotions): ?>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Promoção</th>
			<th>Pesquisa</th>
			<th>Início da Promoção</th>
			<th>Fim da Promoção</th>
			<th>Status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($promotions as $item): ?>		<tr>

			<td><?php echo $item->title; ?></td>
			<td><?php echo $item->pesquisa->title; ?></td>
			<td><?php echo date('d/m/Y H:i:s', $item->date_start); ?></td>
			<td><?php echo date('d/m/Y H:i:s', $item->date_end); ?></td>
			<td><?php echo $item->status = ($item->status === 'ativo') ? '<span class="label label-success">'.Inflector::humanize($item->status).'</span>' : '<span class="label label-danger">'.Inflector::humanize($item->status).'</span>' ?></td>
			<td>
				<div class="btn-group">
					<?php echo Html::anchor('admin/promotion/view/'.$item->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-info btn-sm')); ?>
					<?php echo Html::anchor('admin/promotion/edit/'.$item->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning btn-sm')); ?>
					<?php echo Html::anchor('admin/promotion/delete/'.$item->id, '<i class="glyphicon glyphicon-trash"></i> Delete', array('class' => 'btn btn-danger btn-sm','onclick' => "return confirm('Are you sure?')")); ?>
					<?php echo Html::anchor('admin/promotion/participations/'.$item->id, '<i class="glyphicon glyphicon-check"></i> Participações', array('class' => 'btn btn-primary btn-sm')) ?>
				</div>
			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>Não existe nenhuma promoção cadastrada.</p>

<?php endif; ?>
<?php echo isset($pagination) ? $pagination : ''; ?>