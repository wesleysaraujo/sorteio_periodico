<div class="row">
	<div class="col-md-8">
		<div class="panel panel-info">
			<div class="panel-heading">
				Banner <?php echo $banner->title; ?> #<?php echo $banner->id; ?>
			</div>
			<div class="panel-body">
				<p><?php echo $banner->description; ?></p>
				<?php if ($banner->type == 'adsense'): ?>
					<?php echo $banner->media_url; ?>
				<?php else: ?>
					<a href="<?php echo $banner->url ?>" target="_blank"><img src="<?php echo Uri::base().$banner->media_url; ?>" alt="<?php echo $banner->title ?>"></a>
				<?php endif ?>
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<ul class="list-group">
			<li class="list-group-item">
				<strong>Cadastrado por: </strong><?php echo $banner->user->username; ?>
			</li>
			<li class="list-group-item">
				<strong>Tipo de banner: </strong><?php echo $banner->type; ?>
			</li>
			<li class="list-group-item">
				<strong>Visualizações: </strong><?php echo $banner->views; ?>
			</li>
			<li class="list-group-item">
				<strong>Url: </strong><?php echo Html::anchor($banner->url, 'Clique aqui', array('target' => '_blank')); ?>
			</li>
		</ul>
	</div>
</div>
<div class="btn-group">
	<?php echo Html::anchor('admin/banners/edit/'.$banner->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning btn-sm')); ?>
	<?php echo Html::anchor('admin/banners', 'Volar', array('class' => 'btn btn-danger btn-sm')); ?>
</div>