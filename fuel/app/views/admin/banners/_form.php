<?php echo Form::open(array("class"=>"form-horizontal", 'id' => 'form_banners', 'enctype' => 'multipart/form-data')); ?>
<?php if (isset($val)): ?>
	<input type="hidden" id="validation-error" value="true">
<?php else: ?>
	<input type="hidden" id="validation-error" value="false">
<?php endif ?>
	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Título', 'title', array('class'=>'control-label')); ?>
			<?php echo Form::input('title', Input::post('title', isset($banner) ? $banner->title : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Título')); ?>
		</div>
		<div class="form-group">
			<?php echo Form::label('Descrição', 'description', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('description', Input::post('description', isset($banner) ? $banner->description : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Descrição')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Tipo', 'type', array('class'=>'control-label')); ?>
			<?php echo Form::select('type', Input::post('type', isset($banner) ? $banner->type : ''), array('' => 'Escolha um tipo', 'parceria' => 'Parceria', 'adsense' => 'Google Adsense'), array('class' => 'col-md-4 form-control', 'placeholder'=>'Escolha um tipo')); ?>
		</div>
		<div class="form-group hidden" id="cont-url-banner">
			<?php echo Form::label('Url', 'url', array('class'=>'control-label')); ?>
			<?php echo Form::input('url', Input::post('url', isset($banner) ? $banner->url : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Url', 'disabled')); ?>
		</div>
		<?php 
			$hidden = (isset($banner) and $banner->type === 'adsense') ? '' : 'hidden';
			$disabled = (isset($banner) and $banner->type === 'adsense') ? '' : 'disabled';
		?>
		<div class="form-group <?php echo $hidden; ?>" id="cont-media-url">
			<?php echo Form::label('Script do Adsense (Código embed)', 'media_url', array('class'=>'control-label')); ?>
			<?php echo Form::textarea('media_url', Input::post('media_url', isset($banner) ? $banner->media_url : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Cole aqui o script do banner do Adsense', $disabled)); ?>
		</div>
		<div class="form-group hidden" id="cont-file-banner">
			<?php echo Form::label('Upload do arquivo do banner', 'upload_banner', array('class' => 'control-label', 'disabled')) ?>
			<?php echo Form::file('upload_banner', array('disabled')); ?>
		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<div class="btn-group">
				<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-success')); ?>
				<?php echo Html::anchor('admin/banners', 'Voltar', array('class' => 'btn btn-danger')); ?>
			</div>
		</div>
	</fieldset>
<?php echo Form::close(); ?>