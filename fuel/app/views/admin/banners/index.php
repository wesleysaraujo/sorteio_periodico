<div class="pull-right">
	<?php echo Html::anchor('admin/banners/create', '<i class="glyphicon glyphicon-plus"></i> Criar novo banner de publicidade', array('class' => 'btn btn-success')); ?>
</div >
<h2>Banners de publicidade</h2>
<br>
<?php if ($banners): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Título</th>
			<th>Cadastrado por:</th>
			<th>Tipo</th>
			<th>Url</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($banners as $item): ?>		
		<tr>
			<td><?php echo $item->title; ?></td>
			<td><?php echo $item->user->username; ?></td>
			<td><?php echo $item->type; ?></td>
			<td><?php echo $item->url; ?></td>
			<td>
			<div class="btn-group">
				<?php echo Html::anchor('admin/banners/view/'.$item->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-info btn-sm')); ?>
				<?php echo Html::anchor('admin/banners/edit/'.$item->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning btn-sm')); ?>
				<?php echo Html::anchor('admin/banners/delete/'.$item->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('class' => 'btn btn-danger btn-sm','onclick' => "return confirm('Tem certeza que deseja excluir?')")); ?>
			</div>
			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>Nenhum Banner de plucidade cadastrado.</p>

<?php endif; ?>