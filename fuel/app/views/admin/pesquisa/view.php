<h3>Dados da pesquisa #<?php echo $pesquisa->id; ?></h3>
<div class="row">
	<div class="col-md-8">
		<div class="panel panel-info">
			<div class="panel-heading">
				<?php echo $pesquisa->title; ?>
			</div>
			<div class="panel-body">
				<p><strong>Descrição: </strong><?php echo $pesquisa->description; ?></p>
				<hr>
				<h3>Perguntas (<?php echo count($pesquisa->perguntas) ?>) <small class="pull-right"><a href="<?php echo Uri::base()."admin/perguntas/create?pesquisa=".Crypt::encode($pesquisa->id)."&slug=".$pesquisa->title; ?>" id="btn-add-perguntas" class="btn btn-default bt-sm"><i class="glyphicon glyphicon-plus"></i> Adicionar pergunta</a></small></h3>
				<input type="hidden" name="hash_search" id="hash_search" value="<?php echo Crypt::encode(Uri::segment(4)) ?>">
				<?php if (count($pesquisa->perguntas) > 0): ?>
					<?php echo Html::anchor('admin/perguntas/?pesquisa='.Crypt::encode($pesquisa->id), 'Ver as perguntas dessa pesquisa', array('class' => 'btn btn-info btn-sm')) ?>					
				<?php endif ?>
				<hr>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<ul class="list-group">
			<li class="list-group-item"> 
				<strong>Visualizações: </strong> <span class="text-success"><?php echo $pesquisa->views; ?></span>
			</li>
			<li class="list-group-item">
				<strong>Criado por: </strong> <span class="text-success"><?php echo $profile_fields['full_name']; ?></span>
			</li>
			<li class="list-group-item">			
				<strong>Criado em: </strong> <span class="text-success "><?php echo date('d-m-Y H:i:s', $pesquisa->created_at); echo " (".Date::time_ago($pesquisa->updated_at).")"; ?></span>
			</li>
			<li class="list-group-item">
				<strong>Última alteração: </strong><span class="text-success"><?php echo date('d-m-Y H:i:s', $pesquisa->updated_at); echo " (".Date::time_ago($pesquisa->updated_at).")"; ?></span>
			</li>
			<li class="list-group-item">
				<strong>Status da pesquisa: </strong><span class="<?php switch($pesquisa->status){ case('inativo'): echo 'text-danger'; break; case('ativo'): echo 'text-success'; break; case('terminado'): echo 'text-warning'; break;} ?>"><?php echo $pesquisa->status; ?></span>
			</li>
		</ul>
		<hr>
	</div>	
</div>	
<div class="btn-group">
	<?php echo Html::anchor('admin/pesquisa/edit/'.$pesquisa->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning btn-sm')); ?>
	<?php echo Html::anchor('admin/pesquisa', 'Voltar', array('class' => 'btn btn-danger btn-sm')); ?>
</div>

<div class="modal fade" id="modal-perguntas">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="glyphicon glyphicon-plus"></i> Adicionar pergunta</h4>
      </div>
      <div class="modal-body" id="modal-container-content-pergunta">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">cancelar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->