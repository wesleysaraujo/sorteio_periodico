<div class="row">
	<div class="col-md-6">
		<?php echo Form::open(array("class"=>"form-horizontal")); ?>

			<fieldset>
				<div class="form-group">
					<?php echo Form::label('Título da Pesquisa', 'title', array('class'=>'control-label')); ?>

						<?php echo Form::input('title', Input::post('title', isset($pesquisa) ? $pesquisa->title : ''), array('class' => 'col-md-4 form-control', 'required', 'placeholder'=>'Título')); ?>

				</div>
				<div class="form-group">
					<?php echo Form::label('Descrição', 'description', array('class'=>'control-label')); ?>
						<?php echo Form::textarea('description', Input::post('description', isset($pesquisa) ? $pesquisa->description : ''), array('class' => 'col-md-8 form-control', 'required', 'rows' => 8, 'placeholder'=>'Descrição')); ?>

				</div>

				<?php if (Auth::member(100)): ?>
				<div class="form-group">
					<div class="input-group">
						<?php echo Form::label('Status da Pesquisa', 'status', array('class' => 'control-label')) ?>
						<?php echo Form::select('status', Input::post('status', isset($pesquisa) ? $pesquisa->status : 'Escolha um status'), array('ativo' => 'Ativo', 'inativo' => 'Inativo', 'terminado' => 'Terminado'), array('class' =>'form-control', 'required',)) ?>
					</div>
				</div>
				<?php endif ?>
				<div class="form-group">
					<label class='control-label'>&nbsp;</label>
					<div class="btn-group">
						<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-primary')); ?>
						<?php echo Html::anchor('admin/pesquisa', 'Voltar', array('class' => 'btn btn-danger')); ?>
					</div>
				</div>
			</fieldset>
		<?php echo Form::close(); ?>
	</div>
</div>