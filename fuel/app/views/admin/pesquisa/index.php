<div class="pull-right">
	<?php echo Html::anchor('admin/pesquisa/create', '<i class="glyphicon glyphicon-plus"></i> Criar nova Pesquisa', array('class' => 'btn btn-success')); ?>
</div>
<h2>Pesquisas cadastradas</h2>
<br>
<?php if ($pesquisas): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Título</th>
			<th>Status</th>
			<th>Qtd Perguntas</th>
			<th>Criada por</th>
			<th>Gerenciar</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($pesquisas as $item): ?>
	<tr>
	<?php 
		switch ($item->status) {
			case 'inativo':
				$label_color = 'label-danger';
				break;
			case 'terminado':
				$label_color = 'label-warning';
				break;
			case 'ativo':
				$label_color = 'label-success';
				break;
		}
	 ?>
			<td><?php echo $item->title; ?></td>
			<td><span class="label <?php echo $label_color; ?>"><?php echo $item->status ?></span></td>
			<td><?php echo count($item->perguntas); ?></td>
			<td><?php echo $item->user->username; ?></td>
			<td>
				<div class="btn-group">
					<?php echo Html::anchor('admin/pesquisa/view/'.$item->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-info btn-sm')); ?>
					<?php echo Html::anchor('admin/pesquisa/edit/'.$item->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning btn-sm')); ?>
					<?php echo Html::anchor('admin/pesquisa/delete/'.$item->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('class' => 'btn btn-danger btn-sm','onclick' => "return confirm('Tem certeza que deseja excluir?')")); ?>
				</div>
			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>Nenhuma Pesquisa.</p>

<?php endif; ?>