<div class="pull-right">
		<?php #echo Html::anchor('admin/brindes/create', '<i class="glyphicon glyphicon-plus"></i> Adicionar novo Brinde', array('class' => 'btn btn-success')); ?>
</div>
<h2>Brindes</h2>
<br>
<?php if ($brindes): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Brinde</th>
			<th>Quantidade</th>
			<th>Descrição</th>
			<th>Parceiro</th>
			<th>Promoção</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($brindes as $item): ?>		<tr>

			<td><?php echo $item->name; ?></td>
			<td><?php echo $item->amount; ?></td>
			<td><?php echo $item->description; ?></td>
			<td><?php echo $item->parceiro->name; ?></td>
			<td><?php echo $item->promotion->description; ?></td>
			<td>
				<div class="btn-group">
					<?php echo Html::anchor('admin/brindes/view/'.$item->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-sm btn-info')); ?>
					<?php echo Html::anchor('admin/brindes/edit/'.$item->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-sm btn-warning')); ?>
					<?php echo Html::anchor('admin/brindes/delete/'.$item->id, '<i class="glyphicon glyphicon-trash"></i> Excluir', array('class' => 'btn btn-sm btn-danger', 'onclick' => "return confirm('Tem certeza que deseja excluir?')")); ?>
				</div>
			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>Nenhum Brinde Cadastrado.</p>

<?php endif; ?>