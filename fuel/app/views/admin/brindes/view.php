<div class="row">
	<div class="col-md-7">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $brinde->name; ?> #<?php echo $brinde->id; ?></h3>
			</div>
			<div class="panel-body">
				<p><strong>Quantidade:</strong> <?php echo $brinde->amount; ?></p>	
				<p><strong>Descrição:</strong><br>
					<?php echo $brinde->description; ?>
				</p>
				<p><strong>Parceiro:</strong>
					<?php echo $brinde->parceiro->name; ?>
				</p>
			</div>
		</div>		
	</div>
	<div class="col-md-5">
		<ul class="list-group">
			<li class="list-group-item"><strong>Promoção: </strong><?php echo Html::anchor('admin/promotion/view/'.$brinde->promotion_id, $brinde->promotion->description) ?> </li>
			<li class="list-group-item"><strong>Cadastrado em: </strong><?php echo date('d/m/Y H:i:s', $brinde->created_at) ?> </li>
			<li class="list-group-item"><strong>Última atualização: </strong><?php echo date('d/m/Y H:i:s', $brinde->updated_at) ?> </li>
		</ul>
	</div>
</div>

<div class="btn-group">
	<?php echo Html::anchor('admin/brindes/edit/'.$brinde->id, '<i class="glyphicon glyphicon-pencil"></i> Editar', array('class' => 'btn btn-warning')); ?>
	<?php echo Html::anchor('admin/promotion/view/'.$brinde->promotion_id, 'Voltar', array('class' => 'btn btn-danger')); ?>
</div>