<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Nome', 'name', array('class'=>'control-label')); ?>

				<?php echo Form::input('name', Input::post('name', isset($brinde) ? $brinde->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Nome do brinde')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Quantidade', 'amount', array('class'=>'control-label')); ?>

				<?php echo Form::input('amount', Input::post('amount', isset($brinde) ? $brinde->amount : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Quantos brindes disponíveis')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Descrição', 'description', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('description', Input::post('description', isset($brinde) ? $brinde->description : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Descrição do brinde')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Parceiro', 'parceiro_id', array('class'=>'control-label')); ?>
				<?php echo Form::select('parceiro_id', Input::post('parceiro_id', isset($brinde) ? $brinde->parceiro_id : ''), $parceiros, array('class' => 'col-md-4 form-control')); ?>
		</div>
		<div class="form-group">
				<?php echo Form::hidden('promotion_id', Input::post('promotion_id', isset($brinde) ? $brinde->promotion_id : $promotion), array('class' => 'col-md-4 form-control')); ?>
		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<div class="btn-group">
				<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-primary')); ?>
				<?php echo Html::anchor('admin/brindes', 'Cancelar', array('class' => 'btn btn-danger')); ?>
				<?php if (Uri::segment(3) == 'edit'): ?>
					<?php echo Html::anchor('admin/brindes/view/'.$brinde->id, '<i class="glyphicon glyphicon-eye-open"></i> Visualizar', array('class' => 'btn btn-info')); ?>
				<?php endif ?>
			</div>			
		</div>
	</fieldset>
<?php echo Form::close(); ?>