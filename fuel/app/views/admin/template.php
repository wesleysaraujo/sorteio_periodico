<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<?php echo Asset::css(array('bootstrap.min.css', 'main.css', 'datepicker.css','font-awesome.min.css', 'bootflat.css', 'bootflat-extensions.css', 'bootflat-square.css')); ?>
	<style>
		body { margin: 50px; }
	</style>
	<!--[if lt IE 9]>
    	<?php echo asset::js(array('html5shiv.js', 'respond.min.js')) ?>
    <![endif]-->
</head>
<body>

	<?php if ($current_user): ?>
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
		    <?php echo Html::anchor(Uri::base()."admin", '<small><i class="glyphicon glyphicon-fire"></i> </small>Pesquisa <small>(Painel admin)</small>', array('class'=>'navbar-brand')) ?>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="<?php echo Uri::segment(2) == '' ? 'active' : '' ?>">
						<?php echo Html::anchor('admin', 'Dashboard') ?>
					</li>
					<li class="dropdown">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#">Gerenciar pesquisas<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li>
								<?php echo Html::anchor('admin/promotion', 'Promoções') ?>
							</li>
							<li>
								<?php echo Html::anchor('admin/pesquisa', 'Pesquisas') ?>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#">Publicidade e Parceria<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li>
								<?php echo Html::anchor('admin/banners', 'Banners') ?>
							</li>
							<li>
								<?php echo Html::anchor('admin/parceiros', 'Parceiros') ?>
							</li>
							<li>
								<?php echo Html::anchor('admin/brindes', 'Brindes') ?>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#">Administração<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li>
								<?php echo Html::anchor('admin/users', 'Usuários') ?>
							</li>
							<li>
								<?php echo Html::anchor('#admin/configuration', 'Configurações') ?>
							</li>
						</ul>
					</li>

				</ul>
				<ul class="nav navbar-nav pull-right">
					<li class="dropdown">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="glyphicon glyphicon-user"></i> <?php echo $current_user->username ?> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><?php echo Html::anchor('/', 'Acessar o site') ?></li>
							<li><?php echo Html::anchor('admin/logout', 'Sair') ?></li>
							<li class="divider"></li>
							<li><?php echo Html::anchor('/alterar-senha/'.$current_user->id, 'Alterar Senha') ?></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php echo $title; ?></h1>
				<hr>
<?php if (Session::get_flash('success')): ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<p>
					<?php echo implode('</p><p>', (array) Session::get_flash('success')); ?>
					</p>
				</div>
<?php endif; ?>
<?php if (Session::get_flash('error')): ?>
				<div class="alert alert-error alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<p>
					<?php echo implode('</p><p>', (array) Session::get_flash('error')); ?>
					</p>
				</div>
<?php endif; ?>
			</div>
			<div class="col-md-12">
<?php echo $content; ?>
			</div>
		</div>
		<hr/>
		<footer>
			<p class="pull-right">Sistema desenvolvido por Wesley S. Araújo.</p>
			<p>
				<a href="http://www.odebateon.com.br/site/">Odebateon.com.br</a> Todos os direitos reservados<br>
			</p>
		</footer>
	</div>
	<?php echo Asset::js('jquery-1.10.1.min.js'); ?>
	<!-- <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->
	<?php echo Asset::js('bootstrap.min.js'); ?>
	<?php echo Asset::js('plugins.js'); ?>
	<?php echo Asset::js('main.js'); ?>

	<script>
		$(function(){ $('.topbar').dropdown(); });
	</script>
</body>
</html>
