<?php if ($status != 'aguardando'): ?>
<span class="pull-right"><a href="javascript:void()" id="edit-field" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-pencil">&nbsp;</i>Editar perfil</a></span>	
<?php endif ?>
<h2>Olá <span class="text-info"><?php echo Auth::get_profile_fields('full_name'); ?></span></h2>
<div class="row">
	<div class="container" id="form-user-profile">
		<?php if(isset($messages) AND count($messages)>0): ?>
		<div class="alert alert-error">
			<a href="javascript:void()" class="close" data-dismiss="alert">x</a>
			<h3>Erros no formulário</h3>
			<ul>
				<?php foreach($messages as $message): ?>
				<li><?php echo $message; ?></li>
			<?php endforeach; ?>
			</ul>
		</div>
		<?php endif; ?>
		<?php echo Form::open() ?>
		<h3>Informações Pessoais</h3>
		<div class="row">
			<?php $readonly = ($status == 'ativado') ? "readonly": "";  ?>
			<div class="col-md-4 form-group">
				<?php echo Form::label('Nome Completo: ', 'full_name', array('class' => 'control-label')) ?>
				<?php echo Form::input('full_name', Auth::get_profile_fields('full_name'), array('class' => 'form-control', $readonly)) ?>
			</div>
			
			<div class="col-md-3 form-group">
				<?php echo Form::label('Data de nascimento: ', 'date_birth', array('class' => 'control-label')) ?>
				<?php echo Form::input('date_birth', Auth::get_profile_fields('date_birth'), array('class' => 'form-control date-birth', $readonly)) ?>
			</div>

			<div class="col-md-3 form-group">
				<?php echo Form::label('Nº de Identidade: ', 'number_rg', array('class' => 'control-label')) ?>
				<?php echo Form::input('number_rg', Auth::get_profile_fields('number_rg'), array('class' => 'form-control number_rg', $readonly)) ?>
			</div>
			
			<div class="col-md-2 form-group">
				<?php echo Form::label('CPF: ', 'cpf', array('class' => 'control-label')) ?>
				<?php echo Form::input('cpf', Auth::get_profile_fields('cpf'), array('class' => 'form-control cpf', $readonly)) ?>
			</div>
		</div>
		<h3>Informações de Endereço</h3>
		<div class="row">
			<div class="col-md-5 form-group">
				<?php echo Form::label('Endreço: ', 'address', array('class' => 'control-label')) ?>
				<?php echo Form::input('address', Auth::get_profile_fields('address'), array('class' => 'form-control', $readonly)) ?>
			</div>

			<div class="col-md-3 form-group">
				<?php echo Form::label('Bairro: ','district', array('class' => 'control-label')); ?>
				<?php echo Form::input('district', Auth::get_profile_fields('district'), array('class' => 'form-control', $readonly)) ?>
			</div>

			<div class="col-md-2 form-group">
				<?php echo Form::label('Número: ','number_home', array('class' => 'control-label')); ?>
				<?php echo Form::input('number_home', Auth::get_profile_fields('number_home'), array('class' => 'form-control', $readonly)) ?>
			</div>

			<div class="col-md-2 form-group">
				<?php echo Form::label('Complemento: ', 'complement', array('class' => 'control-label')); ?>
				<?php echo Form::input('complement', Auth::get_profile_fields('complement'), array('class' => 'form-control', $readonly)) ?>
			</div>

			<div class="col-md-3 form-group">
				<?php echo Form::label('Cidade: ', 'city', array('class' => 'control-label')); ?>
				<?php echo Form::input('city', Auth::get_profile_fields('city'), array('class' => 'form-control', $readonly)) ?>
			</div>
			<div class="col-md-3 form-group">
				<?php echo Form::label('CEP: ', 'cep', array('class' => 'control-label')); ?>
				<?php echo Form::input('cep', Auth::get_profile_fields('cep'), array('class' => 'form-control cep', $readonly)) ?>
			</div>

			<div class="col-md-3 form-group">
				<?php echo Form::label('Estado: ', 'uf', array('class' => 'control-label')); ?>
				<?php echo Form::select('uf', Auth::get_profile_fields('uf'), array(
						""	=>"Escolha o estado",
						"AC"=>"Acre",
						"AL"=>"Alagoas",
						"AM"=>"Amazonas",
						"AP"=>"Amapá",
						"BA"=>"Bahia",
						"CE"=>"Ceará",
						"DF"=>"Distrito Federal",
						"ES"=>"Espírito Santo",
						"GO"=>"Goiás",
						"MA"=>"Maranhão",
						"MT"=>"Mato Grosso",
						"MS"=>"Mato Grosso do Sul",
						"MG"=>"Minas Gerais",
						"PA"=>"Pará",
						"PB"=>"Paraíba",
						"PR"=>"Paraná",
						"PE"=>"Pernambuco",
						"PI"=>"Piauí",
						"RJ"=>"Rio de Janeiro",
						"RN"=>"Rio Grande do Norte",
						"RO"=>"Rondônia",
						"RS"=>"Rio Grande do Sul",
						"RR"=>"Roraima",
						"SC"=>"Santa Catarina",
						"SE"=>"Sergipe",
						"SP"=>"São Paulo",
						"TO"=>"Tocantins"
					),
					array('class' => 'form-control', $readonly)) ?>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="btn-group <?php if($status == 'ativado' or $status == 'aguardando') echo "hidden"; ?>">
				<?php echo Form::submit('submit', 'Atualizar Perfil', array('class' => 'btn btn-lg btn-success')) ?>
				<?php echo Html::anchor('pesquisa', 'Cancelar', array('class' => 'btn btn-lg btn-danger')) ?>
			</div>
		</div>
		<?php echo Form::close() ?>
	</div>
</div>