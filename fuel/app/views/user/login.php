<div class="row">
	<div class="col-md-6">
		<?php if (isset($login_error)): ?>
			<div class="alert alert-danger">
				<a href="javascript:void();" class="close">x</a>
				<?php echo $login_error ?>
			</div>
		<?php endif ?>
		<h3>Entre ou se cadastre</h3>
		<?php echo Form::open() ?>
			<div class="form-group <?php if($val->error('email')) echo "has-error"; ?>">
				<?php echo Form::label('Nome de usuário ou e-mail', 'email') ?>
				<?php echo Form::input('email', Input::post('email'), array('class' => 'form-control', 'placeholder' => 'nome de usuário ou e-mail')) ?>
				<?php if ($val->error('email')): ?>
					<span class="control-label"><?php echo $val->error('email')->get_message('Você precisa informar o seu username ou email'); ?></sőan>
				<?php endif; ?>
			</div>
			<div class="form-group <?php if($val->error('password')) echo "has-error"; ?>">
				<?php echo Form::label('Senha', 'password') ?>
				<?php echo Form::password('password', '',array('class' => 'form-control', 'placeholder' => 'Informe sua senha')) ?>
				<?php if ($val->error('password')): ?>
					<span class="control-label"><?php echo $val->error('password')->get_message('Informe sua senha'); ?></sőan>
				<?php endif; ?>
			</div>
			<div class="pull-right">
				<?php echo Form::submit('submit', 'Entrar', array('class' => 'btn btn-primary btn-square')) ?>
			</div>
			<br><br>
			<div class="pull-right">
				<?php echo Html::anchor('cadastro-de-usuario', 'Quero me cadastrar', array('class' => 'btn btn-info btn-square')) ?>
			</div>
		<?php echo Form::close() ?>
	</div>
</div>