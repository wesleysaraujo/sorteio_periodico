<h3><i class="glyphicon glyphicon-user"></i> Sou novo aqui! <small>Quero me cadastrar</small></h3>

<div class="row " id="form-cad-user">
	<div class="container">
		<div class="col-md-6">
			<?php echo Form::open(array('class' => 'form-horizontal')) ?>
				<div class="form-group <?php echo ! $val->error('full_name') ?: 'has-error' ?>">
					<?php echo Form::label('Nome Completo', 'full_name') ?>
					<?php echo Form::input('full_name', Input::post('full_name'), array('id' => 'full_name', 'class' => 'form-control', 'placeholder' => 'Informe seu nome completo')) ?>
					<?php if($val->error('full_name')): ?>
						<span class="text-danger"><?php echo $val->error('full_name'); ?></span>
					<?php endif; ?>
				</div>

				<div class="form-group <?php echo ! $val->error('username') ?: 'has-error' ?>">
					<?php echo Form::label('Username', 'username') ?>
					<div class="input-group">
						<span class="input-group-addon">@</span>
						<?php echo Form::input('username', Input::post('username'), array('id' => 'username', 'class' => 'form-control', 'placeholder' => 'Informe um nome de usuário para login, exempl: zecarioca')) ?>					
					</div>
					<?php if($val->error('username')): ?>
						<span class="text-danger"><?php echo $val->error('username'); ?></span>
					<?php endif; ?>
				</div>
				
				<div class="form-group <?php echo ! $val->error('email') ?: 'has-error' ?>">
					<?php echo Form::label('Seu email', 'email') ?>
					<?php echo Form::input('email', Input::post('email'), array('id' => 'email', 'class' => 'form-control', 'placeholder' => 'Informe seu e-mail')) ?>
					<?php if($val->error('email')): ?>
						<span class="text-danger"><?php echo $val->error('email'); ?></span>
					<?php endif; ?>
				</div>

				<div class="form-group <?php echo ! $val->error('password') ?: 'has-error' ?>">
					<?php echo Form::label('Senha', 'password') ?>
					<?php echo Form::password('password', '', array('id' => 'password', 'class' => 'form-control', 'placeholder' => 'Crie uma senha')) ?>
					<?php if($val->error('password')): ?>
						<span class="text-danger"><?php echo $val->error('password'); ?></span>
					<?php endif; ?>
				</div>
				
				<div class="form-group <?php echo ! $val->error('password-confirm') ?: 'has-error' ?>">
					<?php echo Form::label('Confirme a Senha', 'password-confirm') ?>
					<?php echo Form::password('password-confirm', '', array('id' => 'password-confirm', 'class' => 'form-control', 'placeholder' => 'Confirme a senha criada')) ?>
					<?php if($val->error('password-confirm')): ?>
						<span class="text-danger"><?php echo $val->error('password-confirm'); ?></span>
					<?php endif; ?>
				</div>

				<div class="form-group">
					<div class="btn-group">
						<?php echo Form::submit('submit','Criar Perfil', array('class' => 'btn btn-success btn-square', 'id' => "btn-create-profile")) ?>
						<?php echo HTML::anchor('login-de-acesso', 'Já tenho login', array('class' => 'btn btn-warning btn-square')) ?>						
					</div>
				</div>

			<?php echo Form::close() ?>
		</div>

		<div class="col-md-6">
			<?php echo Asset::img('logo_n.png') ?>
			<div class="panel panel-info" id="aviso-cad-user" style="margin-top: 20px;">
				<div class="panel-heading">
					<span>Seja bem vindo ao cadastro de usuário, esse processo será divido em 2 etapas:</span>				
				</div>
				<div class="panel-body">
					<ul>
						<li>O primeiro você irá passar por um cadastro simples, informando apenas alguns dados cruciais</li>
						<small>*Em seguida você receberá um e-mail de confirmação de cadastro no seu e-mail e deverá ativar seu cadastro</small>
						<li>Assim que seu cadastro for confirmado você será redirecionado para tela do perfil para terminar de preenche-lo</li>
						<small><em>*Vale lembrar que para participar da pesquisa e concorrer aos brindes você precisa preencher todo seu perfil</em></small>
					</ul>
				</div>				
			</div>
		</div>
	</div>
</div>