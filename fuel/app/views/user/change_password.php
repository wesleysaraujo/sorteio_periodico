<h2>Alterar minha senha</h2>
<div class="row">
	<?php if (isset($messages) AND count($messages) > 0): ?>
		<div class="alert alert-danger">
			<ul>
				<?php foreach ($messages as $message): ?>
					<li><?php echo $message ?></li>
				<?php endforeach ?>
			</ul>
		</div>
	<?php endif ?>
	<div class="col-md-6">
		<?php echo Form::open(array('class' => 'horizontal')); ?>
			<div class="form-group <?php echo ! $val->error('old_password') ? : 'has-error' ?>">
				<?php echo Form::label('Senha atual','old_password', array('class' => 'control-label')) ?>
				<?php echo Form::password('old_password', '', array('class' => 'form-control', 'required')) ?>
			</div>
			<div class="form-group <?php echo ! $val->error('password') ? : 'has-error' ?>">
				<?php echo Form::label('Nova Senha', 'password', array('class' => 'control-label')) ?>
				<?php echo Form::password('password', '', array('class' => 'form-control', 'required')); ?>
			</div>

			<div class="form-group <?php echo ! $val->error('repeat_password') ? : 'has-error' ?>">
				<?php echo Form::label('Repita a Nova Senha', 'repeat_password', array('class' => 'control-label')) ?>
				<?php echo Form::password('repeat_password', '', array('class' => 'form-control', 'required')); ?>
			</div>

			<div class="btn-group">
				<?php echo Form::submit('submit', 'Alterar senha', array('class' => 'btn btn-success')) ?>
				<?php echo Html::anchor('user/', 'Cancelar', array('class' => 'btn btn-danger')) ?>
			</div>

		<?php echo Form::close(); ?>
	</div>
</div>
