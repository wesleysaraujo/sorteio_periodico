<?php if (count($participacoes) == 0): ?>
	<div class="alert">Ainda não participei de nenhuma promoção</div>
<?php else: ?>
	<h3>Já participei de <?php echo count($participacoes) ?> <?php echo $pronuncia = (count($participacoes) > 1) ? 'promoções' : 'promoção' ?></h3>
	<table class="table table-striped table-bordered">
		<thead>
			<th>Promoção</th>
			<th>Pesquisa</th>
			<th width="200">Quantidade de Perguntas</th>
			<th width="260">Data da participação</th>
			<th>Gerenciar</th>
		</thead>
		<tbody>
			<?php foreach ($participacoes as $participacao): ?>
				<tr>
					<td><?php echo $participacao->promotion->title ?></td>
					<td><?php echo $participacao->promotion->pesquisa->title ?></td>
					<td><?php echo count($participacao->promotion->pesquisa->perguntas) ?></td>
					<td><?php echo date('d/m/Y H:i:s', $participacao->created_at)." (".Date::time_ago($participacao->created_at).")" ?></td>
					<td><?php echo Html::anchor('detalhe-da-participacao/'.$participacao->id, '<i class="glyphicon glyphicon-list-alt"></i> Detalhes', array('class' => 'btn btn-info btn-block btn-sm')) ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
<?php endif ?>