<h4>Participando da promoção: <span class="text-info"><?php echo $promotion->title ?></span></h4>
<hr>
<h3>Pesquisa: <?php echo $promotion->pesquisa->title ?></h3>
<div class="well">
	<p><?php echo $promotion->pesquisa->description ?></p>
</div>
<hr>
<h4>Perguntas <small>(<?php echo count($promotion->pesquisa->perguntas) ?>)</small></h4>
<!-- Slideshow HTML -->
<div id="row">
	<div id="col-md-9">
    	<?php foreach ($promotion->pesquisa->perguntas as $pergunta): ?>
	    		<div class="slide hidden">
	    			<h3><?php echo $pergunta->description ?></h3>
	    			<?php if ($pergunta->type == 'fechada'): ?>
	    				<div class="form-group">
	    					<div class="row">
	    						<div class="col-md-9">
			    					<?php foreach ($pergunta->opcao as $opcao): ?>
			    						<p><?php echo Form::radio('opcao', $opcao->id)." ".$opcao->title ?></p>
			    					<?php endforeach ?>
			    					<br>
			    					<div class="row">
					    				<div class="col-md-9">
						    				<?php if ($pergunta->banner->type == 'parceria'): ?>
							    				<?php echo Html::anchor($pergunta->banner->url, '<img src="'.$pergunta->banner->media_url.'" alt="'.$pergunta->banner->description.'" width="728" height="90" />', array('target' => '_blank')) ?>
							    			<?php else: ?>
							    				<?php echo $pergunta->banner->media_url ?>
							    			<?php endif ?>
						    			</div>
					    			</div>
					    			<br>
			    					<?php 
							    		echo Form::hidden('pergunta_id', $pergunta->id);
							    		echo Form::hidden('type', $pergunta->type);
							    	?>
									<button class="btn btn-square  btn-warning btn-resposta-pergunta-fechada" id="<?php echo $pergunta->id ?>">Responder</button>	    						
			    					
	    						</div>	
	    					</div>
	    				</div>
	    			<?php else: ?>
	    				<div class="form-group">
	    					<div class="row">
	    						<div class="col-md-9">
									<div class="form-group">
										<?php echo Form::textarea('resposta','',array('class' => 'form-control', 'id' => 'resposta-usuario', 'placeholder' => 'Digite aqui sua resposta', 'rows' => 10)) ?>	    						
		    							<span class="text-danger erro-form"></span>
		    							<br>
		    							<div class="row">
						    				<div class="col-md-9">
							    				<?php if ($pergunta->banner->type == 'parceria'): ?>
								    				<?php echo Html::anchor($pergunta->banner->url, '<img src="'.$pergunta->banner->media_url.'" alt="'.$pergunta->banner->description.'" width="728" height="90" />', array('target' => '_blank')) ?>
								    			<?php else: ?>
								    				<?php echo $pergunta->banner->media_url ?>
								    			<?php endif ?>
							    			</div>
						    			</div>
						    			<br>
						    			<?php 
								    		echo Form::hidden('pergunta_id', $pergunta->id);
								    		echo Form::hidden('type', $pergunta->type);
								    	?>
		    							<button class="btn btn-square btn-warning btn-resposta-pergunta-aberta" id="<?php echo $pergunta->id ?>">Responder</button>
		    						</div>
		    					</div>
	    					</div>
	    				</div>
	    			<?php endif ?>
	    			<div class="row">
						<div class="col-md-4 col-md-offset-2">
							<div class="progress progress-striped active hidden" id="progress">
							  <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							    <span class="sr-only"></span>
							  </div>
							</div>
						</div>
					</div>
				</div>
    	<?php endforeach ?>
    	<?php 
    		echo Form::hidden('pesquisa_id', $promotion->pesquisa->id);
    		echo Form::hidden('promotion_id', $promotion->id);
    		echo Form::hidden('base_url', Uri::base());
    	?>
    	<div class="well hidden" id='finalizar-participacao'>
    		<h3>Obrigado por participar da nossa pesquisa de promoção</h3>
    		<p>Para completar a pesquisa e concorrer aos prêmios, clique no botão abaixo</p>
    		<span id="aviso-status"></span>
    		<?php echo Html::anchor('#', 'Concluir participação na pesquisa', array('class' => 'btn btn-square btn-lg btn-success', 'id' => 'btn-concluir-participacao')) ?>
    	</div>
	</div>
	
</div>