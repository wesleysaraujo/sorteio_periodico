<div class="well">
	<div class="pull-right">
		<?php echo Html::anchor('promocoes-ativas', 'Voltar', array('class' => 'btn btn-danger')) ?>
	</div>
	<h3>Detalhes da Promoção</h3>
	<p class="text-info"><?php echo $promotion->description ?></p>
	<p class="text-primary">Pesquisa: <?php echo $promotion->pesquisa->title ?></p>
	<p class="text-primary">Quantidade de perguntas: <?php echo count($promotion->pesquisa->perguntas) ?></p>
	<hr>
	<h3>Período</h3>
	<p class="text-success">Início em: <?php echo date('d/m/Y \a\s H:i:s', $promotion->date_start) ?></p>
	<p class="text-danger">Término em: <?php echo date('d/m/Y \a\s H:i:s', $promotion->date_end) ?></p>
	<hr>
	<h3>Ganhadores</h3>
	<ul class="list-group">
	<?php 
		foreach ($winners as $winner): 
		$data_profile_fields = trim($winner->user->profile_fields);
		$profile_fields = unserialize(html_entity_decode($data_profile_fields, ENT_QUOTES));
	?>
		<li class="list-group-item">
			<?php echo $profile_fields['full_name']." (".$winner->user->username.")" ?> - <?php echo $winner->brinde->amount." ".$winner->brinde->name ?>
			<?php echo $ganhou = ($winner->user_id === $current_user->id) ? "<span class='label label-success'>Eu Ganhei</span>" : ""; ?> - 
			<small>(Oferecimento: <a href="<?php echo $winner->brinde->parceiro->site_url ?>" target="_blank" class="btn btn-link btn-sm btn-parceiro-click" id="<?php echo $winner->brinde->parceiro->id ?>" rel="<?php echo $promotion->id ?>"><?php echo $winner->brinde->parceiro->name ?></a>)</small>
		</li>
	<?php endforeach ?>
	</ul>
</div>