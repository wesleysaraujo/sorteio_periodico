<?php if (isset($promotions) and count($promotions) > 0): ?>
<?php foreach ($promotions as $promotion): ?>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo $promotion->title ?></h3>
		</div>
		<div class="panel-body">
			<div class="btn-group pull-right">
			<?php if (date('Y-m-d H:i:s', $promotion->date_end) > date('Y-m-d H:i:s')): ?>
				<span class="btn btn-info btn-sm">Início: <?php echo date('d/m/Y H:i:s', $promotion->date_start) ?></span>
				<span class="btn btn-danger btn-sm"> Fim: <?php echo date('d/m/Y H:i:s', $promotion->date_end) ?></span>
				<span class="btn btn-success btn-sm"><?php echo $aberta_promocao_label = (date('Y-m-d H:i:s') >= date('Y-m-d H:i:s', $promotion->date_start)) ? 'Promoção Aberta' : 'Abrirá em '.date('d/m/Y \a\s H:i:s', $promotion->date_start);
 ?></span>
			<?php else: ?>
				<span class="btn btn-disabled btn-sm">Início: <?php echo date('d/m/Y H:i:s', $promotion->date_start) ?></span>
				<span class="btn btn-disabled btn-sm"> Fim: <?php echo date('d/m/Y H:i:s', $promotion->date_end) ?></span>
				<span class="btn btn-danger btn-sm">Promoção Encerrada</span>
			<?php endif;?>
			</div>
			<h3>Pesquisa: <?php echo $promotion->pesquisa->title; ?></h3>
			<p><?php echo $promotion->pesquisa->description; ?></p>
			<hr>
			<!-- Nav tabs -->
			<ul class="nav nav-tabs">
			  <li class="active"><a href="#perguntas" data-toggle="tab">Perguntas</a></li>
			  <li><a href="#brindes" data-toggle="tab">Brindes</a></li>
			  <li><a href="#parceiros" data-toggle="tab">Parceiros</a></li>
			  <li><a href="#info" data-toggle="tab">Informações Gerais</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
			  <div class="tab-pane fade in active" id="perguntas">
			  	<div class="visible-lg">
			  		<br>
			  		<h3>Essa pesquisa possui <?php echo count($promotion->pesquisa->perguntas)." ".$perguntas = (count($promotion->pesquisa->perguntas) > 1) ? 'perguntas' : 'pergunta' ?> </h3>
			  	</div>
			  </div>
			  <div class="tab-pane" id="brindes">
			  	<br>

			  		<?php foreach ($promotion->brindes as $brinde): ?>
						<a href="#" class="btn btn-primary btn-sm btn-brinde-promotion" data-container="body" data-toggle="popover" data-placement="top" title="" data-content="<?php echo $brinde->amount ?> - <?php echo $brinde->description ?> " role="button" data-original-title="Parceiro: <?php echo $brinde->parceiro->name ?>"><?php echo $brinde->name." (".$brinde->amount.")" ?></a>
					<?php endforeach ?>
			  </div>
			  <div class="tab-pane" id="parceiros">
			  	<br>
					<div class="row">

			  		<?php 
			  			$parceiros = $promotion->brindes;
			  			foreach ($parceiros as $parceiro_brinde): ?>
						  <div class="col-xs-6 col-md-2">						    
						  	<a href="<?php echo $parceiro_brinde->parceiro->site_url ?>" target="_blank" class="thumbnail">
						      <img data-src="holder.js/100%x180" src="<?php echo Uri::base().$parceiro_brinde->parceiro->image_url ?>" alt="...">
						    </a>
						    <a href="<?php echo $parceiro_brinde->parceiro->site_url ?>" target="_blank" class="btn btn-primary btn-sm btn-brinde-promotion btn-parceiro-click" id="<?php echo $parceiro_brinde->parceiro->id ?>" rel="<?php echo $promotion->id ?>"><?php echo $parceiro_brinde->parceiro->name ?></a>
						  </div>
					<?php endforeach ?>
					</div>
			  </div>
			  <div class="tab-pane" id="info">
			  	<br>
			  	<?php echo $promotion->description ?>
			  </div>
			</div>
			<hr>
			<?php if (date('Y-m-d H:i:s', $promotion->date_end) > date('Y-m-d H:i:s')): ?>
				<?php 
					$inicio_pesquisa_label = (date('Y-m-d H:i:s') >= date('Y-m-d H:i:s', $promotion->date_start)) ? 'Participar da pesquisa' : 'A pesquisa vai começar em '.date('d/m/Y \a\s H:i:s', $promotion->date_start);
					$inicio_pesquisa_link = (date('Y-m-d H:i:s') >= date('Y-m-d H:i:s', $promotion->date_start)) ? 'participar-promocao/'. $promotion->id : 'javascript:{}';
				?>
				<?php echo Html::anchor($inicio_pesquisa_link, $inicio_pesquisa_label, array('class' => 'btn btn-primary btn-lg')); ?>
			<?php else: ?>
				<?php echo Html::anchor('ver-resultado-promocao/'. $promotion->id, 'Resultado da promoção e pesquisa', array('class' => 'btn btn-disabled btn-lg')); ?>
			<?php endif ?>
		</div>
	</div>
<?php endforeach ?>
<?php else: ?>
<div class="alert">
	Nenhuma promoção ativada
</div>	
<?php endif ?>