<?php 
	if (count($winner) > 0):
		foreach ($winner as $ganhei):
?>
			<div class="alert alert-info">
				<h3>Parabéns você foi sorteado nessa promoção</h3>
				<p>Você ganhou <?php echo $ganhei->brinde->amount." ".$ganhei->brinde->name ?></p>
			</div>
		<?php endforeach; ?>	
<?php endif ?>
<div class="panel panel-info">
	<div class="panel-heading">
		<h3 class="panel-title">Participação #<?php echo $participacao->id ?></h3>
	</div>
	<div class="panel-body">
		<p>
			<strong>Promoção: </strong> <?php echo $participacao->promotion->title ?><br>
			<b><i><?php echo $participacao->promotion->description ?></i></b>
		</p>
		<p>
			<strong>Pesquisa: </strong><?php echo $participacao->promotion->pesquisa->title ?><br>
			<i>"<?php echo $participacao->promotion->pesquisa->description ?>"</i>
		</p>
		<hr>
		<h3>Perguntas: </h3>
		<?php foreach ($participacao->promotion->pesquisa->perguntas as $pergunta): ?>
			<p><b><?php echo $pergunta->description ?></b></p>
			<?php if ($pergunta->type == 'aberta'): ?>
				<?php 
					$respostas = DB::select('description')->from('respostas')->where('pergunta_id', $pergunta->id)->where('user_id', $current_user->id)->as_object()->execute();  
					foreach ($respostas as $resposta): ?>
					<i><?php echo $resposta->description ?></i>
				<?php endforeach ?>
			<?php else: ?>
				<?php 
					$opcoes_usuarios = DB::select()->from('users_opcoes')->where('pergunta_id', $pergunta->id)->where('user_id', $current_user->id)->as_object('Model_Users_Opcao')->execute();
					foreach($opcoes_usuarios as $opcao):
				?>
					<i><?php echo $opcao->opcao->title ?></i>
				<?php endforeach ?>
			<?php endif ?>
		<?php endforeach ?>
	</div>
</div>