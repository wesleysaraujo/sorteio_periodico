<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<?php echo Asset::css(array('bootstrap.min.css', 'main.css','font-awesome.min.css', 'bootflat.css', 'bootflat-extensions.css', 'bootflat-square.css')); ?>
	<style>
		body { margin: 40px; }
	</style>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    	<?php echo asset::js(array('html5shiv.js', 'respond.min.js')) ?>
    <![endif]-->
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-default" role="navigation">
		  <!-- Brand and toggle get grouped for better mobile display -->
		  <div class="navbar-header">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		      <span class="sr-only">Toggle navigation</span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		    </button>
		    <?php echo Html::anchor(Uri::base(), '<small><i class="glyphicon glyphicon-fire"></i> </small>Pesquisa <small>Participe e concorra</small>', array('class'=>'navbar-brand')) ?>
		  </div>

		  <!-- Collect the nav links, forms, and other content for toggling -->
		
		  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		    <?php if($current_user): ?>
		    <ul class="nav navbar-nav">
		    	<li class="dropdown">
		    		<a href="javascript:void();" class="dropdown-toggle" data-toggle="dropdown" >Promoções <b class="caret"></b></a>
		    		<ul class="dropdown-menu">
		    			<li class=""><?php echo Html::anchor('promocoes-ativas', 'Promoções Ativas') ?></li>
		      			<li class=""><?php echo Html::anchor('promocoes-que-participei', 'Minhas participações') ?></li>	
		    		</ul>
		    	</li>
		    </ul>
		    <ul class="nav navbar-nav pull-right">
		    	<li class="dropdown">
			        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> <?php echo $current_user->username ?> <b class="caret"></b></a>
			        <ul class="dropdown-menu">
			          <li><?php echo Html::anchor('alterar-senha/'.$current_user->id, '<i class="glyphicon glyphicon-barcode"></i> Alterar Senha') ?></li>
			          <li><?php echo Html::anchor('perfil/'.$current_user->id, '<i class="glyphicon glyphicon-user"></i> Ver perfil') ?></li>
			          <li><?php echo Html::anchor('perfil/'.$current_user->id, '<i class="glyphicon glyphicon-envelope"></i> Notificações') ?></li>
			          <li><?php echo Html::anchor('sair', '<i class="glyphicon glyphicon-remove-sign"></i> Sair') ?></li>
			          <?php if($current_user->group != 0): ?>
			          <li class="divider"></li>
			          <li><?php echo Html::anchor('admin','<i class="glyphicon glyphicon-lock"></i> Painel Admin') ?></li>
			          <?php endif; ?>
			        </ul>
		      	</li>
		     </ul> 
		  	<?php else: ?>
		  	<ul class="nav navbar-nav">
		    	<li <?php if(Uri::segment(1) == 'login-de-acesso') echo 'class="active"'; ?>><?php echo Html::anchor('login-de-acesso', 'Login') ?></li>  
		    	<li <?php if(Uri::segment(1) == 'cadastro-de-usuario') echo 'class="active"'; ?>><?php echo Html::anchor('cadastro-de-usuario', 'Cadastrar-se') ?></li>  
		    </ul>
		    <?php endif ?>
		  </div><!-- /.navbar-collapse -->
		
		</nav>
		<div class="col-md-12">
<?php if (Session::get_flash('success')): ?>
			<div class="alert alert-success">
				<button class="close" data-dismiss="alert">x</button>
				<strong>Ok!</strong>
				<p>
				<?php echo implode('</p><p>', e((array) Session::get_flash('success'))); ?>
				</p>
			</div>
<?php endif; ?>
<?php if (Session::get_flash('error')): ?>
			<div class="alert alert-danger">
				<button class="close" data-dismiss="alert">x</button>
				<strong>Ops!</strong>
				<p>
				<?php echo implode('</p><p>', e((array) Session::get_flash('error'))); ?>
				</p>
			</div>
<?php endif; ?>
		</div>
		<div class="col-md-12">
			<h1><?php echo $title ?></h1>
			<hr>
			<?php echo $content; ?>
		</div>
		<hr>
		<footer>
			<p class="pull-right">Sistema desenvolvido por Wesley S. Araújo.</p>
			<p>
				<a href="http://www.odebateon.com.br/site/">Odebateon.com.br</a> Todos os direitos reservados<br>
			</p>
		</footer>
	</div>
	<?php echo Asset::js('jquery-1.10.1.min.js'); ?>
	<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<?php echo Asset::js('bootstrap.min.js'); ?>
	<?php echo Asset::js('plugins.js'); ?>
	<?php echo Asset::js('main.js'); ?>
</body>
</html>
