var objeto = 
{
	init: function()
	{
		//Cadastro de usuário
		objeto.form_user();
		objeto.date_picker('#data-inicio', '#data-final'); 

		$('#edit-field').on('click', function(){
			objeto.enable_edit_form();
		});

		//Quando usuário clicar em fechar o navegador ou janaela

		//Página de pesquisa
		$('.horario-inicio, .horario-fim').mask('99:99:99');

		//Banners
		objeto.banners();
		//Perguntas
		objeto.perguntas();
		//Promoções
		objeto.promocoes();
		//Participando do quiz
		objeto.participacao();

		//Clicar no link do participante;
		$(document.body).on('click', '.btn-parceiro-click', function(e){
			e.preventDefault();
			var link = this.href;
			var id = this.id;
			var another = this.rel;
			var url = '/conta-click-parceiro';
			objeto.conta_click(link, id, url, another);
		});

		$(document.body).on('click', '.btn-send-email-winners', function(event) {
			event.preventDefault();
			objeto.envia_email_ganhadores();
		});
	},

	conta_click: function(link, id, url, another)
	{
		$.ajax({
			url: url,
			async: false,
			type: 'POST',
			dataType: 'json',
			data: {parceiro_id: id, promotion_id: another},
		})
		.done(function(response) {
			window.open(link, '_blank');
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	},

	envia_email_ganhadores:function()
	{
		var data = $('#form-send-email').serialize();
		$.ajax({
			url: 'send_email',
			type: 'POST',
			dataType: 'json',
			data: data,
		})
		.done(function(response) {
			console.log("success");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	},

	participacao: function()
	{
		$('.slide:first').removeClass('hidden').addClass('active');
		//Quando o botão reponder for clicado
		$('.btn-resposta-pergunta-aberta').on('click', function(e){
			e.preventDefault();

			//Definindo variáveis que serão passadas
			var resposta = $(this).siblings('#resposta-usuario').val();
			var pergunta_id = $(this).siblings('#form_pergunta_id').val();
			var type = $(this).siblings('#form_type').val();

			var pesquisa_id = $('#form_pesquisa_id').val();
			var base_url = $('#form_base_url').val();
			
			if(resposta === '')
			{
				$(this).parent('.form-group').addClass('has-error');
				$(this).siblings('.erro-form').text('Preencha o campo de resposta');
			}
			else
			{
				$(this).parent('.form-group').addClass('has-success');
				$(this).siblings('.erro-form').text('');
				$(this).addClass('hidden');

				$.ajax({
					dataType: 'json',
					url: base_url+'promotions/resposta',
					type: 'POST',
					data:{resposta:resposta, pergunta_id:pergunta_id, pesquisa_id:pesquisa_id, type:type},
					beforeSend: function()
					{
						$(this).addClass('hidden');
						$('#progress').removeClass('hidden');
					},
					success: function(data)
					{
						$('#progress').addClass('hidden');
						console.log(data);
						if(data.status == true)
						{
							$('.slide.active').addClass('hidden');
							if($('.slide.active').next('.slide').hasClass('hidden'))
							{
								$('.slide.active').removeClass('active').next().removeClass('hidden').addClass('active');	
							}
							else
							{
								$('.slide.active').addClass('hidden').removeClass('active');
								$('#finalizar-participacao').removeClass('hidden');
							}
						}
					}		
				})
			}
		});

		//Quando o botão responder for clicado em uma pergunta fechada
		$('.btn-resposta-pergunta-fechada').on('click', function(e){
			e.preventDefault();

			//Definindo variáveis que serão passadas
			var resposta = $('p input[name="opcao"]:checked').val();
			var pergunta_id = $(this).siblings('#form_pergunta_id').val();
			var type = $(this).siblings('#form_type').val();

			var pesquisa_id = $('#form_pesquisa_id').val();
			var base_url = $('#form_base_url').val();
			
			if(!resposta)
			{
				alert('Escolha uma opção como resposta');
			}
			else
			{
				$(this).parent('.form-group').addClass('has-success');
				$(this).addClass('hidden');
				
				$.ajax({
					dataType: 'json',
					url: base_url+'promotions/resposta',
					type: 'POST',
					data:{opcao_id:resposta, pergunta_id:pergunta_id, pesquisa_id:pesquisa_id, type:type},
					beforeSend: function()
					{
						$(this).addClass('hidden');
						$('#progress').removeClass('hidden');
					},
					success: function(data)
					{
						$('#progress').addClass('hidden');
						console.log(data);
						if(data.status == true)
						{
							$('.slide.active').addClass('hidden');
							if($('.slide.active').next('.slide').hasClass('hidden'))
							{
								$('.slide.active').removeClass('active').next().removeClass('hidden').addClass('active');	
							}
							else
							{
								$('.slide.active').addClass('hidden').removeClass('active');
								$('#finalizar-participacao').removeClass('hidden');
							}
							
						}
					}		
				})

			}
		});
		
		//Concluir participacao
		$('#btn-concluir-participacao').on('click', function(e){
			e.preventDefault();
			
			var promotion_id = $('#form_promotion_id').val();
			var base_url = $('#form_base_url').val();

			$.ajax({
				dataType: 'json',
				type: 'POST',
				url: base_url+'promotions/participacao',
				data:{promotion_id:promotion_id},
				beforeSend: function()
				{
					$('#aviso-status').text('Processando...');
					$('#btn-concluir-participacao').addClass('hidden');
				},
				success: function(data)
				{
					if(data.status == true)
					{
						$('#finalizar-participacao').html('<h3>Parabéns sua participação foi confirmada com sucesso</h3><p>Agora você pode sair dessa tela com segurança, nós recomendamos que você faça logoff do sistema antes de fechar essa janela.</p>');
					}
					else
					{
						$('#btn-concluir-participacao').removeClass('hidden');
						$('#aviso-status').text('Houve um erro tente novamente');
					}
				}
			});

			return false;
		});
	},

	banners: function()
	{
		if($('#validation-error').val() == 'true')
		{
			$('#form_banners select[name="type"]').val('');
		}

		$('#form_submit').click(function(){
			$('input, select, textarea').each(function(){
				if($(this).val() == '')
				{
					$(this).append('<span id="error-val" class="text-error">Campo obrigatório</span>');
					$(this).parent('.form-group').addClass('has-error');
					
					return false;
				}
				else
				{
					form.submit();
				}
			});
		});

		$('#form_banners select[name="type"]').change(function(){
			switch(this.value)
			{
				case 'parceria':
					console.log('É um banner de parceria');
					$('#cont-media-url').addClass('hidden');
					$('#cont-media-url textarea').attr('disabled',true);
					$('#cont-url-banner').removeClass('hidden');
					$('#cont-url-banner input').removeAttr('disabled');
					$('#cont-file-banner').removeClass('hidden');
					$('#cont-file-banner input').removeAttr('disabled');
				break;
				
				case 'adsense':
					$('#cont-media-url').removeClass('hidden');
					$('#cont-media-url textarea').removeAttr('disabled');
					$('#cont-url-banner').addClass('hidden');
					$('#cont-file-banner').addClass('hidden');
					$('#cont-file-banner input').attr('disabled', true);
				break;

				case '':
					alert('Escolha um tipo');
					$('#cont-media-url').addClass('hidden');
				break;
			}
		});
	},

	perguntas: function()
	{
		/*
		$('#btn-add-options').click(function(e){
			e.preventDefault();
			if($('#container-add-options').hasClass('hidden')){
				$('#container-add-options').removeClass('hidden');
			}
			else
			{
				return false;
			}
		});
		*/
		//Quando o campo é preenchido
		$('#title-option').change(function(){
			if($(this).val() == ''){
				if($(this).parents('.form-group').hasClass('has-success'))
				{
					$(this).parents('.form-group').removeClass('has-success');
				}
				$(this).parents('.form-group').addClass('has-error');
				$('#btn-add-option').removeClass('btn-primary').addClass('btn-danger');
			}
			else
			{
				$(this).parents('.form-group').addClass('has-success');
				$('#btn-add-option').removeClass('btn-primary btn-danger').addClass('btn-success');
				$('#val-title').text('');
			}
		});

		//Clicar em Adicionar a Opção
		$('#btn-add-option').on('click', function(e){
			e.preventDefault();
			var opcao = $('#title-option').val(), pergunta = $('#pergunta').val();

			if(opcao === ''){
				
				if($('#title-option').parents('.form-group').hasClass('has-success'))
				{
					$('#title-option').parents('.form-group').removeClass('has-success');
				}
				
				$('#title-option').parents('.form-group').addClass('has-error');
				$('#btn-add-option').removeClass('btn-primary').addClass('btn-danger');
				$('#val-title').text('Preencha o campo acima');	
			}
			else
			{
				$('#title-option').parents('.form-group').addClass('has-success');
				$('#btn-add-option').removeClass('btn-primary btn-danger').addClass('btn-success');
				$('#val-title').text('');
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: $('#form-opcoes').attr('action'),
					data: {title:opcao, pergunta_id:pergunta},
					beforeSend: function(data)
					{
						$('#progress').removeClass('hidden');
					}, 
					success: function(data)
					{
						$('#progress').addClass('hidden');
						if(data.status == true)
						{
							$('#adicionados').append('<span class="label label-success">'+$('#title-option').val()+'</span>');
							$('#success-title').text('Opção adicionada com sucesso');
							$('#title-option').parents('.form-group').removeClass('has-error has-success');
							$('#btn-add-option').removeClass('btn-success').addClass('btn-primary');
							$('#title-option').val('');
							setTimeout('$("#success-title").text("");', 2000);
						}
						else
						{
							$('#val-title').text('Erro ao cadastrar a opção!');
							setTimeout('$("#val-title").text("");', 3000);
						}

					},
					error: function(data)
					{
						console.debug(data);
					}
				});
			}
		});

	},

	promocoes: function()
	{
		//Carrega brindes
		$('#content-brindes').show(function(){
			var id_promotion = $('#form_id_promotion').val();
			var base_url	= $('.base-url').val();
			$.ajax({
				type: 'GET',
				url: base_url+'admin/brindes/get_brindes',
				data: {id:id_promotion},
				dataType: 'json',
				beforeSend: function()
				{
					$('#progress').removeClass('hidden');
				},
				success: function(data)
				{
					$('#progress').addClass('hidden');
					console.log(data.status);
					console.log(data.brindes);
					
					var items = []
					$.each(data.brindes, function(key, val){
						items.push('<li class="list-group-item">'+val+'<span class="pull-right"><span class="btn-group"><a href="'+base_url+'admin/brindes/view/'+key+'" class="btn-sm btn-link"><i class="glyphicon glyphicon-eye-open"></i> Visualizar</a><a href="'+base_url+'admin/brindes/delete/'+key+'" class="btn-sm btn-link" onclick="return confirm("Tem certeza que deseja excluir?")"><i class="glyphicon glyphicon-trash"></i> Excluir</a></span></span></li>');
					});

					$( "<ul/>", {
					    "class": "list-group",
					    html: items.join( "" )
					}).appendTo( "#content-brindes" );
				}
			});
		});
		//Promoções no site
		$('.btn-brinde-promotion').popover('hide');
	},

	is_cpf: function(cpf)
	{
		var validador = /^\d{3}\.\d{3}\.\d{3}\-\d{2}$/;
		if(!validador.test(cpf))
		{
			return 'erro';
		}else{
			return 'ok';
		}
	},

	date_picker: function(data1, data2)
	{
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		 
		var checkin = $(data1).datepicker({
		  onRender: function(date) {
		    return date.valueOf() < now.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  if (ev.date.valueOf() > checkout.date.valueOf()) {
		    var newDate = new Date(ev.date)
		    newDate.setDate(newDate.getDate() + 1);
		    checkout.setValue(newDate);
		  }
		  checkin.hide();
		  $(data2)[0].focus();
		}).data('datepicker');
		var checkout = $(data2).datepicker({
		  onRender: function(date) {
		    return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  checkout.hide();
		}).data('datepicker');
	},

	show_form: function(form, esconde)
	{
		esconde.fadeOut(1000).removeClass('show').addClass('hidden');
		form.removeClass('hidden').addClass('show');
		
		objeto.form_user();
	},

	enable_edit_form: function()
	{
		$('input[type="text"], textarea, select').removeAttr('readonly');
		$('.btn-group, #check-reset').removeClass('hidden');

		objeto.form_user();
	},

	form_user: function()
	{
		//Elimina as classes de erro quando usuário digita no campo validado
		$('#form-user-profile input[type="text"], #form-user-profile input[type="password"]').keypress(function(){
			
			if($(this).parent('.form-group').hasClass('has-error'))
			{
				$(this).parent('.form-group').removeClass('has-error');
				$(this).siblings('.text-danger').remove();
			}

			if($(this).attr('id','username') && $(this).parent('.input-group').parent('.form-group').hasClass('has-error'))
			{
				$(this).parent('.input-group').parent('.form-group').removeClass('has-error');
				$(this).parent('.input-group').siblings('.text-danger').remove();
			}
		});

		$('#form-user-profile input[type="text"], #form-user-profile select').each(function(){
			if($(this).val() == "")
			{
				$(this).parent('.form-group').addClass('has-error');
				$(this).attr('placeholder', 'Preencha esse campo');
			}

			if($(this).val() != "")
			{
				$(this).parent('.form-group').addClass('has-success');
			}

		});

		$('#form-user-profile input[type="text"], #form-user-profile select').change(function(){
			if(this.value != '')
			{
				$(this).parent('.form-group').addClass('has-success');
			}
			else
			{
				$(this).parent('.form-group').addClass('has-error');
				$(this).attr('placeholder', 'Preencha esse campo');
			}
		});

		$('#form-user-profile input[type="text"], #form-user-profile select').blur(function(){
			if(this.value != '' && this.value != '__/__/____' && this.value != '________' && this.value != '___.___.___-__' && this.value != '_____.___')
			{
				$(this).parent('.form-group').addClass('has-success');
			}
			else
			{
				$(this).parent('.form-group').addClass('has-error');
				$(this).attr('placeholder', 'Preencha esse campo');
			}
		});

		$('.cpf').blur(function(){
			if(!objeto.valida_cpf(this.value))
			{
				this.focus();
				$(this).attr('placeholder', 'digite um cpf válido');
				$(this).parent('.form-group').removeClass('has-success');
				$(this).parent('.form-group').addClass('has-error');
				$(this).parent('.form-group').append('<span class="text-error" id="error-cpf"><small>Digite seu número de CPF</small></span>');
				setTimeout('$("#error-cpf").fadeOut().remove();', 4000);
			}
		});

		$('.date-birth').mask('99/99/9999');
		$('.number_rg').mask('99999999');
		$('.cpf').mask('99999999999');
		$('.cep').mask('99999.999');

		$('.reset-pass').click(function(){
		 	if($('.reset-pass').is(':checked'))
		 	{
		 		$('.new-password').removeAttr('disabled');
		 		if($('.new-password').val() == '')
		 		{
		 			$('.new-password').parent('.form-group').addClass('has-error');
		 		}
		 	}
		 	else
		 	{
		 		$('.new-password').attr('disabled', true);	
		 	}

		 });
	},

	valida_cpf: function(cpf)
	{
	   if(cpf.length != 11 || cpf.replace(eval('/'+cpf.charAt(1)+'/g'),'') == '')
	   {
	      return false;
	   }
	   else
	   {
	      for(n=9; n<11; n++)
	      {
	         for(d=0, c=0; c<n; c++) d += cpf.charAt(c) * ((n + 1) - c);
	         d = ((10 * d) % 11) % 10;
	         if(cpf.charAt(c) != d) return false;
	      }
	      return true;
	   }
	}	
	
}

$(document).ready(function(){	
	objeto.init();
})