-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 17/02/2014 às 12h44min
-- Versão do Servidor: 5.5.35
-- Versão do PHP: 5.5.9-1+sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `odebateon_pesquisa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `media_url` text NOT NULL,
  `views` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `title`, `description`, `user_id`, `type`, `url`, `media_url`, `views`, `created_at`, `updated_at`) VALUES
(1, 'Publicidade', 'Esses parceiros tornam possíveis as pesquisas. Clique e participe.', 8, 'adsense', NULL, '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>\r\n<!-- ODebateOn -->\r\n<ins class="adsbygoogle"\r\n     style="display:inline-block;width:728px;height:90px"\r\n     data-ad-client="ca-pub-2207739562838337"\r\n     data-ad-slot="6314517600"></ins>\r\n<script>\r\n(adsbygoogle = window.adsbygoogle || []).push({});\r\n</script>', NULL, 1390784750, 1390784750),
(2, 'Pub1', 'Teste', 7, 'adsense', NULL, '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>\r\n<!-- 300x250, criado 12/10/10 -->\r\n<ins class="adsbygoogle"\r\n     style="display:inline-block;width:300px;height:250px"\r\n     data-ad-client="ca-pub-8262571102889015"\r\n     data-ad-slot="2493751546"></ins>\r\n<script>\r\n(adsbygoogle = window.adsbygoogle || []).push({});\r\n</script>', NULL, 1390790744, 1391560936),
(3, 'Pub 2', 'Banner google adsense', 8, 'adsense', NULL, '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>\r\n<!-- ODebateOn -->\r\n<ins class="adsbygoogle"\r\n     style="display:inline-block;width:728px;height:90px"\r\n     data-ad-client="ca-pub-2207739562838337"\r\n     data-ad-slot="6314517600"></ins>\r\n<script>\r\n(adsbygoogle = window.adsbygoogle || []).push({});\r\n</script>', NULL, 1391570614, 1391570614);

-- --------------------------------------------------------

--
-- Estrutura da tabela `brindes`
--

CREATE TABLE IF NOT EXISTS `brindes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `description` text NOT NULL,
  `parceiro_id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `brindes`
--

INSERT INTO `brindes` (`id`, `name`, `amount`, `description`, `parceiro_id`, `promotion_id`, `created_at`, `updated_at`) VALUES
(1, '1 Ingresso para o Show da Lady Gaga', 2, '1 Ingresso com direito a acompanhante para o Show da Lady Gaga', 2, 1, 1391568503, 1391568503),
(2, 'Entradas para Jogo da Copa do Mundo', 2, '1 entrada para você + 1 entrada para seu acompanhante.', 2, 1, 1391568561, 1391568561),
(3, 'Livro Jóia Rara', 1, 'Livro de Celestino Vivian sobre sua passagem na Folha de São Paulo', 3, 2, 1392130101, 1392130101),
(4, 'Ingresso para o CineMagik 3D', 2, 'As Lojas Brasileiras oferecem dois ingressos para um filme em 3D no CineMagik do Plaza Shopping', 3, 2, 1392130168, 1392130168),
(5, 'Vale compras de R$ 100', 1, 'O Supermercado Costa Compacto oferece um vale compras para sua loja na Tenente Coronel Amado, no Centro de Macaé', 3, 2, 1392130222, 1392130222);

-- --------------------------------------------------------

--
-- Estrutura da tabela `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `type` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `migration` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `migration`
--

INSERT INTO `migration` (`type`, `name`, `migration`) VALUES
('app', 'default', '001_create_users'),
('app', 'default', '002_add_resgisterhash_to_users'),
('app', 'default', '003_add_userstatus_to_users'),
('app', 'default', '004_add_profilefields_to_users'),
('app', 'default', '005_create_pesquisas'),
('app', 'default', '006_add_status_to_pesquisas'),
('app', 'default', '007_create_perguntas'),
('app', 'default', '008_create_banners'),
('app', 'default', '009_rename_field_pergunta_id_to_pesquisa_id_in_perguntas'),
('app', 'default', '010_create_resposta'),
('app', 'default', '011_create_opcaos'),
('app', 'default', '012_create_promotions'),
('app', 'default', '013_create_parceiros'),
('app', 'default', '014_add_status_to_promotions'),
('app', 'default', '015_delete_dates_from_pesquisas'),
('app', 'default', '016_create_brindes'),
('app', 'default', '017_add_title_to_promotions'),
('app', 'default', '018_create_users_promotions'),
('app', 'default', '019_create_users_opcoes'),
('app', 'default', '020_create_winners'),
('app', 'default', '021_create_raffles');

-- --------------------------------------------------------

--
-- Estrutura da tabela `opcoes`
--

CREATE TABLE IF NOT EXISTS `opcoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `pergunta_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Extraindo dados da tabela `opcoes`
--

INSERT INTO `opcoes` (`id`, `title`, `pergunta_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Excelente', 1, 7, 1390790870, NULL),
(2, 'Bom', 1, 7, 1390790882, NULL),
(3, 'Regular', 1, 7, 1390790890, NULL),
(4, 'Ruim', 1, 7, 1390790894, NULL),
(5, 'Péssimo', 1, 7, 1390790904, NULL),
(6, '1', 3, 8, 1391567267, NULL),
(7, '13', 3, 8, 1391567272, NULL),
(8, '12', 3, 8, 1391567277, NULL),
(9, '25', 3, 8, 1391567285, NULL),
(10, 'São Paulo', 4, 8, 1391567323, NULL),
(11, 'Rio de Janeiro', 4, 8, 1391567332, NULL),
(12, 'Minas Gerais', 4, 8, 1391567344, NULL),
(13, 'Rio Grande do Sul', 4, 8, 1391567353, NULL),
(14, 'Excelente', 6, 7, 1392119788, NULL),
(16, 'Boa', 6, 7, 1392119867, NULL),
(17, 'Regular', 6, 7, 1392119872, NULL),
(18, 'Ruim', 6, 7, 1392119875, NULL),
(19, 'Péssima', 6, 7, 1392119888, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `parceiros`
--

CREATE TABLE IF NOT EXISTS `parceiros` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `site_url` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `description` text,
  `status` varchar(16) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `parceiros`
--

INSERT INTO `parceiros` (`id`, `name`, `site_url`, `image_url`, `email`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Jornal O Debate On', 'http://www.odebateon.com.br', '', 'contato@odebateon.com.br', 'Próprio Jornal', 'ativo', 1391568485, 1391568485),
(3, 'Fiat Valore Macaé', 'http://www.valorenet.com.br/macae/‎', 'files/parceiros/4d81595ae35ab2b20fb566c51f2e85cd.jpg', 'oscarpiresjunior@gmail.com', 'A Fiat Valore Macaé é a sua concessionária Fiat na Capital Nacional do Petróleo. Os melhores modelos da marca líder em venda de veículos no Brasil.', 'ativo', 1392120458, 1392120458);

-- --------------------------------------------------------

--
-- Estrutura da tabela `perguntas`
--

CREATE TABLE IF NOT EXISTS `perguntas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pesquisa_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `perguntas`
--

INSERT INTO `perguntas` (`id`, `description`, `banner_id`, `type`, `user_id`, `pesquisa_id`, `created_at`, `updated_at`) VALUES
(1, 'Qual a sua avaliação do governo municipal no ano de 2013?', 2, 'fechada', 7, 1, 1390790824, 1390790824),
(2, 'Olá qual o seu nome?', 1, 'aberta', 8, 2, 1391567176, 1391567176),
(3, 'Qual número você escolhe?', 1, 'fechada', 8, 2, 1391567217, 1391567217),
(4, 'Qual o melhor estado do Brasil?', 1, 'fechada', 8, 2, 1391567247, 1391567247),
(5, 'Resumidamente, dê a sua opinião sobre o transporte público em Macaé', 3, 'aberta', 7, 1, 1392119678, 1392119678),
(6, 'Qual a avaliação da Câmara Municipal em 2013?', 2, 'fechada', 7, 1, 1392119759, 1392119759);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pesquisas`
--

CREATE TABLE IF NOT EXISTS `pesquisas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `result` text,
  `views` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `status` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `pesquisas`
--

INSERT INTO `pesquisas` (`id`, `title`, `description`, `result`, `views`, `user_id`, `created_at`, `updated_at`, `status`) VALUES
(1, 'De olho na cidade', 'A pesquisa ''De olho na cidade'' quer saber a sua opinião sobre os temas mais importantes para o seu dia a dia. ', NULL, NULL, 7, 1390662728, 1392129081, 'ativo'),
(2, 'Pesquisa Teste Wesley', 'Teste de pesquisa', NULL, NULL, 8, 1391567128, 1391567386, 'ativo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `promotions`
--

CREATE TABLE IF NOT EXISTS `promotions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `pesquisa_id` int(11) NOT NULL,
  `date_start` int(11) NOT NULL,
  `date_end` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `status` varchar(16) NOT NULL,
  `title` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `promotions`
--

INSERT INTO `promotions` (`id`, `description`, `pesquisa_id`, `date_start`, `date_end`, `user_id`, `created_at`, `updated_at`, `status`, `title`) VALUES
(1, 'Olá mundo, essa é uma promoção meramente fictícia', 2, 1391587200, 1391803200, 7, 1391568155, 1392130403, 'inativo', 'Promoção de teste'),
(2, 'Aproveite. Participe de nossa pesquisa com 3 perguntas e participe do sorteio de três superbrindes de nossos parceiros.', 1, 1392170400, 1392256800, 7, 1392129237, 1392130753, 'ativo', 'Promoteste 1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `raffles`
--

CREATE TABLE IF NOT EXISTS `raffles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `raffles`
--

INSERT INTO `raffles` (`id`, `promotion_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1392359633, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `respostas`
--

CREATE TABLE IF NOT EXISTS `respostas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `pergunta_id` int(11) NOT NULL,
  `pesquisa_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `respostas`
--

INSERT INTO `respostas` (`id`, `description`, `pergunta_id`, `pesquisa_id`, `user_id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Não sou usuário', 5, 1, 11, 'aberta', 1392195720, NULL),
(2, 'Em visita à cidade, com caminhada de duas horas, não percebi grandes aglomerações em pontos de ônibus. E em conversas com algumas pessoas, não ouvi reclamações relacionadas ao transporte. O que mais me chamou a atenção foram os coletivos urbanos com ar condicionado. Serão todos assim? É um privilégio que a maioria das cidades desconhece.', 5, 1, 12, 'aberta', 1392213448, NULL),
(3, 'Pode melhorar em qualidade e quantidade. A cidade precisa de um transporte mais moderno e pensado para as necessidades e  especificidades da capital do petróleo.', 5, 1, 13, 'aberta', 1392244338, NULL),
(4, 'Insuficiente frente a demanda.', 5, 1, 14, 'aberta', 1392248041, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `group` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `login_hash` varchar(128) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `profile_fields` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `group`, `last_login`, `login_hash`, `created_at`, `updated_at`, `profile_fields`) VALUES
(5, 'wesleysaraujo', 'mqt8ndJ2taosuEzWPyp28KzDYZlmGjUVjAa1EhxtVKs=', 'wesley@maismineiro.com', 0, 1390367404, '1a37f5c0561ace7e742ea4899840715a1fea2eaa', 1389968694, 1389968792, 'a:13:{s:9:"full_name";s:17:"Wesley S. Araújo";s:13:"register_hash";s:40:"3bfdcd6d4d784e93a3e9392652566cdd7fb3e41c";s:11:"user_status";s:7:"ativado";s:10:"date_birth";s:10:"13/06/1988";s:9:"number_rg";s:8:"14850351";s:3:"cpf";s:11:"08376758632";s:7:"address";s:24:"Estrada dos Bandeirantes";s:8:"district";s:14:"Vargem Pequena";s:11:"number_home";s:5:"11609";s:10:"complement";s:21:"QD 5, Lote 38, CASA 3";s:3:"cep";s:9:"22783.116";s:4:"city";s:14:"Rio de Janeiro";s:2:"uf";s:2:"RJ";}'),
(6, 'wesley', 'mqt8ndJ2taosuEzWPyp28KzDYZlmGjUVjAa1EhxtVKs=', 'wesley@skidun.com.br', 0, 1389969162, 'f563754969b2b52c9caf8dc297aa9b9e481f92d3', 1389969110, 1389969162, 'a:3:{s:9:"full_name";s:13:"Wesley Skidun";s:13:"register_hash";s:40:"165492d6ebe8b4a5ee8bd217740356d674034697";s:11:"user_status";s:10:"incompleto";}'),
(7, 'oscar', 'EVsDlitWZdlVq1hxhepYvaEuX/eqomYbdujsuxYQs1I=', 'oscarpiresjunior@gmail.com', 100, 1392455565, 'c1e30598fc80116d4ff913215ada5acc3bb73e73', 1389969708, NULL, 'a:1:{s:9:"full_name";s:18:"Oscar Pires Junior";}'),
(8, 'admin', 'MyIDQgch81aFM6Xlei+tl+TNyGMfcLmPxe3rtX4LdAA=', 'wsadesigner@gmail.com', 100, 1392653404, 'd0b5849d87c9b91bc62d1456e392dc0ed6137cf4', 1389969804, 1389970291, 'a:1:{s:9:"full_name";s:16:"Wesley S. Araujo";}'),
(9, 'marcovinibr', 'HgGqS3XRjbyfR0yJJzIk/X9+nbie42i1tfkIaspLkN4=', 'marcovinibr@hotmail.com', 0, 1392146251, '74a3c933347b11162154139cbe68de7e936748e5', 1392146193, 1392146359, 'a:13:{s:9:"full_name";s:15:"Marcos Vinicius";s:13:"register_hash";s:40:"ddf8361fe860187f94e0ed54ac77afaa02f05b2c";s:11:"user_status";s:7:"ativado";s:10:"date_birth";s:10:"20/03/1961";s:9:"number_rg";s:8:"05549074";s:3:"cpf";s:11:"68772467720";s:7:"address";s:23:"Rua Euzébio de Queiroz";s:8:"district";s:6:"Centro";s:11:"number_home";s:3:"580";s:10:"complement";s:3:"204";s:3:"cep";s:9:"27910.230";s:4:"city";s:6:"Macaé";s:2:"uf";s:2:"RJ";}'),
(10, 'leonardo', 'oIvSDuMZPZx3lkkTx9fIZKlSDV0Vypw5X7e2IP6JMu4=', 'leonardo@odebateon.com.br', 0, 1392147166, '51d99bb63c166c150c0587448bc40972ff5745dc', 1392146962, 1392147298, 'a:13:{s:9:"full_name";s:24:"leonardo mattos de souza";s:13:"register_hash";s:40:"f780e0a8a15dc2933f2410368ccf7ed4861a67c2";s:11:"user_status";s:7:"ativado";s:10:"date_birth";s:10:"05/11/1976";s:9:"number_rg";s:8:"10576838";s:3:"cpf";s:11:"07478829759";s:7:"address";s:14:"rua 7 quadra i";s:8:"district";s:6:"unamar";s:11:"number_home";s:3:"250";s:10:"complement";s:14:"con long beach";s:3:"cep";s:9:"28927.000";s:4:"city";s:9:"cabo frio";s:2:"uf";s:2:"RJ";}'),
(11, 'RHefler', 'DuPN7XFzAzhBkz6x4t673sK/K6xs1j+vP6ij3fMbGSY=', 'robertohefler@yahoo.com.br', 0, 1392195903, '567d11612865760dc5ad01ba46d1077f36e99264', 1392195205, 1392195564, 'a:13:{s:9:"full_name";s:14:"Roberto Hefler";s:13:"register_hash";s:40:"d684a267bc72d415a70d9ab4797d7eac11de5887";s:11:"user_status";s:7:"ativado";s:10:"date_birth";s:10:"20/01/1956";s:9:"number_rg";s:8:"87282641";s:3:"cpf";s:11:"98945041834";s:7:"address";s:27:"Rua Tancredo Mendes Paixão";s:8:"district";s:3:"BNH";s:11:"number_home";s:3:"199";s:10:"complement";s:7:"Sobrado";s:3:"cep";s:9:"28860.000";s:4:"city";s:17:"Casimiro de Abreu";s:2:"uf";s:2:"RJ";}'),
(12, 'cvivian', 'DMWTngo3t+V/uKo/1akIZ3SztWe5FxF5Sv7a7Cq7dsY=', 'celestinoavivian@gmail.com', 0, 1392214788, 'ef6126e269e461ad7d9937a5ea165e63a66e8ef8', 1392212732, 1392213146, 'a:13:{s:9:"full_name";s:23:"Celestino Angelo Vivian";s:13:"register_hash";s:40:"7e3e186e506100c241a30a56dfbe6bd18008aacf";s:11:"user_status";s:7:"ativado";s:10:"date_birth";s:10:"27/06/1949";s:9:"number_rg";s:8:"58391216";s:3:"cpf";s:11:"70416281834";s:7:"address";s:23:"Av. Professora Ida Kolb";s:8:"district";s:10:"Casa Verde";s:11:"number_home";s:3:"387";s:10:"complement";s:12:"Apto 73 Bl 1";s:3:"cep";s:9:"02518.000";s:4:"city";s:10:"São Paulo";s:2:"uf";s:2:"SP";}'),
(13, 'fofuxa', 'pUpMpw/QQQ2SrjIy+vRhdL3dQ7o/q+XoqWfxNBWk9UM=', 'simonimpires@gmail.com', 0, 1392243627, '5c4c2e7a70d67edfe49f34582daba0347fb3bf0a', 1392242826, 1392243831, 'a:13:{s:9:"full_name";s:23:"Simoni Magalhães Pires";s:13:"register_hash";s:40:"e1a40dd061cfc127b1cb8f668e7303087ce50066";s:11:"user_status";s:7:"ativado";s:10:"date_birth";s:10:"15/12/1969";s:9:"number_rg";s:8:"79093035";s:3:"cpf";s:11:"99699184787";s:7:"address";s:14:"Rua de Santana";s:8:"district";s:6:"centro";s:11:"number_home";s:3:"270";s:10:"complement";s:8:"apto 201";s:3:"cep";s:9:"27910.090";s:4:"city";s:6:"Macaé";s:2:"uf";s:2:"RJ";}'),
(14, 'alice', '1+5MAHGB6qaJLj6LolPl3C+Vvn24n0thDpQFRuJp6OI=', 'alice.moraes.amorim@gmail.com', 0, 1392247839, 'a730b7e6c3fc8f1d5350a4d2b96892c64d6d894a', 1392247755, 1392247966, 'a:13:{s:9:"full_name";s:28:"Alice de Moraes Amorim Vogas";s:13:"register_hash";s:40:"457bfc681506ae4ebd33742244bacd28cc243a04";s:11:"user_status";s:7:"ativado";s:10:"date_birth";s:10:"25/09/1982";s:9:"number_rg";s:8:"11111111";s:3:"cpf";s:11:"08984414719";s:7:"address";s:17:"rua Soares cabral";s:8:"district";s:11:"laranjeiras";s:11:"number_home";s:2:"56";s:10:"complement";s:3:"701";s:3:"cep";s:8:"22420020";s:4:"city";s:14:"RIO DE JANEIRO";s:2:"uf";s:2:"RJ";}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users_opcoes`
--

CREATE TABLE IF NOT EXISTS `users_opcoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `opcao_id` int(11) NOT NULL,
  `pergunta_id` int(11) NOT NULL,
  `pesquisa_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `users_opcoes`
--

INSERT INTO `users_opcoes` (`id`, `opcao_id`, `pergunta_id`, `pesquisa_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 4, 1, 1, 11, 1392195644, NULL),
(2, 4, 1, 1, 11, 1392195691, NULL),
(3, 18, 6, 1, 11, 1392195739, NULL),
(4, 3, 1, 1, 12, 1392213231, NULL),
(5, 16, 6, 1, 12, 1392213474, NULL),
(6, 3, 1, 1, 13, 1392243888, NULL),
(7, 3, 1, 1, 13, 1392244243, NULL),
(8, 18, 6, 1, 13, 1392244353, NULL),
(9, 3, 1, 1, 14, 1392247994, NULL),
(10, 18, 6, 1, 14, 1392248053, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users_promotions`
--

CREATE TABLE IF NOT EXISTS `users_promotions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `users_promotions`
--

INSERT INTO `users_promotions` (`id`, `user_id`, `promotion_id`, `created_at`, `updated_at`) VALUES
(1, 11, 2, 1392195744, NULL),
(2, 12, 2, 1392213488, NULL),
(3, 13, 2, 1392244367, NULL),
(4, 14, 2, 1392248059, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `winners`
--

CREATE TABLE IF NOT EXISTS `winners` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `brinde_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `winners`
--

INSERT INTO `winners` (`id`, `user_id`, `promotion_id`, `brinde_id`, `created_at`, `updated_at`) VALUES
(1, 13, 2, 3, 1392359633, NULL),
(2, 14, 2, 4, 1392359633, NULL),
(3, 12, 2, 5, 1392359633, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
