# Sistema para sorteio de brindes para leitores de um jornal.

Esse sistema foi desenvolvido usando PHP no backend, HTML, CSS e Javascript no frontend.

Nesse sistema o usuário se cadastra, preenche seu perfil, e responde a pesquisa que foi cadastrada, essa pesquisa é em formato de um quiz. No final do período da promoção o sistema faz um sorteio aleatório e escolhe a quantidade de ganhadores de acordo com a quantidade de brindes.